use crate::check_type;
use crate::field::EventField;
use crate::log_type::OutputType;
use crate::parse::ParseArgs;
use crate::stats_vec::StatsDiffVec;
use crate::time_chunks::DetailEventTimeChunks;
use crate::{top, Args, Input, LogType};

use std::fs::File;
use std::io;
use std::io::{stdin, BufReader, ErrorKind};
use std::thread;

const MIN_PLOT_INTERVAL: i64 = 5;

pub enum Reader {
    File(BufReader<File>),
    Stdin,
}

fn open_reader(file_name: &str) -> Result<BufReader<File>, io::Error> {
    let f = File::open(file_name)?;
    Ok(BufReader::new(f))
}

pub fn exec_stats(args: Args) -> Result<(), io::Error> {
    let (log_type, reader, line_cache) =
        determine_input_type(&args.input, args.requested_log_type)?;

    let calc_args = ParseArgs::new(args.search_for, args.thread_ct, line_cache);

    match args.interval {
        Some(interval) => {
            let time_stats =
                log_type.calculate_time_stats(reader, interval, calc_args, args.sort_by);

            time_stats.print(
                args.print_format,
                &args.print_fields,
                args.limit,
                args.color,
            )?;
        }
        None => {
            let stats = log_type.calculate_stats(reader, calc_args, args.sort_by);

            stats.print(
                args.print_format,
                &args.print_fields,
                args.limit,
                None,
                None,
            )?;
        }
    }

    Ok(())
}

pub fn exec_stats_verbose(args: Args) -> Result<(), io::Error> {
    let (log_type, reader, line_cache) =
        determine_input_type(&args.input, args.requested_log_type)?;

    let calc_args = ParseArgs::new(args.search_for, args.thread_ct, line_cache);
    let log_type = log_type.specify(OutputType::Detail);

    let mut data = log_type.calculate_detailed_time_data(reader, calc_args);

    if let Some(interval) = args.interval {
        let chunked_data =
            DetailEventTimeChunks::from_time_data(data, interval, log_type, EventField::Duration);

        let chunked_stats = chunked_data.into_verbose(args.sort_by, args.color);

        chunked_stats.print(
            args.print_format,
            &args.print_fields,
            args.limit,
            args.color,
        )
    } else {
        data.sort_by(EventField::Duration);

        let stats = data.into_verbose_stats_vec(log_type.stats_t(), args.sort_by, args.color);

        stats.print(args.print_format, &args.print_fields, args.limit)
    }
}

pub fn exec_compare(args: Args, cmp_input: Input) -> Result<(), io::Error> {
    let (log_type, reader, first_cache) =
        determine_input_type(&args.input, args.requested_log_type)?;
    let (cmp_type, cmp_reader, cmp_cache) =
        determine_input_type(&cmp_input, args.requested_log_type)?;

    if log_type.stats_t() == cmp_type.stats_t() {
        let calc_args = ParseArgs::new(args.search_for.clone(), args.thread_ct / 2, first_cache);
        let sort_by = args.sort_by;

        let handle = thread::spawn(move || log_type.calculate_stats(reader, calc_args, sort_by));

        let cmp_calc_args = ParseArgs::new(args.search_for.clone(), args.thread_ct / 2, cmp_cache);
        let cmp_handle =
            thread::spawn(move || cmp_type.calculate_stats(cmp_reader, cmp_calc_args, sort_by));

        let stats = handle.join().expect("Primary thread panicked");
        let cmp_stats = cmp_handle.join().expect("Compare thread panicked");

        let file_name = args.input.file_name();
        let cmp_file_name = cmp_input.file_name();

        let diffs = StatsDiffVec::new(
            stats.stats_t,
            &stats,
            &cmp_stats,
            &file_name,
            &cmp_file_name,
            args.limit,
        );

        diffs.print(
            args.print_format,
            &args.print_fields,
            args.limit,
            args.color,
        )?;
    } else {
        eprintln!(
            "Log types found -- {} : {}",
            log_type.stats_t(),
            cmp_type.stats_t()
        );
        return Err(io::Error::new(
            ErrorKind::InvalidInput,
            "Attempted to compare different log types",
        ));
    }

    Ok(())
}

pub fn exec_bench(args: Args, bench_vers: String) -> Result<(), io::Error> {
    let (log_type, reader, line_cache) =
        determine_input_type(&args.input, args.requested_log_type)?;

    let calc_args = ParseArgs::new(args.search_for, args.thread_ct, line_cache);
    let sort_by = args.sort_by;

    let handle = thread::spawn(move || log_type.calculate_stats(reader, calc_args, sort_by));

    let file_name = args.input.file_name();
    let stats = handle.join().expect("Primary thread panicked");
    let stats_t = stats.stats_t;

    let bench_stats = match crate::bench::bench_stats(&bench_vers, stats_t) {
        Some(Ok(s)) => s,
        _ => {
            eprintln!("Benchmarking data not available for this version");
            return Ok(());
        }
    };

    let diffs = StatsDiffVec::new(
        stats_t,
        &bench_stats,
        &stats,
        &bench_vers,
        &file_name,
        args.limit,
    );

    diffs.print(
        args.print_format,
        &args.print_fields,
        args.limit,
        args.color,
    )?;

    Ok(())
}

pub fn exec_top(args: Args, sort_by: EventField, display: top::DisplayFmt) -> io::Result<()> {
    let (log_type, reader, line_cache) =
        determine_input_type(&args.input, args.requested_log_type)?;

    let log_type = log_type.specify(OutputType::Detail);
    let calc_args = ParseArgs::new(None, args.thread_ct, line_cache);

    if let Some(interval) = args.interval {
        let time_stats = log_type.calculate_detailed_time_data(reader, calc_args);
        let time_chunks =
            DetailEventTimeChunks::from_time_data(time_stats, interval, log_type, sort_by);

        time_chunks
            .into_top()
            .print(sort_by, display, args.print_format, args.limit, args.color)
    } else {
        let mut data = log_type.calculate_top_stats(reader, calc_args);
        data.insert_session_duration();

        data.print(sort_by, display, args.print_format, args.limit)
    }
}

pub fn exec_errors(args: Args) -> io::Result<()> {
    let (log_type, reader, line_cache) =
        determine_input_type_w_errors(&args.input, args.requested_log_type, true)?;

    let log_type = log_type.specify(OutputType::Error);
    let calc_args = ParseArgs::new(args.search_for, args.thread_ct, line_cache);

    let mut errors = log_type.calculate_error_summary(reader, calc_args);
    errors.sort();

    errors.print(args.color, args.no_border, args.print_format)
}

#[cfg(feature = "plot")]
pub fn exec_plot(args: Args, output_path: Option<&str>) -> Result<(), Box<dyn std::error::Error>> {
    use crate::time_chunks::{DetailTimeChunks, Interval, TimeUnit};
    use std::ffi::OsStr;
    use std::io::Write;
    use std::path::PathBuf;

    let (log_type, reader, line_cache) =
        determine_input_type(&args.input, args.requested_log_type)?;

    let log_type = log_type.specify(OutputType::Detail);
    let calc_args = ParseArgs::new(args.search_for, args.thread_ct, line_cache);

    let raw_data = log_type.calculate_detailed_time_data(reader, calc_args);

    let interval = match args.interval {
        Some(i) => i,
        None => {
            let total_secs = (raw_data.end_time - raw_data.start_time)
                .num_seconds()
                .abs();

            let split_secs = match total_secs / 100 {
                0 => MIN_PLOT_INTERVAL,
                t => t,
            };

            Interval::new(split_secs as u64, TimeUnit::Second)
        }
    };

    let output_path = match output_path {
        Some(p) => PathBuf::from(p),
        None => match args.input {
            Input::File(s) => {
                let mut out_name = PathBuf::from(s)
                    .file_stem()
                    .map(|s| s.to_owned())
                    .expect("Received empty file name");
                out_name.push(OsStr::new("_plot.png"));

                PathBuf::from(out_name)
            }
            Input::Stdin => PathBuf::from("plot.png"),
        },
    };

    let chunks = DetailTimeChunks::from_time_data(raw_data, interval, log_type);

    crate::plot::plot(chunks, interval, &output_path)?;

    writeln!(io::stdout(), "Plots written to {}", &output_path.display())?;

    Ok(())
}

#[cfg(feature = "compress")]
pub fn compress(args: Args, out_path: &str) -> Result<(), Box<dyn std::error::Error>> {
    use std::io::Write;

    let (log_type, reader, line_cache) =
        determine_input_type(&args.input, args.requested_log_type)?;
    let calc_args = ParseArgs::new(args.search_for, args.thread_ct, line_cache);

    let stats = log_type.calculate_stats(reader, calc_args, args.sort_by);

    if stats.stats.len() == 0 {
        eprintln!("Unable to parse input");
        std::process::exit(1);
    }

    let encoded: Vec<u8> = bincode::serialize(&stats)?;
    let mut compressor = lz4::EncoderBuilder::new()
        .level(9)
        .build(File::create(out_path)?)?;

    compressor.write_all(&encoded)?;
    compressor.finish().1?;

    Ok(())
}

fn determine_input_type(
    input: &Input,
    req_type: Option<LogType>,
) -> Result<(LogType, Reader, Vec<String>), io::Error> {
    determine_input_type_w_errors(input, req_type, false)
}

fn determine_input_type_w_errors(
    input: &Input,
    req_type: Option<LogType>,
    check_errors: bool,
) -> Result<(LogType, Reader, Vec<String>), io::Error> {
    let params = match (input, req_type) {
        (Input::File(file_name), Some(log_type)) => {
            let reader = open_reader(file_name)?;
            (log_type, Reader::File(reader), Vec::new())
        }
        (Input::File(file_name), None) => {
            let mut reader = open_reader(file_name)?;

            let (log_type, line_cache) = check_type::check_log_type(&mut reader, check_errors)?;
            (log_type, Reader::File(reader), line_cache)
        }
        (Input::Stdin, Some(log_type)) => (log_type, Reader::Stdin, Vec::new()),
        (Input::Stdin, None) => {
            let (log_type, line_cache) = check_type::check_log_type(stdin().lock(), check_errors)?;
            (log_type, Reader::Stdin, line_cache)
        }
    };

    Ok(params)
}
