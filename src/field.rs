use std::fmt;

use StatField::*;

#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub enum StatField {
    Count,
    Rps,
    P99,
    P95,
    Median,
    Max,
    Min,
    Score,
    Fail,
}

impl Default for StatField {
    fn default() -> StatField {
        StatField::Score
    }
}

impl fmt::Display for StatField {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Count => "COUNT".fmt(f),
            Rps => "RPS".fmt(f),
            P99 => "P99_ms".fmt(f),
            P95 => "P95_ms".fmt(f),
            Median => "MEDIAN_ms".fmt(f),
            Max => "MAX_ms".fmt(f),
            Min => "MIN_ms".fmt(f),
            Score => "SCORE".fmt(f),
            Fail => "%FAIL".fmt(f),
        }
    }
}

impl StatField {
    pub fn all_fields() -> Vec<StatField> {
        vec![Count, Rps, P99, P95, Median, Max, Min, Score, Fail]
    }

    #[inline]
    pub const fn len(&self, lens: &[usize]) -> usize {
        match self {
            Count => lens[1],
            Rps => lens[2],
            P99 => lens[3],
            P95 => lens[4],
            Median => lens[5],
            Max => lens[6],
            Min => lens[7],
            Score => lens[8],
            Fail => lens[9],
        }
    }

    #[inline]
    pub const fn precision(&self) -> usize {
        match self {
            Count => 0,
            Rps | Fail => 2,
            _ => 1,
        }
    }
}

impl From<&str> for StatField {
    fn from(s: &str) -> StatField {
        match s {
            "score" => Score,
            "count" => Count,
            "rps" => Rps,
            "max" => Max,
            "median" => Median,
            "min" => Min,
            "p99" => P99,
            "p95" => P95,
            "fail" => Fail,
            _ => unreachable!(),
        }
    }
}

#[derive(Clone, Copy, Eq, Hash, PartialEq, Debug)]
pub enum EventField {
    Count,
    Rps,
    Duration,
    Db,
    Redis,
    Gitaly,
    Rugged,
    Queue,
    CpuS,
    FailCt,
}

impl EventField {
    pub const fn min_len(&self) -> usize {
        self.header().len()
    }

    pub const fn header(&self) -> &'static str {
        match self {
            EventField::Count => "COUNT",
            EventField::Rps => "RPS",
            EventField::Duration => "DUR",
            EventField::Db => "DB",
            EventField::Redis => "REDIS",
            EventField::Gitaly => "GITALY",
            EventField::Rugged => "RUGGED",
            EventField::Queue => "QUEUE",
            EventField::CpuS => "CPU",
            EventField::FailCt => "FAIL_CT",
        }
    }

    pub fn is_count(&self) -> bool {
        matches!(self, EventField::Count | EventField::FailCt)
    }

    pub fn is_float(&self) -> bool {
        matches!(self, EventField::Rps)
    }
}

impl fmt::Display for EventField {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use EventField::*;

        match self {
            Count => "Count".fmt(f),
            Rps => "Requests per Second".fmt(f),
            Duration => "Duration".fmt(f),
            Db => "Database Duration".fmt(f),
            Redis => "Redis Duration".fmt(f),
            Gitaly => "Gitaly Duration".fmt(f),
            Rugged => "Rugged Duration".fmt(f),
            Queue => "Queue Duration".fmt(f),
            CpuS => "CPU Time".fmt(f),
            FailCt => "Failure Count".fmt(f),
        }
    }
}

impl From<&str> for EventField {
    fn from(s: &str) -> EventField {
        use EventField::*;

        match s {
            "count" => Count,
            "rps" => Rps,
            "duration" => Duration,
            "db" => Db,
            "redis" => Redis,
            "gitaly" => Gitaly,
            "rugged" => Rugged,
            "queue" => Queue,
            "cpu" => CpuS,
            "fail" => FailCt,
            _ => unreachable!(),
        }
    }
}

impl Default for EventField {
    fn default() -> EventField {
        EventField::Duration
    }
}
