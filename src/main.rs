use data::raw::RawData;
use field::{EventField, StatField};
use log_type::{LogType, LogType::*};
use stats_vec::PrintFormat;
use time_chunks::Interval;

use clap::ArgMatches;
use std::convert::TryFrom;
use std::io::{self, ErrorKind};
use std::path::Path;

mod bench;
mod check_type;
mod cli;
mod data;
mod error;
mod event;
mod execute;
mod field;
mod log_type;
mod parse;
mod print;
mod stats;
mod stats_vec;
mod time_chunks;
mod top;
mod util;

#[cfg(feature = "plot")]
mod plot;

pub type HashMap<K, V> = hashbrown::HashMap<K, V>;
pub type HashSet<T> = hashbrown::HashSet<T>;

#[global_allocator]
static ALLOC: tikv_jemallocator::Jemalloc = tikv_jemallocator::Jemalloc;

#[derive(Clone, Debug)]
pub enum Input {
    File(String),
    Stdin,
}

impl Input {
    fn file_name(&self) -> String {
        match &self {
            Input::File(file) => String::from(
                // Remove leading path if present
                Path::new(file)
                    .file_name()
                    .unwrap_or_default()
                    .to_string_lossy(),
            ),
            Input::Stdin => String::from("stdin"),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Args {
    input: Input,
    sort_by: StatField,
    search_for: Option<String>,
    print_fields: Vec<StatField>,
    print_format: PrintFormat,
    requested_log_type: Option<LogType>,
    thread_ct: usize,
    limit: Option<usize>,
    interval: Option<Interval>,
    color: bool,
    no_border: bool,
}

fn main() {
    let cli_args = cli::cli_args().get_matches();

    let input = match cli_args.value_of("file") {
        Some(f) if f != "-" => Input::File(f.to_owned()),
        _ => Input::Stdin,
    };

    let sort_by = if let Some(sort) = cli_args.value_of("sort_by") {
        StatField::from(sort)
    } else {
        StatField::Score
    };

    let search_for = cli_args.value_of("search").map(|s| s.to_ascii_lowercase());

    let bench = cli_args.value_of("benchmark").map(|b| b.to_owned());

    let print_fields = match cli_args.values_of("print_fields") {
        Some(fields) => fields.into_iter().map(StatField::from).collect::<Vec<_>>(),
        None => StatField::all_fields(),
    };

    let print_format = match cli_args.value_of("format") {
        Some(format) => match format {
            "csv" => PrintFormat::Csv,
            "md" => PrintFormat::Md,
            "json" => PrintFormat::Json,
            _ => PrintFormat::Text,
        },
        None => PrintFormat::Text,
    };

    let requested_log_type = if let Some(log_type) = cli_args.value_of("log_type") {
        match log_type {
            "api_json" => Some(ApiJson),
            "api_json_dur_s" => Some(ApiJsonDurS),
            "gitaly" => Some(GitalyText),
            "gitaly_json" => Some(GitalyJson),
            "production_json" => Some(ProdJson),
            "production_json_dur_s" => Some(ProdJsonDurS),
            "sidekiq" => Some(SidekiqText),
            "sidekiq_json" => Some(SidekiqJson),
            "sidekiq_json_dur_s" => Some(SidekiqJsonDurS),
            "shell_json" => Some(ShellJson),
            _ => unreachable!(),
        }
    } else {
        None
    };

    let compare_file = cli_args
        .value_of("compare")
        .map(|comp| Input::File(comp.to_owned()));

    let limit = cli_args
        .value_of("limit")
        .map(|comp| comp.parse::<usize>().expect("Invalid limit"));

    let color = atty::is(atty::Stream::Stdout) || cli_args.is_present("force_color");

    let verbose = cli_args.is_present("verbose");

    let interval = cli_args
        .value_of("interval")
        .and_then(|i| Interval::try_from(i).ok());

    let parsed_args = Args {
        input,
        sort_by,
        search_for,
        print_fields,
        print_format,
        requested_log_type,
        thread_ct: num_cpus::get_physical(),
        limit,
        interval,
        color,
        no_border: bool::default(),
    };

    #[cfg(feature = "plot")]
    {
        if let Some(plot_args) = cli_args.subcommand_matches("plot") {
            let input = match (plot_args.value_of("file"), cli_args.value_of("file")) {
                (Some(f), _) if f != "-" => Input::File(f.to_owned()),
                (None, Some(f)) if f != "-" => Input::File(f.to_owned()),
                _ => Input::Stdin,
            };
            let mut args = parsed_args;
            args.input = input;

            let plot_path = plot_args.value_of("output_file");

            if let Err(e) = execute::exec_plot(args, plot_path) {
                eprintln!("Error - {}", e);
                std::process::exit(1);
            }

            return;
        }
    }

    #[cfg(feature = "compress")]
    if cli_args.is_present("compress") {
        let compress_path = cli_args.value_of("compress").unwrap();
        if let Err(e) = execute::compress(parsed_args, compress_path) {
            eprintln!("Error - {}", e);
            std::process::exit(1);
        }
        return;
    }

    let top_args = cli_args.subcommand_matches("top");
    let error_args = cli_args.subcommand_matches("errors");

    let result = match (top_args, error_args) {
        (Some(args), _) => run_top(args, parsed_args),
        (None, Some(args)) => run_errors(args, parsed_args),
        (None, None) => match (compare_file, bench, verbose) {
            (Some(comp), _, _) => execute::exec_compare(parsed_args, comp),
            (_, Some(bench_vers), _) => execute::exec_bench(parsed_args, bench_vers),
            (None, None, true) => execute::exec_stats_verbose(parsed_args),
            (None, None, false) => execute::exec_stats(parsed_args),
        },
    };

    if let Err(e) = result {
        match e.kind() {
            ErrorKind::BrokenPipe => {} //ignore errors caused by piping
            _ => {
                eprintln!("Error - {}", e);
                std::process::exit(1);
            }
        }
    }
}

fn run_top(top_args: &ArgMatches, mut parsed_args: Args) -> io::Result<()> {
    parsed_args.input = match top_args.value_of("file") {
        Some(f) if f != "-" => Input::File(f.to_owned()),
        _ => parsed_args.input,
    };

    let top_sort: EventField = top_args
        .value_of("sort_by")
        .map(|s| s.into())
        .unwrap_or_default();

    parsed_args.limit = match top_args
        .value_of("limit")
        .and_then(|l| l.parse::<usize>().ok())
    {
        Some(n) => Some(n),
        None => parsed_args.limit,
    };

    parsed_args.interval = match top_args
        .value_of("interval")
        .and_then(|i| Interval::try_from(i).ok())
    {
        Some(i) => Some(i),
        None => parsed_args.interval,
    };

    parsed_args.print_format = match top_args.value_of("format") {
        Some(f) => f.into(),
        None => parsed_args.print_format,
    };

    let top_disp: top::DisplayFmt = top_args
        .value_of("display")
        .map(|s| s.into())
        .unwrap_or_default();

    execute::exec_top(parsed_args, top_sort, top_disp)
}

fn run_errors(err_args: &ArgMatches, mut parsed_args: Args) -> io::Result<()> {
    parsed_args.input = match err_args.value_of("file") {
        Some(f) if f != "-" => Input::File(f.to_owned()),
        _ => parsed_args.input,
    };

    parsed_args.print_format = match err_args.value_of("format").map(|s| s.into()) {
        Some(f) => f,
        None => parsed_args.print_format,
    };

    parsed_args.no_border = err_args.is_present("no_border");

    parsed_args.color = match err_args.is_present("force_color") {
        true => true,
        false => parsed_args.color,
    };

    execute::exec_errors(parsed_args)
}
