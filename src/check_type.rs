use crate::{event::*, log_type::LogType};
use serde::Deserialize;
use std::io::{prelude::*, Error, ErrorKind};

#[derive(Copy, Clone, PartialEq, Debug)]
enum ParsingLogType {
    ApiJson,
    ApiJsonDurS,
    ProdJson,
    ProdJsonDurS,
    SidekiqJson,
    SidekiqJsonDurS,
    SidekiqText,
    GitalyJson,
    GitalyText,
    ShellAPI,
    ShellSSH,
    Ambiguous,
}

#[allow(clippy::from_over_into)]
impl Into<LogType> for ParsingLogType {
    fn into(self) -> LogType {
        match self {
            ParsingLogType::ApiJson => LogType::ApiJson,
            ParsingLogType::ApiJsonDurS => LogType::ApiJsonDurS,
            ParsingLogType::ProdJson => LogType::ProdJson,
            ParsingLogType::ProdJsonDurS => LogType::ProdJsonDurS,
            ParsingLogType::SidekiqJson => LogType::SidekiqJson,
            ParsingLogType::SidekiqJsonDurS => LogType::SidekiqJsonDurS,
            ParsingLogType::SidekiqText => LogType::SidekiqText,
            ParsingLogType::GitalyJson => LogType::GitalyJson,
            ParsingLogType::GitalyText => LogType::GitalyText,
            ParsingLogType::ShellAPI | ParsingLogType::ShellSSH => LogType::ShellJson,
            ParsingLogType::Ambiguous => unreachable!("Tried to convert ambiguous ParsingLogType"),
        }
    }
}

pub fn check_log_type(
    mut reader: impl BufRead,
    check_errors: bool,
) -> Result<(LogType, Vec<String>), Error> {
    let mut buff = String::with_capacity(1024);
    let mut line_cache = Vec::new();

    while reader.read_line(&mut buff)? > 0 {
        if let Some(log_type) = check_line(&buff) {
            match log_type {
                ParsingLogType::ShellAPI | ParsingLogType::Ambiguous => {
                    line_cache.push(buff.clone());
                }
                ParsingLogType::ShellSSH => {
                    line_cache.push(buff);
                    return Ok((LogType::ShellJson, line_cache));
                }
                ParsingLogType::GitalyJson => {
                    line_cache.push(buff);
                    return Ok((LogType::GitalyJson, line_cache));
                }
                _ => {
                    line_cache.push(buff);
                    return Ok((log_type.into(), line_cache));
                }
            }
        }

        if check_errors
            && (buff.contains("error") || buff.contains("ERROR") || buff.contains("Error"))
        {
            line_cache.push(buff.clone());
        }

        buff.clear();
    }
    Err(Error::new(
        ErrorKind::InvalidInput,
        "Unrecognized log format",
    ))
}

fn check_line(line: &str) -> Option<ParsingLogType> {
    if ApiJsonDurSEvent::parse(line).is_some() {
        Some(ParsingLogType::ApiJsonDurS)
    } else if ApiJsonEvent::parse(line).is_some() {
        Some(ParsingLogType::ApiJson)
    } else if CheckShellApiJsonEvent::parse(line).is_some() {
        Some(ParsingLogType::ShellAPI)
    } else if let Some(event) = CheckShellCommandJsonEvent::parse(line) {
        if event.gl_key_type.is_some() || event.msg == "gitlab-shell: main: executing command" {
            Some(ParsingLogType::ShellSSH)
        } else {
            Some(ParsingLogType::GitalyJson)
        }
    } else if CheckAmbiguousShellGitalyEvent::parse(line).is_some() {
        Some(ParsingLogType::Ambiguous)
    } else if CheckGitalyJsonEvent::parse(line).is_some() {
        Some(ParsingLogType::GitalyJson)
    } else if ProdJsonDurSEvent::parse(line).is_some() {
        Some(ParsingLogType::ProdJsonDurS)
    } else if ProdJsonEvent::parse(line).is_some() {
        Some(ParsingLogType::ProdJson)
    } else if SidekiqJsonDurSEvent::parse(line).is_some() {
        Some(ParsingLogType::SidekiqJsonDurS)
    } else if SidekiqJsonEvent::parse(line).is_some() {
        Some(ParsingLogType::SidekiqJson)
    } else if line.contains("time=") {
        Some(ParsingLogType::GitalyText)
    } else if line.contains("TID-") {
        Some(ParsingLogType::SidekiqText)
    } else {
        None
    }
}

// Valid Shell events may not contain the fields we care about
// This struct contains fields unique to Shell API events that are always present
#[derive(Deserialize)]
#[allow(dead_code)]
struct CheckGitalyJsonEvent {
    time: String,
    level: String,
    msg: String,
}

impl CheckGitalyJsonEvent {
    fn parse(s: &str) -> Option<CheckGitalyJsonEvent> {
        serde_json::from_str::<CheckGitalyJsonEvent>(s).ok()
    }
}

// Valid Shell events may not contain the fields we care about
// This struct contains fields unique to Shell command events that are always present
#[derive(Deserialize)]
#[allow(dead_code)]
struct CheckShellCommandJsonEvent {
    command: String,
    gl_key_type: Option<String>,
    msg: String,
}

impl CheckShellCommandJsonEvent {
    fn parse(s: &str) -> Option<CheckShellCommandJsonEvent> {
        serde_json::from_str::<CheckShellCommandJsonEvent>(s).ok()
    }
}

#[derive(Deserialize)]
#[allow(dead_code)]
struct CheckShellApiJsonEvent {
    content_length_bytes: u32,
}

impl CheckShellApiJsonEvent {
    fn parse(s: &str) -> Option<CheckShellApiJsonEvent> {
        serde_json::from_str::<CheckShellApiJsonEvent>(s).ok()
    }
}

#[derive(Deserialize)]
#[allow(dead_code)]
#[serde(deny_unknown_fields)]
struct CheckAmbiguousShellGitalyEvent {
    level: String,
    msg: String,
    time: String,
}

impl CheckAmbiguousShellGitalyEvent {
    fn parse(s: &str) -> Option<Self> {
        serde_json::from_str::<Self>(s).ok()
    }
}

// TODO add shell check structs here

#[cfg(test)]
mod tests {
    use super::*;
    use ParsingLogType::*;

    #[test]
    fn find_api_json() {
        let json = r##"{"time":"2019-04-10T04:47:01.360Z","severity":"INFO","duration":3.82,"db":1.22,"view":2.5999999999999996,"status":200,"method":"GET","path":"/api/v4/internal/authorized_keys","params":[{"key":"key","value":"[FILTERED]"},{"key":"secret_token","value":"[FILTERED]"}],"host":"127.0.0.1","ip":"127.0.0.1","ua":"Ruby","route":"/api/:version/internal/authorized_keys","gitaly_calls":0,"correlation_id":"8d5c2a7d-66f1-4e0d-917b-ae2a65954bf9"}"##;
        assert_eq!(check_line(json), Some(ApiJson));
    }

    #[test]
    fn find_api_json_dur_s() {
        let json = r##"{"time":"2020-04-23T22:14:20.290Z","severity":"INFO","duration_s":1.46,"db_duration_s":0.09,"view_duration_s":1.37,"status":200,"method":"GET","path":"/api/v4/projects","params":[{"key":"page","value":"2"},{"key":"per_page","value":"100"},{"key":"statistics","value":"true"}],"host":"gitlab.example.com","remote_ip":"10.10.10.10, 10.10.10.10","ua":"go-gitlab","route":"/api/:version/projects","user_id":1,"username":"root","queue_duration_s":0.01,"gitaly_calls":11,"gitaly_duration_s":0.13,"redis_calls":701,"redis_duration_s":0.06,"correlation_id":"u4G996jjXJ1"}"##;
        assert_eq!(check_line(json), Some(ApiJsonDurS));
    }

    #[test]
    fn find_prod_json() {
        let json = r##"{"method":"GET","path":"/group/project.git/info/refs","format":"html","controller":"Projects::GitHttpController","action":"info_refs","status":200,"duration":147.69,"view":0.34,"db":14.5,"time":"2019-04-19T08:10:14.269Z","params":[{"key":"service","value":"git-upload-pack"},{"key":"namespace_id","value":"group"},{"key":"project_id","value":"project.git"}],"remote_ip":"10.10.10.10","user_id":1,"username":"root","ua":"JGit/3.5.3.201412180710-r","queue_duration":null,"correlation_id":"063a9dd3-c4ee-4dbc-a912-948f9b6da880"}"##;
        assert_eq!(check_line(json), Some(ProdJson));
    }

    #[test]
    fn find_prod_json_dur_s() {
        let json = r##"{"method":"GET","path":"/group/project/blob/0000000000000000000000000000000000000001/src/__pycache__/__init__.cpython-35.pyc","format":"*/*","controller":"Projects::BlobController","action":"show","status":200,"time":"2020-04-24T04:54:41.461Z","params":[{"key":"namespace_id","value":"group"},{"key":"project_id","value":"project"},{"key":"id","value":"0000000000000000000000000000000000000001/io/__pycache__/__init__.cpython-35.pyc"}],"remote_ip":"10.10.10.10","user_id":null,"username":null,"ua":"Mozilla/5.0 (compatible; )","queue_duration_s":0.01,"gitaly_calls":2,"gitaly_duration_s":0.02,"rugged_calls":5,"rugged_duration_s":0.01,"redis_calls":26,"redis_duration_s":0.0,"correlation_id":"IratXOPyxi6","cpu_s":0.12,"db_duration_s":0.01,"view_duration_s":0.08,"duration_s":0.14}"##;
        assert_eq!(check_line(json), Some(ProdJsonDurS));
    }

    #[test]
    fn find_gitaly_text() {
        let text = r##"2019-02-01_11:30:20.13239 time="2019-02-01T11:30:20Z" level=info msg="finished streaming call with code OK" grpc.code=OK grpc.meta.auth_version=v2 grpc.method=InfoRefsUploadPack grpc.request.fullMethod=/gitaly.SmartHTTPService/InfoRefsUploadPack grpc.request.glRepository=project-1 grpc.request.repoPath=group/project.git grpc.request.repoStorage=default grpc.request.topLevelGroup=group grpc.service=gitaly.SmartHTTPService grpc.start_time="2019-02-01T11:30:19Z" grpc.time_ms=322.1 peer.address=@ span.kind=server system=grpc"##;
        assert_eq!(check_line(text), Some(GitalyText));
    }

    #[test]
    fn find_gitaly_json() {
        let json = r##"{"correlation_id":"KikVy1oq2R1","grpc.code":"OK","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-sidekiq","grpc.method":"FindCommit","grpc.request.deadline":"2019-05-10T09:19:49Z","grpc.request.fullMethod":"/gitaly.CommitService/FindCommit","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-1","grpc.request.repoPath":"@hashed/aa/bb/0000000000000000000000000000000000000000000000000000000000000001.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.CommitService","grpc.start_time":"2019-05-10T08:57:18Z","grpc.time_ms":3.373,"level":"info","msg":"finished unary call with code OK","peer.address":"10.10.10.10:50000","span.kind":"server","system":"grpc","time":"2019-05-10T08:57:18Z"}"##;
        assert_eq!(check_line(json), Some(GitalyJson));
    }

    #[test]
    fn find_sidekiq_text() {
        let text = r##"2019-03-17_02:53:25.22981 2019-03-17T02:53:25.229Z 129639 TID-otfgm18w3 Gitlab::GithubImport::ImportPullRequestWorker JID-c73ed50847d6b5746e45d913 INFO: done: 289.24 sec"##;
        assert_eq!(check_line(text), Some(SidekiqText));
    }

    #[test]
    fn find_sidekiq_text_no_dur() {
        let text = r##"2019-03-17_02:52:57.56595 2019-03-17T02:52:57.565Z 129639 TID-otfw8x097 PipelineUpdateWorker JID-86d85e6a53849b3413e21c0c INFO: start"##;
        assert_eq!(check_line(text), Some(SidekiqText));
    }

    #[test]
    fn find_sidekiq_json() {
        let json = r##"{"severity":"INFO","time":"2020-04-23T05:19:04.613Z","queue":"cronjob:elastic_index_bulk_cron","class":"ElasticIndexBulkCronWorker","retry":false,"queue_namespace":"cronjob","jid":"1fabed514650adbc9f088be2","created_at":"2020-04-23T05:19:04.557Z","correlation_id":"8ce2d6b874090d8994419d3a921ed457","enqueued_at":"2020-04-23T05:19:04.557Z","pid":29039,"message":"ElasticIndexBulkCronWorker JID-1fabed514650adbc9f088be2: done: 0.052651 sec","job_status":"done","scheduling_latency_s":0.003145,"duration":0.052651,"cpu_s":0.003783,"completed_at":"2020-04-23T05:19:04.613Z","db_duration":0,"db_duration_s":0}"##;
        assert_eq!(check_line(json), Some(SidekiqJson));
    }

    #[test]
    fn no_match_sidekiq_json_no_dur() {
        let json = r##"{"severity":"INFO","time":"2020-04-23T05:19:04.560Z","queue":"cronjob:elastic_index_bulk_cron","class":"ElasticIndexBulkCronWorker","retry":false,"queue_namespace":"cronjob","jid":"1fabed514650adbc9f088be2","created_at":"2020-04-23T05:19:04.557Z","correlation_id":"8ce2d6b874090d8994419d3a921ed457","enqueued_at":"2020-04-23T05:19:04.557Z","pid":29039,"message":"ElasticIndexBulkCronWorker JID-1fabed514650adbc9f088be2: start","job_status":"start","scheduling_latency_s":0.003145}"##;
        assert_eq!(check_line(json), None);
    }

    #[test]
    fn find_sidekiq_json_dur_s() {
        let json = r##"{"severity":"INFO","time":"2020-05-08T21:39:40.012Z","class":"BuildFinishedWorker","args":["1"],"retry":3,"queue":"pipeline_processing:build_finished","queue_namespace":"pipeline_processing","jid":"2e2703334a8a5cbbb4463207","created_at":"2020-05-08T21:39:39.938Z","correlation_id":"aIt1a0yfTb2","enqueued_at":"2020-05-08T21:39:39.938Z","pid":559,"message":"BuildFinishedWorker JID-2e2703334a8a5cbbb4463207: done: 0.073214 sec","job_status":"done","scheduling_latency_s":0.000773,"redis_calls":6,"redis_duration_s":0.000809,"duration_s":0.073214,"cpu_s":0.034583,"completed_at":"2020-05-08T21:39:40.012Z","db_duration_s":0.030742}"##;
        assert_eq!(check_line(json), Some(SidekiqJsonDurS));
    }

    #[test]
    fn no_match_sidekiq_json_dur_s_no_dur() {
        let json = r##"{"severity":"INFO","time":"2020-05-08T21:39:39.604Z","class":"BuildQueueWorker","args":["1"],"retry":3,"queue":"pipeline_processing:build_queue","queue_namespace":"pipeline_processing","jid":"1de523f594c9ee7417a1675c","created_at":"2020-05-08T21:39:39.602Z","correlation_id":"QnCYtQTJ5O1","enqueued_at":"2020-05-08T21:39:39.603Z","pid":557,"message":"BuildQueueWorker JID-1de523f594c9ee7417a1675c: start","job_status":"start","scheduling_latency_s":0.000836}"##;
        assert_eq!(check_line(json), None);
    }

    #[test]
    fn match_shell_first_line_api() {
        let json = r##"{"content_length_bytes":512,"correlation_id":"01FFGNJ5XRAD0VFDCWVFZFPQ6B","duration_ms":77,"level":"info","method":"GET","msg":"Finished HTTP request","status":200,"time":"2021-09-14T00:48:37+02:00","url":"http://unix/dvcs/api/v4/internal/authorized_keys?key=AAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"}
{"command":"git-upload-pack","correlation_id":"01FFGNJ6HSQ8CBG4CGQK1P5BR1","git_protocol":"","gl_key_id":11,"gl_key_type":"deploy_key","gl_project_path":"group/project","gl_repository":"project-59","level":"info","msg":"executing git command","remote_ip":"10.240.219.10","time":"2021-09-14T00:48:38+02:00","user_id":"user-11","username":"myuser"}"##;
        let (log_type, _) = check_log_type(json.as_bytes(), false).unwrap();
        assert_eq!(log_type, LogType::ShellJson);
    }

    #[test]
    fn match_shell_ssh() {
        let json = r##"{"command":"git-upload-pack","correlation_id":"01FFGNJ6HSQ8CBG4CGQK1P5BR1","git_protocol":"","gl_key_id":11,"gl_key_type":"deploy_key","gl_project_path":"group/project","gl_repository":"project-59","level":"info","msg":"executing git command","remote_ip":"10.240.219.10","time":"2021-09-14T00:48:38+02:00","user_id":"user-11","username":"myuser"}"##;
        assert_eq!(check_line(json), Some(ShellSSH));
    }

    #[test]
    fn match_shell_command() {
        let json = r##"{"command":"*uploadpack.Command","correlation_id":"01FQ2JR69874QYWGJTPWXRTB56","env":{"GitProtocolVersion":"","IsSSHConnection":true,"OriginalCommand":"git-upload-pack 'root/broken-repo.git'","RemoteAddr":"65.60.175.130"},"level":"info","msg":"gitlab-shell: main: executing command","time":"2021-12-16T21:35:41Z"}"##;
        assert_eq!(check_line(json), Some(ShellSSH));
    }

    #[test]
    fn no_match_gibberish() {
        let text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
        assert_eq!(check_line(text), None);
    }
}
