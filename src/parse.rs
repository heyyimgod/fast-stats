use crate::data::RawOutput;
use crate::event::Event;
use crate::event::*;
use crate::log_type::LogType;
use batcher::{Batcher, LineBatch};

use crossbeam_channel::bounded as bounded_channel;
use crossbeam_channel::{Receiver, Sender};
use std::io::prelude::*;
use std::thread::{self, JoinHandle};

const BATCH_SIZE: usize = 50;
const QUEUE_DEPTH: usize = 128;

#[derive(Clone, PartialEq, Debug)]
pub struct ParseArgs {
    pub search_for: Option<String>,
    pub thread_ct: usize,
    pub line_cache: Vec<String>,
}

impl ParseArgs {
    pub fn new(search_for: Option<String>, thread_ct: usize, line_cache: Vec<String>) -> ParseArgs {
        ParseArgs {
            search_for,
            thread_ct,
            line_cache,
        }
    }
}

pub fn parse<B, O>(log_t: LogType, reader: B, args: ParseArgs) -> O
where
    B: BufRead,
    O: RawOutput + 'static,
{
    if args.thread_ct <= 1 {
        parse_st::<B, O>(log_t, reader, args)
    } else {
        parse_mt::<B, O>(log_t, reader, args)
    }
}

fn parse_st<B, O>(log_t: LogType, mut reader: B, args: ParseArgs) -> O
where
    B: BufRead,
    O: RawOutput,
{
    let mut data_map = O::new(log_t);

    for event in args.line_cache {
        parse_line(log_t, &event, &mut data_map);
    }

    let mut buf = String::with_capacity(1024);
    while let Ok(ct) = reader.read_line(&mut buf) {
        if ct == 0 {
            break;
        }

        parse_line(log_t, &buf, &mut data_map);
        buf.clear();
    }

    data_map
}

fn parse_mt<B, O>(log_t: LogType, reader: B, args: ParseArgs) -> O
where
    B: BufRead,
    O: RawOutput + 'static,
{
    let (tx, rx): (Sender<LineBatch>, Receiver<_>) = bounded_channel(QUEUE_DEPTH);
    let handles = spawn_threads::<O>(log_t, args.thread_ct, rx);

    tx.send(args.line_cache.into())
        .expect("Failed to send to input channel");

    let mut batcher = Batcher::<_>::new(BATCH_SIZE, reader);

    for batch in batcher.iter() {
        let b = match batch {
            Ok(b) => b,
            Err(e) => panic!("Failed to read: {}", e),
        };
        tx.send(b).expect("Failed to send to input channel");
    }

    drop(tx);

    handles
        .into_iter()
        .filter_map(|h| h.join().ok())
        .fold(O::new(log_t), |map, thd_map| map.coalesce(thd_map))
}

fn spawn_threads<O>(log_t: LogType, ct: usize, rx: Receiver<LineBatch>) -> Vec<JoinHandle<O>>
where
    O: RawOutput + 'static,
{
    (0..ct - 1)
        .map(|_| {
            let rx_clone = rx.clone();
            thread::spawn(move || receive_lines::<O>(log_t, rx_clone))
        })
        .collect()
}

fn receive_lines<O>(log_t: LogType, rx_input: Receiver<LineBatch>) -> O
where
    O: RawOutput,
{
    let mut local_map = O::new(log_t);

    rx_input.iter().for_each(|batch| {
        for line in batch.iter() {
            parse_line(log_t, line, &mut local_map);
        }
    });

    local_map
}

fn parse_line<O: RawOutput>(log_t: LogType, s: &str, map: &mut O) {
    use LogType::*;

    match log_t {
        ApiJson => {
            if let Some(event) = ApiJsonEvent::parse(s) {
                map.insert_event(event);
            }
        }
        ApiJsonDetail => {
            if let Some(event) = ApiJsonEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        ApiJsonError => {
            if let Some(event) = ApiJsonEventError::parse(s) {
                map.insert_event(event);
            }
        }
        ApiJsonDurS => {
            if let Some(event) = ApiJsonDurSEvent::parse(s) {
                map.insert_event(event);
            }
        }
        ApiJsonDurSDetail => {
            if let Some(event) = ApiJsonDurSEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        ApiJsonDurSError => {
            if let Some(event) = ApiJsonDurSEventError::parse(s) {
                map.insert_event(event);
            }
        }
        GitalyText => {
            if let Some(event) = GitalyEvent::parse(s) {
                map.insert_event(event);
            }
        }
        GitalyJson => {
            if let Some(event) = GitalyJsonEvent::parse(s) {
                map.insert_event(event);
            }
        }
        GitalyJsonDetail => {
            if let Some(event) = GitalyJsonEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        GitalyJsonError => {
            if let Some(event) = GitalyJsonEventError::parse(s) {
                map.insert_event(event);
            }
        }
        ProdJson => {
            if let Some(event) = ProdJsonEvent::parse(s) {
                map.insert_event(event);
            }
        }
        ProdJsonDetail => {
            if let Some(event) = ProdJsonEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        ProdJsonError => {
            if let Some(event) = ProdJsonEventError::parse(s) {
                map.insert_event(event);
            }
        }
        ProdJsonDurS => {
            if let Some(event) = ProdJsonDurSEvent::parse(s) {
                map.insert_event(event);
            }
        }
        ProdJsonDurSDetail => {
            if let Some(event) = ProdJsonDurSEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        ProdJsonDurSError => {
            if let Some(event) = ProdJsonDurSEventError::parse(s) {
                map.insert_event(event);
            }
        }
        ShellJson => {
            if let Some(event) = ShellJsonEvent::parse(s) {
                map.insert_event(event);
            }
        }
        SidekiqText => {
            if let Some(event) = SidekiqEvent::parse(s) {
                map.insert_event(event);
            }
        }
        SidekiqJson => {
            if let Some(event) = SidekiqJsonEvent::parse(s) {
                map.insert_event(event);
            }
        }
        SidekiqJsonDetail => {
            if let Some(event) = SidekiqJsonEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        SidekiqJsonError => {
            if let Some(event) = SidekiqJsonEventError::parse(s) {
                map.insert_event(event);
            }
        }
        SidekiqJsonDurS => {
            if let Some(event) = SidekiqJsonDurSEvent::parse(s) {
                map.insert_event(event);
            }
        }
        SidekiqJsonDurSDetail => {
            if let Some(event) = SidekiqJsonDurSEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        SidekiqJsonDurSError => {
            if let Some(event) = SidekiqJsonDurSEventError::parse(s) {
                map.insert_event(event);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::execute::Reader;
    use crate::field::StatField;
    use crate::stats::*;

    use std::fs::File;
    use std::io::BufReader;

    #[test]
    fn calc_st() {
        let input_file = File::open("test/api.log").unwrap();
        let reader = Reader::File(BufReader::new(input_file));
        let args = ParseArgs::new(None, 1, Vec::new());

        let stats_v = LogType::ApiJsonDurS.calculate_stats(reader, args, StatField::Count);

        assert_eq!(stats_v.stats_t, StatsType::Api);
        assert_eq!(stats_v.stats.len(), 2);
        assert_eq!(
            stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec!["/api/:version/projects", "/api/:version/users"]
        );
        assert_eq!(
            stats_v.stats.iter().map(|s| s.count).collect::<Vec<_>>(),
            vec![6, 1],
        );
        assert_eq!(
            stats_v.stats.iter().map(|s| s.max).collect::<Vec<_>>(),
            vec![1560.0, 550.0],
        );
        assert_eq!(
            stats_v
                .stats
                .iter()
                .map(|s| s.perc_failed)
                .collect::<Vec<_>>(),
            vec![0.0, 0.0],
        );
    }

    #[test]
    fn calc_mt() {
        let input_file = File::open("test/api.log").unwrap();
        let reader = Reader::File(BufReader::new(input_file));
        let args = ParseArgs::new(None, 4, Vec::new());

        let stats_v = LogType::ApiJsonDurS.calculate_stats(reader, args, StatField::Count);

        assert_eq!(stats_v.stats_t, StatsType::Api);
        assert_eq!(stats_v.stats.len(), 2);
        assert_eq!(
            stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec!["/api/:version/projects", "/api/:version/users"]
        );
        assert_eq!(
            stats_v.stats.iter().map(|s| s.count).collect::<Vec<_>>(),
            vec![6, 1],
        );
        assert_eq!(
            stats_v.stats.iter().map(|s| s.max).collect::<Vec<_>>(),
            vec![1560.0, 550.0],
        );
        assert_eq!(
            stats_v
                .stats
                .iter()
                .map(|s| s.perc_failed)
                .collect::<Vec<_>>(),
            vec![0.0, 0.0],
        );
    }

    #[test]
    fn calc_st_eq_calc_mt() {
        let st_input_file = File::open("test/api.log").unwrap();
        let st_reader = Reader::File(BufReader::new(st_input_file));
        let st_args = ParseArgs::new(None, 1, Vec::new());

        let mt_input_file = File::open("test/api.log").unwrap();
        let mt_reader = Reader::File(BufReader::new(mt_input_file));
        let mt_args = ParseArgs::new(None, 4, Vec::new());

        assert_eq!(
            LogType::ApiJsonDurS.calculate_stats(st_reader, st_args, StatField::Count),
            LogType::ApiJsonDurS.calculate_stats(mt_reader, mt_args, StatField::Count),
        );
    }
}
