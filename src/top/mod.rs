use crate::field::EventField;
pub use data::ProdData;

mod data;
mod stats;

const SPACER: &str = "   ";

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum DisplayFmt {
    Percentage,
    Value,
    Both,
}

impl From<&str> for DisplayFmt {
    fn from(s: &str) -> DisplayFmt {
        use DisplayFmt::*;

        match s {
            "perc" => Percentage,
            "value" => Value,
            "both" => Both,
            _ => unreachable!(),
        }
    }
}

impl Default for DisplayFmt {
    fn default() -> DisplayFmt {
        DisplayFmt::Both
    }
}

#[derive(Clone, Default, PartialEq, Debug)]
pub struct FieldLens {
    count: usize,
    rps: usize,
    dur: usize,
    db: usize,
    redis: usize,
    gitaly: usize,
    rugged: usize,
    queue: usize,
    cpu: usize,
    fails: usize,
}

impl FieldLens {
    const PERC_LEN: usize = 6; // "100.00%"
    const SEPARATOR_LEN: usize = 3; // " / "

    pub fn insert(&mut self, field: EventField, len: usize) {
        match field {
            EventField::Count => self.count = len,
            EventField::Rps => self.rps = len,
            EventField::Duration => self.dur = len,
            EventField::Db => self.db = len,
            EventField::Redis => self.redis = len,
            EventField::Gitaly => self.gitaly = len,
            EventField::Rugged => self.rugged = len,
            EventField::Queue => self.queue = len,
            EventField::CpuS => self.cpu = len,
            EventField::FailCt => self.fails = len,
        }
    }

    pub fn get(&self, field: EventField) -> usize {
        match field {
            EventField::Count => self.count,
            EventField::Rps => self.rps,
            EventField::Duration => self.dur,
            EventField::Db => self.db,
            EventField::Redis => self.redis,
            EventField::Gitaly => self.gitaly,
            EventField::Rugged => self.rugged,
            EventField::Queue => self.queue,
            EventField::CpuS => self.cpu,
            EventField::FailCt => self.fails,
        }
    }

    pub fn fmt_len(&self, field: EventField, fmt: DisplayFmt) -> usize {
        match fmt {
            DisplayFmt::Percentage => Self::PERC_LEN,
            DisplayFmt::Value => self.get(field),
            DisplayFmt::Both => self.get(field) + Self::SEPARATOR_LEN + Self::PERC_LEN,
        }
    }

    pub fn min_fmt_len(&self, field: EventField, fmt: DisplayFmt) -> usize {
        self.fmt_len(field, fmt).max(field.min_len())
    }
}
