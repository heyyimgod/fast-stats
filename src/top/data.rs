use crate::data::detail::DetailTimeEvent;
use crate::data::RawOutput;
use crate::event::Event;
use crate::field::EventField;
use crate::log_type::LogType;
use crate::print::fmt_dur;
use crate::stats::StatsType;
use crate::stats_vec::PrintFormat;
use crate::top::stats::Stats;
use crate::top::{DisplayFmt, FieldLens, SPACER};
use crate::{HashMap, HashSet};

use rayon::prelude::*;
use serde::Serialize;
use std::io::{self, prelude::*};

const DEFAULT_LIMIT: usize = 10;
const DEFAULT_LEN: usize = 10;

const MANDATORY_FIELDS_START: &[EventField] =
    &[EventField::Count, EventField::Rps, EventField::Duration];
const OPTIONAL_FIELDS: &[EventField] = &[
    EventField::Db,
    EventField::Redis,
    EventField::Gitaly,
    EventField::Rugged,
    EventField::Queue,
    EventField::CpuS,
];
const MANDATORY_FIELDS_END: &[EventField] = &[EventField::FailCt];

#[derive(Clone, PartialEq, Serialize, Debug)]
pub enum ColumnType {
    Path,
    Project,
    User,
    Client,
}

impl ColumnType {
    fn name(&self) -> &'static str {
        use ColumnType::*;

        match self {
            Path => "PATH",
            Project => "PROJECT",
            User => "USER",
            Client => "CLIENT",
        }
    }

    fn desc(&self) -> &'static str {
        use ColumnType::*;

        match self {
            Path => "Path",
            Project => "Project",
            User => "User",
            Client => "Client",
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
struct PrintArgs {
    fmt: DisplayFmt,
    item_len: usize,
    ct: usize,
    sort_by: EventField,
    lens: FieldLens,
}

impl PrintArgs {
    pub fn new(
        fmt: DisplayFmt,
        item_len: usize,
        ct: usize,
        sort_by: EventField,
        lens: FieldLens,
    ) -> PrintArgs {
        PrintArgs {
            fmt,
            item_len,
            ct,
            sort_by,
            lens,
        }
    }
}

#[derive(PartialEq, Serialize, Debug)]
pub struct ProdData {
    pub stats_t: StatsType,
    pub path_stats: StatsMap,
    pub project_stats: StatsMap,
    pub user_stats: StatsMap,
    pub client_stats: StatsMap,
    pub totals: Stats,
    pub start_time: i64,
    pub end_time: i64,
}

#[derive(Clone, PartialEq, Serialize, Debug)]
pub struct StatsMap {
    pub stats: HashMap<String, Stats>,
    col_type: ColumnType,
}

impl RawOutput for ProdData {
    fn new(log_t: LogType) -> ProdData {
        let stats_t = log_t.stats_t();

        ProdData {
            stats_t,
            path_stats: StatsMap::new(ColumnType::Path),
            project_stats: StatsMap::new(ColumnType::Project),
            user_stats: StatsMap::new(ColumnType::User),
            client_stats: StatsMap::new(ColumnType::Client),
            totals: Stats::default(),
            start_time: i64::MAX,
            end_time: i64::MIN,
        }
    }

    fn insert_event<'a, T: Event<'a>>(&mut self, event: T) {
        self.insert_path(&event);
        self.insert_project(&event);
        self.insert_user(&event);
        self.insert_client(&event);
        self.update_total(&event);

        if let Some(ts) = T::parse_time(event.raw_time()).map(|t| t.timestamp()) {
            self.update_times(ts);
        }
    }

    fn coalesce(mut self, other: ProdData) -> ProdData {
        self.path_stats.extend(other.path_stats);
        self.project_stats.extend(other.project_stats);
        self.user_stats.extend(other.user_stats);
        self.client_stats.extend(other.client_stats);
        self.totals.add(&other.totals);

        if other.start_time < self.start_time {
            self.start_time = other.start_time;
        }

        if other.end_time > self.end_time {
            self.end_time = other.end_time;
        }

        self
    }
}

impl ProdData {
    fn insert_path<'a, T: Event<'a>>(&mut self, event: &T) {
        if let Some(path) = event.path() {
            self.path_stats
                .stats
                .entry(path.to_string())
                .or_insert_with(Stats::default)
                .add_event(event);
        }
    }

    fn insert_project<'a, T: Event<'a>>(&mut self, event: &T) {
        if let Some(proj) = event.project() {
            self.project_stats
                .stats
                .entry(proj.to_string())
                .or_insert_with(Stats::default)
                .add_event(event);
        }
    }

    fn insert_user<'a, T: Event<'a>>(&mut self, event: &T) {
        if let Some(u) = event.user() {
            self.user_stats
                .stats
                .entry(u.to_string())
                .or_insert_with(Stats::default)
                .add_event(event);
        }
    }

    fn insert_client<'a, T: Event<'a>>(&mut self, event: &T) {
        if let Some(c) = event.client_type() {
            self.client_stats
                .stats
                .entry(c.to_string())
                .or_insert_with(Stats::default)
                .add_event(event);
        }
    }

    fn update_total<'a, T: Event<'a>>(&mut self, event: &T) {
        self.totals.add_event(event);
    }

    fn insert_path_detail(&mut self, event: &DetailTimeEvent) {
        if let Some(path) = event.path.as_deref() {
            self.path_stats
                .stats
                .entry(path.to_string())
                .or_insert_with(Stats::default)
                .add_detail_event(event);
        }
    }

    fn insert_project_detail(&mut self, event: &DetailTimeEvent) {
        if let Some(proj) = event.project.as_deref() {
            self.project_stats
                .stats
                .entry(proj.to_string())
                .or_insert_with(Stats::default)
                .add_detail_event(event);
        }
    }

    fn insert_user_detail(&mut self, event: &DetailTimeEvent) {
        if let Some(u) = event.user.as_deref() {
            self.user_stats
                .stats
                .entry(u.to_string())
                .or_insert_with(Stats::default)
                .add_detail_event(event);
        }
    }

    fn insert_client_detail(&mut self, event: &DetailTimeEvent) {
        if let Some(c) = event.client.as_deref() {
            self.client_stats
                .stats
                .entry(c.to_string())
                .or_insert_with(Stats::default)
                .add_detail_event(event);
        }
    }

    fn update_total_detail(&mut self, event: &DetailTimeEvent) {
        self.totals.add_detail_event(event);
    }

    fn update_times(&mut self, ts: i64) {
        if ts < self.start_time {
            self.start_time = ts;
        }

        if ts > self.end_time {
            self.end_time = ts;
        }
    }

    pub fn print(
        &self,
        sort_by: EventField,
        fmt: DisplayFmt,
        print_fmt: PrintFormat,
        limit: Option<usize>,
    ) -> io::Result<()> {
        let stdout = io::stdout();
        let out = stdout.lock();

        match print_fmt {
            PrintFormat::Json => self.print_json(out),
            _ => self.print_text(out, sort_by, fmt, limit),
        }
    }

    pub fn print_text(
        &self,
        mut out: impl Write,
        sort_by: EventField,
        fmt: DisplayFmt,
        limit: Option<usize>,
    ) -> io::Result<()> {
        let paths = self.path_stats.to_sorted(sort_by);
        let projects = self.project_stats.to_sorted(sort_by);
        let users = self.user_stats.to_sorted(sort_by);
        let clients = self.client_stats.to_sorted(sort_by);
        let sorted_stats = [&paths, &projects, &users, &clients];

        let ct = limit.unwrap_or(DEFAULT_LIMIT);
        let item_len = max_item_len(&sorted_stats, ct);
        let all_lens = self.calc_dur_lens(&sorted_stats, ct);
        let args = PrintArgs::new(fmt, item_len, ct, sort_by, all_lens);

        self.print_totals(&mut out)?;
        self.print_stats(
            &mut out,
            &args,
            &self.path_stats.present_fields(),
            &self.path_stats,
            &paths,
        )?;
        self.print_stats(
            &mut out,
            &args,
            &self.project_stats.present_fields(),
            &self.project_stats,
            &projects,
        )?;
        self.print_stats(
            &mut out,
            &args,
            &self.user_stats.present_fields(),
            &self.user_stats,
            &users,
        )?;
        self.print_stats(
            &mut out,
            &args,
            &self.client_stats.present_fields(),
            &self.client_stats,
            &clients,
        )?;

        Ok(())
    }

    pub fn from_detail_events<'a>(
        stats_t: StatsType,
        events: impl IntoIterator<Item = &'a DetailTimeEvent>,
    ) -> ProdData {
        let other_type = match stats_t {
            StatsType::Gitaly => ColumnType::Client,
            _ => ColumnType::User,
        };

        let mut out = ProdData {
            stats_t,
            path_stats: StatsMap::new(ColumnType::Path),
            project_stats: StatsMap::new(ColumnType::Project),
            user_stats: StatsMap::new(ColumnType::User),
            client_stats: StatsMap::new(other_type),
            totals: Stats::default(),
            start_time: i64::MAX,
            end_time: i64::MIN,
        };

        for event in events {
            out.insert_path_detail(event);
            out.insert_project_detail(event);
            out.insert_user_detail(event);
            out.insert_client_detail(event);
            out.update_total_detail(event);
            out.update_times(event.time.timestamp_millis());
        }

        out
    }

    // We don't know the total duration until after all events have been collected.
    // This needs to be run after the StatsMap is complete to populate the seconds
    // field.
    pub fn insert_session_duration(&mut self) {
        let dur = self.end_time - self.start_time;

        for stats in self.path_stats.stats.values_mut() {
            stats.seconds = dur;
        }
        for stats in self.project_stats.stats.values_mut() {
            stats.seconds = dur;
        }
        for stats in self.user_stats.stats.values_mut() {
            stats.seconds = dur;
        }
        for stats in self.client_stats.stats.values_mut() {
            stats.seconds = dur;
        }
        self.totals.seconds = dur;
    }

    fn print_totals(&self, mut out: impl Write) -> io::Result<()> {
        writeln!(&mut out, "Totals\n")?;

        writeln!(&mut out, "COUNT:   {}", self.totals.count)?;
        writeln!(
            &mut out,
            "RPS:     {:.2}",
            self.totals.count as f64 / (self.end_time - self.start_time) as f64
        )?;
        writeln!(&mut out, "DUR:     {}", fmt_dur(self.totals.duration))?;

        if self.totals.db_duration > 0.0 {
            writeln!(&mut out, "DB:      {}", fmt_dur(self.totals.db_duration))?;
        }
        if self.totals.redis_duration > 0.0 {
            writeln!(&mut out, "REDIS:   {}", fmt_dur(self.totals.redis_duration))?;
        }
        if self.totals.gitaly_duration > 0.0 {
            writeln!(
                &mut out,
                "GITALY:  {}",
                fmt_dur(self.totals.gitaly_duration)
            )?;
        }
        if self.totals.rugged_duration > 0.0 {
            writeln!(
                &mut out,
                "RUGGED:  {}",
                fmt_dur(self.totals.rugged_duration)
            )?;
        }
        if self.totals.queue_duration > 0.0 {
            writeln!(&mut out, "QUEUE:   {}", fmt_dur(self.totals.queue_duration))?;
        }
        if self.totals.cpu_s > 0.0 {
            writeln!(&mut out, "CPU:     {}", fmt_dur(self.totals.cpu_s))?;
        }
        if self.totals.fails > 0 {
            writeln!(&mut out, "FAIL_CT: {}", self.totals.fails)?;
        }

        Ok(())
    }

    fn print_stats(
        &self,
        mut out: impl Write,
        args: &PrintArgs,
        fields: &[EventField],
        stats_map: &StatsMap,
        stats: &[(&String, &Stats)],
    ) -> io::Result<()> {
        if stats_map.stats.is_empty() {
            return Ok(());
        }

        stats_map.print_header(&mut out, args, fields)?;
        for (item, stats) in stats.iter().take(args.ct) {
            writeln!(
                out,
                "{:<len$}{}",
                item,
                stats.fmt(args.fmt, fields, &self.totals, &args.lens),
                len = args.item_len
            )?;
        }

        Ok(())
    }

    fn print_json(&self, mut out: impl Write) -> io::Result<()> {
        let serialized = serde_json::to_string(&self).expect("failed to serialize top data");
        writeln!(out, "{}", serialized)
    }

    fn calc_dur_lens(&self, maps: &[&Vec<(&String, &Stats)>], ct: usize) -> FieldLens {
        let mut lens = FieldLens::default();

        for field in self.all_present_fields() {
            let len = max_field_len(maps, ct, field);
            lens.insert(field, len);
        }

        lens
    }

    fn all_present_fields(&self) -> HashSet<EventField> {
        let mut all_present_fields = HashSet::new();

        all_present_fields.extend(self.path_stats.present_fields());
        all_present_fields.extend(self.project_stats.present_fields());
        all_present_fields.extend(self.user_stats.present_fields());
        all_present_fields.extend(self.client_stats.present_fields());

        all_present_fields
    }
}

impl StatsMap {
    pub fn new(col_type: ColumnType) -> StatsMap {
        StatsMap {
            col_type,
            stats: HashMap::new(),
        }
    }

    pub fn extend(&mut self, other: StatsMap) {
        for (key, data) in other.stats.into_iter() {
            self.stats
                .entry(key)
                .or_insert_with(Stats::default)
                .add(&data);
        }
    }

    pub fn to_sorted(&self, sort_by: EventField) -> Vec<(&String, &Stats)> {
        use EventField::*;

        let mut stats: Vec<_> = self.stats.iter().collect();

        match sort_by {
            Count | Rps => stats.par_sort_by(|x, y| (y.1.count).cmp(&x.1.count)),
            Duration => stats.par_sort_by(|x, y| {
                (y.1.duration)
                    .partial_cmp(&x.1.duration)
                    .expect("unable to sort durations")
            }),
            Db => stats.par_sort_by(|x, y| {
                (y.1.db_duration)
                    .partial_cmp(&x.1.db_duration)
                    .expect("unable to sort db durations")
            }),
            Redis => stats.par_sort_by(|x, y| {
                (y.1.redis_duration)
                    .partial_cmp(&x.1.redis_duration)
                    .expect("unable to sort redis durations")
            }),
            Gitaly => stats.par_sort_by(|x, y| {
                (y.1.gitaly_duration)
                    .partial_cmp(&x.1.gitaly_duration)
                    .expect("unable to sort gitaly durations")
            }),
            Rugged => stats.par_sort_by(|x, y| {
                (y.1.rugged_duration)
                    .partial_cmp(&x.1.rugged_duration)
                    .expect("unable to sort rugged durations")
            }),
            Queue => stats.par_sort_by(|x, y| {
                (y.1.queue_duration)
                    .partial_cmp(&x.1.queue_duration)
                    .expect("unable to sort queue durations")
            }),
            CpuS => stats.par_sort_by(|x, y| {
                (y.1.cpu_s)
                    .partial_cmp(&x.1.cpu_s)
                    .expect("unable to sort cpu durations")
            }),
            FailCt => stats.par_sort_by(|x, y| (y.1.fails).cmp(&x.1.fails)),
        }

        stats
    }

    pub fn present_fields(&self) -> Vec<EventField> {
        use EventField::*;

        let mut fields = Vec::new();
        fields.extend_from_slice(MANDATORY_FIELDS_START);

        for field in OPTIONAL_FIELDS {
            match field {
                Count => {
                    if self.stats.values().map(|s| s.count).any(|v| v > 0) {
                        fields.push(Count);
                    }
                }
                Rps => {
                    if self.stats.values().map(|s| s.count).any(|v| v > 0) {
                        fields.push(Rps);
                    }
                }
                Duration => {
                    if self.stats.values().map(|s| s.duration).any(|v| v > 0.0) {
                        fields.push(Duration);
                    }
                }
                Db => {
                    if self.stats.values().map(|s| s.db_duration).any(|v| v > 0.0) {
                        fields.push(Db);
                    }
                }
                Redis => {
                    if self
                        .stats
                        .values()
                        .map(|s| s.redis_duration)
                        .any(|v| v > 0.0)
                    {
                        fields.push(Redis);
                    }
                }
                Gitaly => {
                    if self
                        .stats
                        .values()
                        .map(|s| s.gitaly_duration)
                        .any(|v| v > 0.0)
                    {
                        fields.push(Gitaly);
                    }
                }
                Rugged => {
                    if self
                        .stats
                        .values()
                        .map(|s| s.rugged_duration)
                        .any(|v| v > 0.0)
                    {
                        fields.push(Rugged);
                    }
                }
                Queue => {
                    if self
                        .stats
                        .values()
                        .map(|s| s.queue_duration)
                        .any(|v| v > 0.0)
                    {
                        fields.push(Queue);
                    }
                }
                CpuS => {
                    if self.stats.values().map(|s| s.cpu_s).any(|v| v > 0.0) {
                        fields.push(CpuS);
                    }
                }
                FailCt => {
                    if self.stats.values().map(|s| s.fails).any(|v| v > 0) {
                        fields.push(FailCt);
                    }
                }
            }
        }
        fields.extend_from_slice(MANDATORY_FIELDS_END);

        fields
    }

    fn print_header(
        &self,
        mut out: impl Write,
        args: &PrintArgs,
        fields: &[EventField],
    ) -> io::Result<()> {
        writeln!(
            out,
            "\n\nTop {} {} by {}\n",
            args.ct,
            self.col_type.desc(),
            args.sort_by
        )?;
        let mut hdr = format!("{:<len$}", self.col_type.name(), len = args.item_len);
        for field in fields {
            hdr += &format!(
                "{}{:>len$}",
                SPACER,
                field.header(),
                len = args.lens.min_fmt_len(*field, args.fmt)
            );
        }

        writeln!(out, "{}", hdr)
    }
}

fn max_item_len(maps: &[&Vec<(&String, &Stats)>], ct: usize) -> usize {
    maps.iter()
        .flat_map(|i| i.iter().take(ct))
        .map(|s| s.0.len())
        .max()
        .unwrap_or(DEFAULT_LEN)
}

fn max_field_len(maps: &[&Vec<(&String, &Stats)>], ct: usize, field: EventField) -> usize {
    let max = maps
        .iter()
        .flat_map(|i| i.iter().take(ct))
        .map(|(_, s)| s.get_field(field))
        .max_by(|x, y| {
            x.partial_cmp(y)
                .expect("Failed to sort: invalid comparison")
        })
        .unwrap_or(DEFAULT_LEN as f64);

    if field.is_count() {
        (max as usize).to_string().len()
    } else {
        fmt_dur(max).len()
    }
}
