use super::DisplayFmt;
use crate::data::detail::DetailTimeEvent;
use crate::event::Event;
use crate::field::EventField;
use crate::print::fmt_dur;
use crate::top::{FieldLens, SPACER};

use serde::Serialize;

#[derive(Clone, Default, PartialEq, Serialize, Debug)]
pub struct Stats {
    pub count: usize,
    pub duration: f64,
    pub db_duration: f64,
    pub rugged_duration: f64,
    pub gitaly_duration: f64,
    pub redis_duration: f64,
    pub queue_duration: f64,
    pub cpu_s: f64,
    pub fails: u32,
    pub seconds: i64,
}

impl Stats {
    pub fn add_event<'a, T: Event<'a>>(&mut self, event: &T) {
        self.count += 1;
        self.duration += event.dur_s();
        self.db_duration += event.db().unwrap_or_default();
        self.redis_duration += event.redis().unwrap_or_default();
        self.gitaly_duration += event.gitaly().unwrap_or_default();
        self.rugged_duration += event.rugged().unwrap_or_default();
        self.queue_duration += event.queue().unwrap_or_default();
        self.cpu_s += event.cpu_s().unwrap_or_default();
        if event.status().is_fail() {
            self.fails += 1;
        }
    }

    pub fn add_detail_event(&mut self, event: &DetailTimeEvent) {
        self.count += 1;
        self.duration += event.dur;
        self.db_duration += event.db.unwrap_or_default();
        self.redis_duration += event.redis.unwrap_or_default();
        self.gitaly_duration += event.gitaly.unwrap_or_default();
        self.rugged_duration += event.rugged.unwrap_or_default();
        self.queue_duration += event.queue.unwrap_or_default();
        self.cpu_s += event.cpu_s.unwrap_or_default();
        if event.status.is_fail() {
            self.fails += 1;
        }
    }

    pub fn add(&mut self, other: &Stats) {
        self.count += other.count;
        self.duration += other.duration;
        self.db_duration += other.db_duration;
        self.redis_duration += other.redis_duration;
        self.gitaly_duration += other.gitaly_duration;
        self.rugged_duration += other.rugged_duration;
        self.queue_duration += other.queue_duration;
        self.cpu_s += other.cpu_s;
        self.fails += other.fails;
    }

    pub fn get_field(&self, field: EventField) -> f64 {
        match field {
            EventField::Count => self.count as f64,
            EventField::Rps => self.count as f64 / self.seconds as f64,
            EventField::Duration => self.duration,
            EventField::Db => self.db_duration,
            EventField::Redis => self.redis_duration,
            EventField::Gitaly => self.gitaly_duration,
            EventField::Rugged => self.rugged_duration,
            EventField::Queue => self.queue_duration,
            EventField::CpuS => self.cpu_s,
            EventField::FailCt => self.fails as f64,
        }
    }

    pub fn fmt(
        &self,
        fmt: DisplayFmt,
        fields: &[EventField],
        totals: &Stats,
        lens: &FieldLens,
    ) -> String {
        match fmt {
            DisplayFmt::Percentage => self.fmt_percs(fields, totals, lens),
            DisplayFmt::Value => self.fmt_values(fields, lens),
            DisplayFmt::Both => self.fmt_both(fields, totals, lens),
        }
    }

    fn fmt_both(&self, fields: &[EventField], totals: &Stats, lens: &FieldLens) -> String {
        let mut out = String::new();
        for field in fields {
            out += &format!(
                "{}{:>val_len$} / {:>perc_len$}",
                SPACER,
                self.fmt_value(*field),
                self.fmt_perc(*field, totals),
                val_len = lens.fmt_len(*field, DisplayFmt::Value),
                perc_len = lens.fmt_len(*field, DisplayFmt::Percentage),
            );
        }

        out
    }

    fn fmt_percs(&self, fields: &[EventField], totals: &Stats, lens: &FieldLens) -> String {
        let mut out = String::new();
        for field in fields {
            out += &format!(
                "{}{:>perc_len$}",
                SPACER,
                self.fmt_perc(*field, totals),
                perc_len = lens.min_fmt_len(*field, DisplayFmt::Percentage)
            );
        }

        out
    }

    fn fmt_values(&self, fields: &[EventField], lens: &FieldLens) -> String {
        let mut out = String::new();
        for field in fields {
            out += &format!(
                "{}{:>len$}",
                SPACER,
                self.fmt_value(*field),
                len = lens.min_fmt_len(*field, DisplayFmt::Value)
            );
        }

        out
    }

    fn fmt_perc(&self, field: EventField, totals: &Stats) -> String {
        format!(
            "{:>3.2}%",
            perc(self.get_field(field), totals.get_field(field))
        )
    }

    fn fmt_value(&self, field: EventField) -> String {
        if field.is_count() {
            self.get_field(field).to_string()
        } else if field.is_float() {
            format!("{:.2}", self.get_field(field))
        } else {
            fmt_dur(self.get_field(field))
        }
    }
}

fn perc(event: f64, total: f64) -> f64 {
    if total == 0.0 {
        return 0.0;
    }

    event / total * 100.0
}
