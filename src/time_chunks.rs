use crate::data::detail::{DetailData, DetailMap, DetailTimeEvent, DetailTimeMap};
use crate::data::raw::{RawDataMap, RawTimeDataMap, TimeEvent};
use crate::data::RawOutput;
use crate::event::Status;
use crate::field::{EventField, StatField};
use crate::log_type::LogType;
use crate::stats::StatsType;
use crate::stats_vec::{PrintFormat, StatsVec, VerboseStatsVec};
use crate::top::{DisplayFmt, ProdData};
use crate::util::{BRIGHT_GREEN, RESET};
use crate::HashMap;
use crate::RawData;

use chrono::{prelude::*, Duration};
use rayon::prelude::*;
use std::collections::BTreeMap;
use std::convert::TryFrom;
use std::fmt;
use std::io::{self, stdout, Write};

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Interval {
    length: u64,
    unit: TimeUnit,
}

impl Interval {
    pub fn new(length: u64, unit: TimeUnit) -> Interval {
        Interval { length, unit }
    }

    pub fn seconds(&self) -> i64 {
        (self.length as i64) * self.unit.seconds()
    }

    pub fn as_duration(&self) -> Duration {
        Duration::seconds(self.seconds())
    }
}

impl TryFrom<&str> for Interval {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let err_msg = "WINDOW_LEN must be in <LEN>[h|m|s] format where LEN is a positive 64-bit integer, ex. 30h or 1h";

        if s.len() < 2 || !s.is_ascii() {
            return Err(err_msg);
        }

        let (ct_s, unit_s) = s.split_at(s.len() - 1);

        let unit = TimeUnit::try_from(unit_s)?;

        let ct = ct_s.parse::<u64>().map_err(|_| err_msg)?;

        Ok(Interval::new(ct, unit))
    }
}

impl fmt::Display for Interval {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", self.length, self.unit)
    }
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum TimeUnit {
    Hour,
    Minute,
    Second,
}

impl TimeUnit {
    pub fn seconds(&self) -> i64 {
        match self {
            TimeUnit::Hour => 3_600,
            TimeUnit::Minute => 60,
            TimeUnit::Second => 1,
        }
    }
}

impl fmt::Display for TimeUnit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            TimeUnit::Hour => "hour",
            TimeUnit::Minute => "minute",
            TimeUnit::Second => "second",
        };
        write!(f, "{}", s)
    }
}

impl TryFrom<&str> for TimeUnit {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        match s {
            "h" | "H" => Ok(TimeUnit::Hour),
            "m" | "M" => Ok(TimeUnit::Minute),
            "s" | "S" => Ok(TimeUnit::Second),
            _ => Err("Time unit must be (h)ours, (m)inutes, or (s)econds"),
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct TimeChunks {
    pub data: HashMap<i64, RawDataMap>,
    pub interval: Interval,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
    pub seconds: i64,
    pub stats_t: StatsType,
}

impl TimeChunks {
    pub fn new(
        interval: Interval,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
        log_t: LogType,
    ) -> TimeChunks {
        TimeChunks {
            data: HashMap::default(),
            interval,
            start_time,
            end_time,
            seconds: interval.seconds() as i64,
            stats_t: log_t.stats_t(),
        }
    }

    pub fn from_time_data(
        data_map: RawTimeDataMap,
        interval: Interval,
        log_t: LogType,
    ) -> TimeChunks {
        let mut chunks = TimeChunks::new(interval, data_map.start_time, data_map.end_time, log_t);

        for (key, events) in data_map.data.into_iter() {
            for event in events {
                chunks.insert(log_t, key.clone(), event);
            }
        }

        chunks
    }

    #[inline]
    fn calc_idx(&self, time: DateTime<FixedOffset>) -> i64 {
        let diff = time - self.start_time;
        diff.num_seconds() / self.seconds
    }

    pub fn insert(&mut self, log_t: LogType, key: String, event: TimeEvent) {
        let idx = self.calc_idx(event.time);
        let time_entry = self
            .data
            .entry(idx)
            .or_insert_with(|| RawDataMap::new(log_t));
        let event_entry = time_entry.data.entry(key).or_insert_with(RawData::new);

        event_entry.data.push(event.dur);
        if let Status::Fail = event.status {
            event_entry.fails += 1;
        }
    }

    pub fn into_stats(self, sort_by: StatField) -> TimeStatsMap {
        let mut stats = TimeStatsMap::new(self.interval, self.start_time, self.end_time);

        for (idx, data) in self.data.into_iter() {
            let offset = self.seconds * idx as i64;
            let time = self.start_time + Duration::seconds(offset);

            // handle last batch being less than interval duration
            let dur = if time + self.interval.as_duration() > stats.end_time {
                self.end_time - time
            } else {
                self.interval.as_duration()
            };

            let mut stats_v = StatsVec::from_raw_data(data, dur, self.stats_t);
            stats_v.sort_by(sort_by);

            stats.data.insert(time, stats_v);
        }

        stats
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct DetailTimeChunks {
    pub data: HashMap<i64, DetailMap>,
    pub interval: Interval,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
    pub seconds: i64,
    pub stats_t: StatsType,
}

impl DetailTimeChunks {
    pub fn new(
        interval: Interval,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
        log_t: LogType,
    ) -> DetailTimeChunks {
        DetailTimeChunks {
            data: HashMap::default(),
            interval,
            start_time,
            end_time,
            seconds: interval.seconds() as i64,
            stats_t: log_t.stats_t(),
        }
    }

    pub fn from_time_data(
        data_map: DetailTimeMap,
        interval: Interval,
        log_t: LogType,
    ) -> DetailTimeChunks {
        let mut chunks =
            DetailTimeChunks::new(interval, data_map.start_time, data_map.end_time, log_t);

        for (key, events) in data_map.data.into_iter() {
            for event in events {
                chunks.insert(log_t, &key, event);
            }
        }

        chunks
    }

    #[inline]
    fn calc_idx(&self, time: DateTime<FixedOffset>) -> i64 {
        let diff = time - self.start_time;
        diff.num_seconds() / self.seconds
    }

    pub fn insert(&mut self, log_t: LogType, key: &str, event: DetailTimeEvent) {
        let idx = self.calc_idx(event.time);

        let time_entry = self
            .data
            .entry(idx)
            .or_insert_with(|| DetailMap::new(log_t));
        let (_key, event_entry) = time_entry
            .data
            .raw_entry_mut()
            .from_key(key)
            .or_insert_with(|| (key.to_string(), DetailData::new()));

        event_entry.push(&event);
        if let Status::Fail = event.status {
            event_entry.fails += 1;
        }
    }
}

#[derive(PartialEq, Debug)]
pub struct TimeStatsMap {
    pub data: BTreeMap<DateTime<FixedOffset>, StatsVec>,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
    pub interval: Interval,
}

impl TimeStatsMap {
    pub fn new(
        interval: Interval,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
    ) -> TimeStatsMap {
        TimeStatsMap {
            data: BTreeMap::new(),
            interval,
            start_time,
            end_time,
        }
    }

    pub fn print(
        &self,
        format: PrintFormat,
        fields: &[StatField],
        limit: Option<usize>,
        color: bool,
    ) -> io::Result<()> {
        if matches!(format, PrintFormat::Text) {
            writeln!(
                stdout(),
                "** Splitting input into {} increments **",
                self.interval
            )?;
            writeln!(
                stdout(),
                "\nStart time: {}\nEnd time:   {}\n",
                self.start_time.naive_local(),
                self.end_time.naive_local(),
            )?;
        }

        for (time, stats) in &self.data {
            if color {
                writeln!(
                    stdout(),
                    "\n   {}>>> {} <<<{}\n",
                    BRIGHT_GREEN,
                    time.naive_local(),
                    RESET
                )?;
            } else {
                writeln!(stdout(), "\n   >>> {} <<<\n", time.naive_local())?;
            }

            stats.print(format, fields, limit, self.max_col_len(limit), None)?;
        }
        Ok(())
    }

    fn max_col_len(&self, limit: Option<usize>) -> Option<usize> {
        self.data.values().map(|v| v.primary_col_len(limit)).max()
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct DetailEventTimeChunks {
    pub data: HashMap<i64, DetailTimeMap>,
    pub interval: Interval,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
    pub seconds: i64,
    pub log_t: LogType,
}

impl DetailEventTimeChunks {
    pub fn new(
        interval: Interval,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
        log_t: LogType,
    ) -> DetailEventTimeChunks {
        DetailEventTimeChunks {
            data: HashMap::default(),
            interval,
            start_time,
            end_time,
            seconds: interval.seconds() as i64,
            log_t,
        }
    }

    pub fn from_time_data(
        data_map: DetailTimeMap,
        interval: Interval,
        log_t: LogType,
        sort_by: EventField,
    ) -> DetailEventTimeChunks {
        let mut chunks =
            DetailEventTimeChunks::new(interval, data_map.start_time, data_map.end_time, log_t);

        for (action, events) in data_map.data.into_iter() {
            for event in events {
                chunks.insert(&action, event);
            }
        }

        chunks.data.par_iter_mut().for_each(|(_idx, chunk)| {
            chunk
                .data
                .iter_mut()
                .for_each(|(_action, items)| match sort_by {
                    EventField::Duration => items
                        .sort_by(|x, y| x.dur.partial_cmp(&y.dur).expect("NaN found when sorting")),
                    EventField::Db => items
                        .sort_by(|x, y| x.db.partial_cmp(&y.db).expect("NaN found when sorting")),
                    EventField::Redis => items.sort_by(|x, y| {
                        x.redis
                            .partial_cmp(&y.redis)
                            .expect("NaN found when sorting")
                    }),
                    EventField::Gitaly => items.sort_by(|x, y| {
                        x.gitaly
                            .partial_cmp(&y.gitaly)
                            .expect("NaN found when sorting")
                    }),
                    EventField::Rugged => items.sort_by(|x, y| {
                        x.rugged
                            .partial_cmp(&y.rugged)
                            .expect("NaN found when sorting")
                    }),
                    EventField::CpuS => items.sort_by(|x, y| {
                        x.cpu_s
                            .partial_cmp(&y.cpu_s)
                            .expect("NaN found when sorting")
                    }),
                    EventField::Queue => items.sort_by(|x, y| {
                        x.queue
                            .partial_cmp(&y.queue)
                            .expect("NaN found when sorting")
                    }),
                    _ => {}
                })
        });

        chunks
    }

    pub fn into_top(self) -> TimeTopDataMap {
        let mut top =
            crate::time_chunks::TimeTopDataMap::new(self.interval, self.start_time, self.end_time);

        for (idx, chunk) in self.data.into_iter() {
            let offset = self.seconds * idx as i64;
            let time = self.start_time + Duration::seconds(offset);

            let events: Vec<_> = chunk.data.into_iter().flat_map(|(_k, v)| v).collect();

            let mut data = ProdData::from_detail_events(self.log_t.stats_t(), &events);
            data.insert_session_duration();

            top.data.insert(time, data);
        }

        top
    }

    pub fn into_verbose(self, sort_by: StatField, color: bool) -> TimeVerboseStatsMap {
        let mut verbose = crate::time_chunks::TimeVerboseStatsMap::new(
            self.interval,
            self.start_time,
            self.end_time,
        );

        for (idx, mut chunk) in self.data.into_iter() {
            let offset = self.seconds * idx as i64;
            let time = self.start_time + Duration::seconds(offset);
            chunk.set_times(time, time + Duration::seconds(self.interval.seconds()));

            let stats = chunk.into_verbose_stats_vec(self.log_t.stats_t(), sort_by, color);

            verbose.data.insert(time, stats);
        }

        verbose
    }

    #[inline]
    fn calc_idx(&self, time: DateTime<FixedOffset>) -> i64 {
        let diff = time - self.start_time;
        diff.num_seconds() / self.seconds
    }

    pub fn insert(&mut self, action: &str, event: DetailTimeEvent) {
        let log_t = self.log_t;

        let idx = self.calc_idx(event.time);
        let time_entry = self
            .data
            .entry(idx)
            .or_insert_with(|| DetailTimeMap::new(log_t));
        let action_entry = time_entry
            .data
            .entry(action.to_string())
            .or_insert_with(Vec::new);
        action_entry.push(event);
    }
}

#[derive(PartialEq, Debug)]
pub struct TimeTopDataMap {
    pub data: BTreeMap<DateTime<FixedOffset>, ProdData>,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
    pub interval: Interval,
}

impl TimeTopDataMap {
    pub fn new(
        interval: Interval,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
    ) -> Self {
        TimeTopDataMap {
            data: BTreeMap::new(),
            interval,
            start_time,
            end_time,
        }
    }

    pub fn print(
        &self,
        sort_by: EventField,
        fmt: DisplayFmt,
        print_fmt: PrintFormat,
        limit: Option<usize>,
        color: bool,
    ) -> io::Result<()> {
        if matches!(print_fmt, PrintFormat::Text) {
            writeln!(
                stdout(),
                "** Splitting input into {} increments **",
                self.interval
            )?;
            writeln!(
                stdout(),
                "\nStart time: {}\nEnd time:   {}\n",
                self.start_time.naive_local(),
                self.end_time.naive_local(),
            )?;
        }

        for (time, stats) in &self.data {
            if color {
                writeln!(
                    stdout(),
                    "\n   {}>>> {} <<<{}\n",
                    BRIGHT_GREEN,
                    time.naive_local(),
                    RESET
                )?;
            } else {
                writeln!(stdout(), "\n   >>> {} <<<\n", time.naive_local())?;
            }

            stats.print(sort_by, fmt, print_fmt, limit)?;
        }
        Ok(())
    }
}

#[derive(Debug)]
pub struct TimeVerboseStatsMap {
    pub data: BTreeMap<DateTime<FixedOffset>, VerboseStatsVec>,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
    pub interval: Interval,
}

impl TimeVerboseStatsMap {
    pub fn new(
        interval: Interval,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
    ) -> Self {
        TimeVerboseStatsMap {
            data: BTreeMap::new(),
            interval,
            start_time,
            end_time,
        }
    }

    pub fn print(
        &self,
        fmt: PrintFormat,
        fields: &[StatField],
        limit: Option<usize>,
        color: bool,
    ) -> io::Result<()> {
        if matches!(fmt, PrintFormat::Text) {
            writeln!(
                stdout(),
                "** Splitting input into {} increments **",
                self.interval
            )?;
            writeln!(
                stdout(),
                "\nStart time: {}\nEnd time:   {}\n",
                self.start_time.naive_local(),
                self.end_time.naive_local(),
            )?;
        }

        for (time, stats) in &self.data {
            if color {
                writeln!(
                    stdout(),
                    "\n   {}>>> {} <<<{}\n",
                    BRIGHT_GREEN,
                    time.naive_local(),
                    RESET
                )?;
            } else {
                writeln!(stdout(), "\n   >>> {} <<<\n", time.naive_local())?;
            }

            stats.print(fmt, fields, limit)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::execute::Reader;
    use crate::field::StatField;
    use crate::parse::ParseArgs;

    use std::fs::File;
    use std::io::BufReader;

    #[test]
    fn calc_time_st() {
        let input_file = File::open("test/prod.log").unwrap();
        let reader = Reader::File(BufReader::new(input_file));
        let args = ParseArgs::new(None, 1, Vec::new());
        let int = Interval::new(10, TimeUnit::Minute);

        let time_stats =
            LogType::ProdJsonDurS.calculate_time_stats(reader, int, args, StatField::Count);

        assert_eq!(time_stats.interval, Interval::new(10, TimeUnit::Minute));
        assert_eq!(time_stats.data.len(), 2);

        let first_time = DateTime::parse_from_rfc3339("2020-04-24T07:20:08.004Z").unwrap();
        let first_stats_v = time_stats.data.get(&first_time).unwrap();
        let last_time = DateTime::parse_from_rfc3339("2020-04-24T07:30:08.004Z").unwrap();
        let last_stats_v = time_stats.data.get(&last_time).unwrap();

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec![
                "Projects::BlobController#show",
                "Projects::TreeController#show",
            ]
        );

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| s.count)
                .collect::<Vec<_>>(),
            vec![4, 1]
        );

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| s.max)
                .collect::<Vec<_>>(),
            vec![9980.00, 9980.0]
        );

        assert_eq!(
            last_stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec![
                "Projects::BlobController#show",
                "Projects::TreeController#show",
                "Groups::MilestonesController#index",
            ]
        );

        assert_eq!(
            last_stats_v
                .stats
                .iter()
                .map(|s| s.count)
                .collect::<Vec<_>>(),
            vec![3, 2, 1]
        );

        assert_eq!(
            last_stats_v.stats.iter().map(|s| s.max).collect::<Vec<_>>(),
            vec![9980.00, 9980.0, 290.0]
        );
    }

    #[test]
    fn calc_time_mt() {
        let input_file = File::open("test/prod.log").unwrap();
        let reader = Reader::File(BufReader::new(input_file));
        let args = ParseArgs::new(None, 4, Vec::new());
        let int = Interval::new(10, TimeUnit::Minute);

        let time_stats =
            LogType::ProdJsonDurS.calculate_time_stats(reader, int, args, StatField::Count);

        assert_eq!(time_stats.interval, Interval::new(10, TimeUnit::Minute));
        assert_eq!(time_stats.data.len(), 2);

        let first_time = DateTime::parse_from_rfc3339("2020-04-24T07:20:08.004Z").unwrap();
        let first_stats_v = time_stats.data.get(&first_time).unwrap();
        let last_time = DateTime::parse_from_rfc3339("2020-04-24T07:30:08.004Z").unwrap();
        let last_stats_v = time_stats.data.get(&last_time).unwrap();

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec![
                "Projects::BlobController#show",
                "Projects::TreeController#show",
            ]
        );

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| s.count)
                .collect::<Vec<_>>(),
            vec![4, 1]
        );

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| s.max)
                .collect::<Vec<_>>(),
            vec![9980.00, 9980.0]
        );

        assert_eq!(
            last_stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec![
                "Projects::BlobController#show",
                "Projects::TreeController#show",
                "Groups::MilestonesController#index",
            ]
        );

        assert_eq!(
            last_stats_v
                .stats
                .iter()
                .map(|s| s.count)
                .collect::<Vec<_>>(),
            vec![3, 2, 1]
        );

        assert_eq!(
            last_stats_v.stats.iter().map(|s| s.max).collect::<Vec<_>>(),
            vec![9980.00, 9980.0, 290.0]
        );
    }

    #[test]
    fn calc_time_st_eq_calc_time_mt() {
        let st_input_file = File::open("test/prod.log").unwrap();
        let st_reader = Reader::File(BufReader::new(st_input_file));
        let st_args = ParseArgs::new(None, 1, Vec::new());
        let st_int = Interval::new(10, TimeUnit::Minute);

        let mt_input_file = File::open("test/prod.log").unwrap();
        let mt_reader = Reader::File(BufReader::new(mt_input_file));
        let mt_args = ParseArgs::new(None, 4, Vec::new());
        let mt_int = Interval::new(10, TimeUnit::Minute);

        assert_eq!(
            LogType::ProdJsonDurS.calculate_time_stats(
                st_reader,
                st_int,
                st_args,
                StatField::Count
            ),
            LogType::ProdJsonDurS.calculate_time_stats(
                mt_reader,
                mt_int,
                mt_args,
                StatField::Count
            ),
        );
    }
}
