use crate::event::Event;
use crate::log_type::LogType;

pub mod detail;
pub mod raw;

pub trait RawOutput: Send {
    fn new(log_t: LogType) -> Self;
    fn insert_event<'a, T: Event<'a>>(&mut self, event: T);
    fn coalesce(self, local_map: Self) -> Self;
}
