use super::RawOutput;
use crate::event::{Event, Status};
use crate::log_type::LogType;
use crate::HashMap;

use chrono::{prelude::*, Duration};

#[derive(Clone, PartialEq, Debug)]
pub struct RawData {
    pub data: Vec<f64>,
    pub fails: usize,
}

impl RawData {
    #[inline]
    pub fn new() -> RawData {
        RawData {
            data: Vec::new(),
            fails: 0,
        }
    }
}

impl From<Vec<TimeEvent>> for RawData {
    fn from(events: Vec<TimeEvent>) -> RawData {
        let mut raw = RawData::new();

        for event in events {
            raw.data.push(event.dur);

            if let Status::Fail = event.status {
                raw.fails += 1;
            }
        }
        raw
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct TimeEvent {
    pub dur: f64,
    pub time: DateTime<FixedOffset>,
    pub status: Status,
}

impl TimeEvent {
    pub fn new(dur: f64, time: DateTime<FixedOffset>, status: Status) -> TimeEvent {
        TimeEvent { dur, time, status }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct RawDataMap {
    pub data: HashMap<String, RawData>,
    pub start_time: String,
    pub end_time: String,
}

impl RawOutput for RawDataMap {
    fn new(_log_t: LogType) -> RawDataMap {
        RawDataMap {
            data: HashMap::default(),
            start_time: String::from("9999"), // greater than all valid times
            end_time: String::from("0000"),   // less than all valid times
        }
    }

    fn insert_event<'a, T: Event<'a>>(&mut self, event: T) {
        self.update_time(event.raw_time());

        let action = event.action();
        let (_key, entry) = self
            .data
            .raw_entry_mut()
            .from_key(action.as_ref())
            .or_insert_with(|| (action.to_string(), RawData::new()));

        entry.data.push(event.dur_ms());

        if let Status::Fail = event.status() {
            entry.fails += 1;
        }
    }

    fn coalesce(mut self, local_map: RawDataMap) -> RawDataMap {
        for (action, local_data) in local_map.data {
            let main_entry = self.data.entry(action).or_insert_with(RawData::new);
            main_entry.data.extend(local_data.data);
            main_entry.fails += local_data.fails;
        }

        self.start_time = self.start_time.min(local_map.start_time);
        self.end_time = self.end_time.max(local_map.end_time);

        self
    }
}

impl RawDataMap {
    pub fn filter(&mut self, term: &str) {
        self.data
            .retain(|key, _| key.to_ascii_lowercase().contains(term));
    }

    pub fn duration(&self, log_t: LogType) -> Duration {
        match (
            log_t.parse_time(&self.start_time),
            log_t.parse_time(&self.end_time),
        ) {
            (Some(start), Some(end)) => end - start,
            _ => Duration::zero(),
        }
    }

    fn update_time(&mut self, time: &str) {
        if time < &self.start_time {
            self.start_time = time.to_string();
        }

        if time > &self.end_time {
            self.end_time = time.to_string();
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct RawTimeDataMap {
    pub data: HashMap<String, Vec<TimeEvent>>,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
}

impl RawOutput for RawTimeDataMap {
    fn new(_log_t: LogType) -> RawTimeDataMap {
        RawTimeDataMap {
            data: HashMap::default(),
            start_time: DateTime::parse_from_rfc3339("3000-01-01T00:00:00Z").unwrap(), // larger than any valid start time
            end_time: DateTime::parse_from_rfc3339("1000-01-01T00:00:00Z").unwrap(), // smaller than any valid start time
        }
    }

    fn insert_event<'a, T: Event<'a>>(&mut self, event: T) {
        if let Some(ts) = T::parse_time(event.raw_time()) {
            self.update_time(&ts);

            let time_event = TimeEvent::new(event.dur_ms(), ts, event.status());

            let action = event.action();
            let (_key, entry) = self
                .data
                .raw_entry_mut()
                .from_key(action.as_ref())
                .or_insert_with(|| (action.to_string(), Vec::new()));

            entry.push(time_event);
        }
    }

    fn coalesce(mut self, local_map: RawTimeDataMap) -> RawTimeDataMap {
        for (action, local_data) in local_map.data {
            let main_entry = self.data.entry(action).or_insert_with(Vec::new);
            main_entry.extend(local_data);
        }

        self.start_time = self.start_time.min(local_map.start_time);
        self.end_time = self.end_time.max(local_map.end_time);

        self
    }
}

impl RawTimeDataMap {
    pub fn filter(&mut self, term: &str) {
        self.data
            .retain(|key, _| key.to_ascii_lowercase().contains(term));
    }

    fn update_time(&mut self, time: &DateTime<FixedOffset>) {
        if time < &self.start_time {
            self.start_time = *time;
        }

        if time > &self.end_time {
            self.end_time = *time;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::event::*;

    #[test]
    fn raw_data_from_time_event() {
        let time_events = vec![
            TimeEvent::new(
                1.0,
                DateTime::parse_from_rfc3339("2020-12-10T12:12:12.001Z").unwrap(),
                Status::Fail,
            ),
            TimeEvent::new(
                5.0,
                DateTime::parse_from_rfc3339("2020-12-10T12:12:13.001Z").unwrap(),
                Status::Success,
            ),
        ];
        assert_eq!(
            RawData::from(time_events),
            RawData {
                data: vec![1.0, 5.0],
                fails: 1,
            }
        );
    }

    #[test]
    fn raw_data_map_insert_event() {
        let input = r##"2019-02-01_11:30:20.13239 time="2019-02-01T11:30:20Z" level=info msg="finished streaming call with code OK" grpc.code=OK grpc.meta.auth_version=v2 grpc.method=InfoRefsUploadPack grpc.request.fullMethod=/gitaly.SmartHTTPService/InfoRefsUploadPack grpc.request.glRepository=project-14306 grpc.request.repoPath=group/project.git grpc.request.repoStorage=default grpc.request.topLevelGroup=group grpc.service=gitaly.SmartHTTPService grpc.start_time="2019-02-01T11:30:19Z" grpc.time_ms=322.1 peer.address=@ span.kind=server system=grpc"##;
        let event = GitalyEvent::parse(input).unwrap();
        let mut raw = RawDataMap::new(LogType::GitalyText);
        raw.insert_event(event);

        assert_eq!(
            raw.data.into_iter().collect::<Vec<_>>(),
            vec![(
                String::from("InfoRefsUploadPack"),
                RawData {
                    data: vec![322.1],
                    fails: 0,
                }
            )]
        );
    }

    #[test]
    fn raw_data_map_insert_failed_event() {
        let input = r##"{"correlation_id":"LHjQZjYDHQ9","error":"rpc error: code = FailedPrecondition desc = conflict side missing","grpc.code":"FailedPrecondition","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.meta.deadline_type":"limited","grpc.method":"ListConflictFiles","grpc.request.deadline":"2020-11-18T17:47:44Z","grpc.request.fullMethod":"/gitaly.ConflictsService/ListConflictFiles","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-26273","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.ConflictsService","grpc.start_time":"2020-11-18T17:46:48Z","grpc.time_ms":188.34,"level":"warning","msg":"finished streaming call with code FailedPrecondition","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:46:48.854Z"}"##;
        let event = GitalyJsonEvent::parse(input).unwrap();
        let mut raw = RawDataMap::new(LogType::GitalyJson);
        raw.insert_event(event);

        assert_eq!(
            raw.data.into_iter().collect::<Vec<_>>(),
            vec![(
                String::from("ListConflictFiles"),
                RawData {
                    data: vec![188.34],
                    fails: 1,
                }
            )]
        );
    }

    #[test]
    fn raw_data_map_coalesce() {
        let input_a = r##"{"correlation_id":"qNWQPts8Zs","grpc.code":"OK","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.meta.deadline_type":"regular","grpc.method":"GetBlobs","grpc.request.deadline":"2020-11-18T17:22:37Z","grpc.request.fullMethod":"/gitaly.BlobService/GetBlobs","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-39207","grpc.request.repoPath":"@hashed/6a/f7/6af704b5fae984705e2046c7ed9972a0c10e681ccfaf8f521a50c45c1df1ca7a.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.BlobService","grpc.start_time":"2020-11-18T17:22:27Z","grpc.time_ms":42.833,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:22:28.021Z"}
{"correlation_id":"qYFWWJJAZ25","grpc.code":"OK","grpc.meta.client_name":"gitlab-shell","grpc.meta.deadline_type":"none","grpc.method":"SSHUploadPack","grpc.request.fullMethod":"/gitaly.SSHService/SSHUploadPack","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-38026","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.SSHService","grpc.start_time":"2020-11-18T17:29:27Z","grpc.time_ms":302.327,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:29:28.049Z"}"##;
        let input_b = r##"{"correlation_id":"CyxviAtYMAa","grpc.code":"OK","grpc.meta.client_name":"gitlab-shell","grpc.meta.deadline_type":"none","grpc.method":"SSHUploadPack","grpc.request.fullMethod":"/gitaly.SSHService/SSHUploadPack","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-1515","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.SSHService","grpc.start_time":"2020-11-18T17:22:28Z","grpc.time_ms":199.295,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:22:28.802Z"}
{"correlation_id":"7MGB7vWeyt5","grpc.code":"OK","grpc.meta.client_name":"gitlab-shell","grpc.meta.deadline_type":"none","grpc.method":"SSHUploadPack","grpc.request.fullMethod":"/gitaly.SSHService/SSHUploadPack","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-38026","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.SSHService","grpc.start_time":"2020-11-18T17:30:27Z","grpc.time_ms":297.168,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:30:28.115Z"}"##;
        let mut raw_a = RawDataMap::new(LogType::GitalyJson);
        let mut raw_b = RawDataMap::new(LogType::GitalyJson);
        let events_a: Vec<_> = input_a
            .lines()
            .map(|l| GitalyJsonEvent::parse(l).unwrap())
            .collect();
        let events_b: Vec<_> = input_b
            .lines()
            .map(|l| GitalyJsonEvent::parse(l).unwrap())
            .collect();

        events_a.into_iter().for_each(|e| raw_a.insert_event(e));
        events_b.into_iter().for_each(|e| raw_b.insert_event(e));
        let raw = raw_a.coalesce(raw_b);

        assert_eq!(raw.start_time, String::from("2020-11-18T17:22:28.021Z"));
        assert_eq!(raw.end_time, String::from("2020-11-18T17:30:28.115Z"));

        let mut blob_data = raw.data.get("GetBlobs").unwrap().data.clone();
        blob_data.sort_by(|x, y| x.partial_cmp(y).unwrap());
        assert_eq!(blob_data, vec![42.833]);

        let mut ssh_data = raw.data.get("SSHUploadPack").unwrap().data.clone();
        ssh_data.sort_by(|x, y| x.partial_cmp(y).unwrap());
        assert_eq!(ssh_data, vec![199.295, 297.168, 302.327]);

        assert_eq!(raw.data.get("GetBlobs").unwrap().fails, 0);
        assert_eq!(raw.data.get("SSHUploadPack").unwrap().fails, 0);
    }

    #[test]
    fn raw_data_map_filter() {
        let input = r##"{"correlation_id":"qNWQPts8Zs","grpc.code":"OK","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.meta.deadline_type":"regular","grpc.method":"GetBlobs","grpc.request.deadline":"2020-11-18T17:22:37Z","grpc.request.fullMethod":"/gitaly.BlobService/GetBlobs","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-39207","grpc.request.repoPath":"@hashed/6a/f7/6af704b5fae984705e2046c7ed9972a0c10e681ccfaf8f521a50c45c1df1ca7a.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.BlobService","grpc.start_time":"2020-11-18T17:22:27Z","grpc.time_ms":42.833,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:22:28.021Z"}
{"correlation_id":"qYFWWJJAZ25","grpc.code":"OK","grpc.meta.client_name":"gitlab-shell","grpc.meta.deadline_type":"none","grpc.method":"SSHUploadPack","grpc.request.fullMethod":"/gitaly.SSHService/SSHUploadPack","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-38026","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.SSHService","grpc.start_time":"2020-11-18T17:29:27Z","grpc.time_ms":302.327,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:29:28.049Z"}"##;

        let mut raw = RawDataMap::new(LogType::GitalyJson);
        let events: Vec<_> = input
            .lines()
            .map(|l| GitalyJsonEvent::parse(l).unwrap())
            .collect();
        events.into_iter().for_each(|e| raw.insert_event(e));

        raw.filter(&"upload");

        let keys: Vec<_> = raw.data.keys().cloned().collect();
        assert_eq!(keys, vec!["SSHUploadPack"]);
    }

    #[test]
    fn raw_time_data_map_insert_event() {
        let input = r##"{"method":"GET","path":"group/project/discover","format":"*/*","controller":"Projects::BlobController","action":"show","status":200,"time":"2020-04-24T04:54:41.461Z","params":[{"key":"namespace_id","value":"group"},{"key":"project_id","value":"project"},{"key":"id","value":"blah"}],"remote_ip":"10.10.10.10","user_id":null,"username":null,"ua":"Mozilla/5.0","queue_duration_s":0.01,"gitaly_calls":2,"gitaly_duration_s":0.02,"rugged_calls":5,"rugged_duration_s":0.01,"redis_calls":26,"redis_duration_s":0.0,"correlation_id":"IratXOPyxi6","cpu_s":0.12,"db_duration_s":0.01,"view_duration_s":0.08,"duration_s":0.14}"##;
        let event = ProdJsonDurSEvent::parse(input).unwrap();
        let mut raw = RawTimeDataMap::new(LogType::ProdJsonDurS);
        raw.insert_event(event);

        assert_eq!(
            raw.data.into_iter().collect::<Vec<_>>(),
            vec![(
                String::from("Projects::BlobController#show"),
                vec![TimeEvent {
                    dur: 140.0,
                    time: DateTime::parse_from_rfc3339("2020-04-24T04:54:41.461Z").unwrap(),
                    status: Status::Success,
                }]
            )]
        );
    }

    #[test]
    fn raw_time_data_map_coalesce() {
        let input_a = r##"{"time":"2020-04-23T22:14:20.290Z","severity":"INFO","duration_s":1.46,"db_duration_s":0.09,"view_duration_s":1.37,"status":200,"method":"GET","path":"/api/v4/projects","params":[{"key":"page","value":"2"},{"key":"per_page","value":"100"},{"key":"statistics","value":"true"}],"host":"gitlab.example.com","remote_ip":"10.10.10.10","ua":"go-gitlab","route":"/api/:version/projects","user_id":2,"username":"user","queue_duration_s":0.01,"gitaly_calls":11,"gitaly_duration_s":0.13,"redis_calls":701,"redis_duration_s":0.06,"correlation_id":"u4G996jjXJ1"}
{"time":"2020-04-23T22:14:21.769Z","severity":"INFO","duration_s":1.46,"db_duration_s":0.05,"view_duration_s":1.41,"status":200,"method":"GET","path":"/api/v4/projects","params":[{"key":"page","value":"3"},{"key":"per_page","value":"100"},{"key":"statistics","value":"true"}],"host":"gitlab.example.com","remote_ip":"10.10.10.10","ua":"go-gitlab","route":"/api/:version/projects","user_id":2,"username":"user","queue_duration_s":0.01,"gitaly_calls":12,"gitaly_duration_s":0.15,"redis_calls":677,"redis_duration_s":0.06,"correlation_id":"SXIj2D1n5A"}"##;
        let input_b = r##"{"time":"2020-04-24T07:14:23.209Z","severity":"INFO","duration_s":0.37,"db_duration_s":0.05,"view_duration_s":0.32,"status":200,"method":"GET","path":"/api/v4/users","params":[{"key":"page","value":"5"},{"key":"per_page","value":"100"}],"host":"gitlab.example.com","remote_ip":"10.10.10.10","ua":"go-gitlab","route":"/api/:version/users","user_id":2,"username":"user","queue_duration_s":0.01,"redis_calls":100,"redis_duration_s":0.01,"correlation_id":"Pie6rvQWNI8"}
{"time":"2020-04-24T07:14:23.396Z","severity":"INFO","duration_s":0.17,"db_duration_s":0.02,"view_duration_s":0.14,"status":200,"method":"GET","path":"/api/v4/users","params":[{"key":"page","value":"6"},{"key":"per_page","value":"100"}],"host":"gitlab.example.com","remote_ip":"10.10.10.10","ua":"go-gitlab","route":"/api/:version/users","user_id":2,"username":"user","queue_duration_s":0.01,"redis_calls":43,"redis_duration_s":0.0,"correlation_id":"RQAUgK4clA4"}"##;
        let mut raw_a = RawTimeDataMap::new(LogType::ApiJsonDurS);
        let mut raw_b = RawTimeDataMap::new(LogType::ApiJsonDurS);
        let events_a: Vec<_> = input_a
            .lines()
            .map(|l| ApiJsonDurSEvent::parse(l).unwrap())
            .collect();
        let events_b: Vec<_> = input_b
            .lines()
            .map(|l| ApiJsonDurSEvent::parse(l).unwrap())
            .collect();

        events_a.into_iter().for_each(|e| raw_a.insert_event(e));
        events_b.into_iter().for_each(|e| raw_b.insert_event(e));
        let raw = raw_a.coalesce(raw_b);

        assert_eq!(
            raw.start_time,
            DateTime::parse_from_rfc3339("2020-04-23T22:14:20.290Z").unwrap()
        );
        assert_eq!(
            raw.end_time,
            DateTime::parse_from_rfc3339("2020-04-24T07:14:23.396Z").unwrap()
        );

        let projects_data = raw.data.get("/api/:version/projects").unwrap().clone();
        assert_eq!(
            projects_data,
            vec![
                TimeEvent {
                    dur: 1460.0,
                    time: DateTime::parse_from_rfc3339("2020-04-23T22:14:20.290Z").unwrap(),
                    status: Status::Success,
                },
                TimeEvent {
                    dur: 1460.0,
                    time: DateTime::parse_from_rfc3339("2020-04-23T22:14:21.769Z").unwrap(),
                    status: Status::Success,
                }
            ]
        );

        let users_data = raw.data.get("/api/:version/users").unwrap().clone();
        assert_eq!(
            users_data,
            vec![
                TimeEvent {
                    dur: 370.0,
                    time: DateTime::parse_from_rfc3339("2020-04-24T07:14:23.209Z").unwrap(),
                    status: Status::Success,
                },
                TimeEvent {
                    dur: 170.0,
                    time: DateTime::parse_from_rfc3339("2020-04-24T07:14:23.396Z").unwrap(),
                    status: Status::Success,
                }
            ]
        );
    }

    #[test]
    fn raw_time_data_map_filter() {
        let input = r##"{"severity":"INFO","time":"2020-04-23T05:19:04.593Z","queue":"cronjob:pipeline_schedule","class":"PipelineScheduleWorker","retry":false,"queue_namespace":"cronjob","jid":"75666b794a52687726000513","created_at":"2020-04-23T05:19:04.544Z","correlation_id":"fa48f5803ad824eabb004aa1774537d6","enqueued_at":"2020-04-23T05:19:04.547Z","pid":29039,"message":"PipelineScheduleWorker JID-75666b794a52687726000513: done: 0.04574 sec","job_status":"done","scheduling_latency_s":0.001009,"duration":0.04574,"cpu_s":0.00551,"completed_at":"2020-04-23T05:19:04.593Z","db_duration":1.021306961774826,"db_duration_s":0.001021306961774826}
{"severity":"INFO","time":"2020-04-23T05:19:04.613Z","queue":"cronjob:elastic_index_bulk_cron","class":"ElasticIndexBulkCronWorker","retry":false,"queue_namespace":"cronjob","jid":"1fabed514650adbc9f088be2","created_at":"2020-04-23T05:19:04.557Z","correlation_id":"8ce2d6b874090d8994419d3a921ed457","enqueued_at":"2020-04-23T05:19:04.557Z","pid":29039,"message":"ElasticIndexBulkCronWorker JID-1fabed514650adbc9f088be2: done: 0.052651 sec","job_status":"done","scheduling_latency_s":0.003145,"duration":0.052651,"cpu_s":0.003783,"completed_at":"2020-04-23T05:19:04.613Z","db_duration":0,"db_duration_s":0}
{"severity":"INFO","time":"2020-04-23T05:19:04.636Z","queue":"cronjob:geo_sidekiq_cron_config","class":"Geo::SidekiqCronConfigWorker","retry":false,"queue_namespace":"cronjob","jid":"e13c94ee1bb11f98aebce052","created_at":"2020-04-23T05:19:04.569Z","correlation_id":"8ce95140d5a15a46fa116bbc25238a3d","enqueued_at":"2020-04-23T05:19:04.570Z","pid":29039,"message":"Geo::SidekiqCronConfigWorker JID-e13c94ee1bb11f98aebce052: done: 0.064898 sec","job_status":"done","scheduling_latency_s":0.000823,"duration":0.064898,"cpu_s":0.019244,"completed_at":"2020-04-23T05:19:04.636Z","db_duration":0,"db_duration_s":0}"##;
        let mut raw = RawTimeDataMap::new(LogType::SidekiqJson);
        let events: Vec<_> = input
            .lines()
            .map(|l| SidekiqJsonEvent::parse(l).unwrap())
            .collect();
        events.into_iter().for_each(|e| raw.insert_event(e));

        raw.filter(&"pipeline");

        let keys: Vec<_> = raw.data.keys().cloned().collect();
        assert_eq!(keys, vec!["PipelineScheduleWorker"]);
    }
}
