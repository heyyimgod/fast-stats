use crate::data::raw::RawData;
use crate::data::RawOutput;
use crate::event::{Event, Status};
use crate::field::{EventField, StatField};
use crate::log_type::LogType;
use crate::stats::{Stats, StatsType};
use crate::stats_vec::{StatsVec, VerboseStatsVec};
use crate::util::{self, BLUE, CYAN, GREEN, RED, RESET, YELLOW};
use crate::HashMap;

use chrono::prelude::*;
use rayon::prelude::*;

#[derive(Clone, PartialEq, Debug)]
pub struct DetailData {
    pub dur: Vec<f64>,
    pub db: Vec<f64>,
    pub gitaly: Vec<f64>,
    pub rugged: Vec<f64>,
    pub redis: Vec<f64>,
    pub queue: Vec<f64>,
    pub cpu_s: Vec<f64>,
    pub fails: usize,
}

impl DetailData {
    #[inline]
    pub fn new() -> DetailData {
        DetailData {
            dur: Vec::new(),
            db: Vec::new(),
            gitaly: Vec::new(),
            rugged: Vec::new(),
            redis: Vec::new(),
            queue: Vec::new(),
            cpu_s: Vec::new(),
            fails: 0,
        }
    }

    pub fn len(&self) -> usize {
        self.dur.len()
    }

    pub fn push(&mut self, event: &DetailTimeEvent) {
        self.dur.push(event.dur);

        if let Some(db) = event.db {
            self.db.push(db);
        }

        if let Some(gitaly) = event.gitaly {
            self.gitaly.push(gitaly);
        }

        if let Some(rugged) = event.rugged {
            self.rugged.push(rugged);
        }

        if let Some(redis) = event.redis {
            self.redis.push(redis);
        }

        if let Some(queue) = event.queue {
            self.queue.push(queue);
        }

        if let Some(queue) = event.queue {
            self.queue.push(queue);
        }

        if let Some(cpu_s) = event.cpu_s {
            self.cpu_s.push(cpu_s);
        }

        if let Status::Fail = event.status {
            self.fails += 1;
        }
    }

    pub fn extend(&mut self, other: &DetailData) {
        self.dur.extend(&other.dur);
        self.db.extend(&other.db);
        self.gitaly.extend(&other.gitaly);
        self.rugged.extend(&other.rugged);
        self.redis.extend(&other.redis);
        self.queue.extend(&other.queue);
        self.cpu_s.extend(&other.cpu_s);
        self.fails += other.fails;
    }

    pub fn sort(&mut self) {
        self.dur.sort_by(|x, y| x.partial_cmp(y).unwrap());
        self.db.sort_by(|x, y| x.partial_cmp(y).unwrap());
        self.gitaly.sort_by(|x, y| x.partial_cmp(y).unwrap());
        self.rugged.sort_by(|x, y| x.partial_cmp(y).unwrap());
        self.redis.sort_by(|x, y| x.partial_cmp(y).unwrap());
        self.queue.sort_by(|x, y| x.partial_cmp(y).unwrap());
        self.cpu_s.sort_by(|x, y| x.partial_cmp(y).unwrap());
    }
}

impl From<Vec<DetailTimeEvent>> for DetailData {
    fn from(events: Vec<DetailTimeEvent>) -> DetailData {
        let mut data = DetailData::new();

        for event in events {
            data.push(&event);
        }

        data
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct DetailTimeEvent {
    pub time: DateTime<FixedOffset>,
    pub dur: f64,
    pub status: Status,
    pub db: Option<f64>,
    pub gitaly: Option<f64>,
    pub rugged: Option<f64>,
    pub redis: Option<f64>,
    pub queue: Option<f64>,
    pub cpu_s: Option<f64>,
    pub correlation_id: Option<String>,
    pub user: Option<String>,
    pub project: Option<String>,
    pub path: Option<String>,
    pub client: Option<String>,
}

impl DetailTimeEvent {
    pub fn new<'a, T: Event<'a>>(event: &T, time: DateTime<FixedOffset>) -> DetailTimeEvent {
        DetailTimeEvent {
            time,
            dur: event.dur_s(),
            db: event.db(),
            gitaly: event.gitaly(),
            rugged: event.rugged(),
            redis: event.redis(),
            queue: event.queue(),
            cpu_s: event.cpu_s(),
            status: event.status(),
            correlation_id: event.correlation_id().map(|c| c.to_string()),
            user: event.user().map(|u| u.to_string()),
            project: event.project().map(|p| p.to_string()),
            path: event.path().map(|p| p.to_string()),
            client: event.client_type().map(|c| c.to_string()),
        }
    }

    pub fn as_string(&self, title: &str, color: bool) -> String {
        if color {
            format!(
                "{} dur: {:5.2}s  {}db: {:5.2}s{}  {}redis: {:5.2}s{}  {}gitaly: {:5.2}s{}  {}rugged: {:5.2}s{}  {}cpu: {:5.2}s{} -- {}  {}",
                title,
                self.dur,
                BLUE,
                self.db.unwrap_or_default(),
                RESET,
                RED,
                self.redis.unwrap_or_default(),
                RESET,
                GREEN,
                self.gitaly.unwrap_or_default(),
                RESET,
                YELLOW,
                self.rugged.unwrap_or_default(),
                RESET,
                CYAN,
                self.cpu_s.unwrap_or_default(),
                RESET,
                self.time.to_rfc3339_opts(SecondsFormat::Millis, true),
                self.correlation_id.as_deref().unwrap_or_default(),
            )
        } else {
            format!(
                "{} dur: {:5.2}s  db: {:5.2}s  redis: {:5.2}s  gitaly: {:5.2}s  rugged: {:5.2}s  cpu: {:5.2}s -- {} {}",
                title,
                self.dur,
                self.db.unwrap_or_default(),
                self.redis.unwrap_or_default(),
                self.gitaly.unwrap_or_default(),
                self.rugged.unwrap_or_default(),
                self.cpu_s.unwrap_or_default(),
                self.time.to_rfc3339(),
                self.correlation_id.as_deref().unwrap_or_default(),
            )
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct DetailMap {
    pub data: HashMap<String, DetailData>,
    pub start_time: String,
    pub end_time: String,
}

impl RawOutput for DetailMap {
    fn new(_log_t: LogType) -> DetailMap {
        DetailMap {
            data: HashMap::default(),
            start_time: String::from("9999"), // greater than all valid times
            end_time: String::from("0000"),   // less than all valid times
        }
    }

    fn insert_event<'a, T: Event<'a>>(&mut self, event: T) {
        self.update_time(event.raw_time());

        let action = event.action();
        let (_key, entry) = self
            .data
            .raw_entry_mut()
            .from_key(action.as_ref())
            .or_insert_with(|| (action.to_string(), DetailData::new()));

        if let Status::Fail = event.status() {
            entry.fails += 1;
        }
    }

    fn coalesce(mut self, local_map: DetailMap) -> DetailMap {
        for (action, local_data) in local_map.data {
            let main_entry = self.data.entry(action).or_insert_with(DetailData::new);
            main_entry.dur.extend(local_data.dur);
            main_entry.db.extend(local_data.db);
            main_entry.gitaly.extend(local_data.gitaly);
            main_entry.rugged.extend(local_data.rugged);
            main_entry.redis.extend(local_data.redis);
            main_entry.queue.extend(local_data.queue);
            main_entry.cpu_s.extend(local_data.cpu_s);
            main_entry.fails += local_data.fails;
        }

        self.start_time = self.start_time.min(local_map.start_time);
        self.end_time = self.end_time.max(local_map.end_time);

        self
    }
}

impl DetailMap {
    fn update_time(&mut self, time: &str) {
        if time < &self.start_time {
            self.start_time = time.to_string();
        }

        if time > &self.end_time {
            self.end_time = time.to_string();
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct DetailTimeMap {
    pub data: HashMap<String, Vec<DetailTimeEvent>>,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
}

impl RawOutput for DetailTimeMap {
    fn new(_log_t: LogType) -> DetailTimeMap {
        DetailTimeMap {
            data: HashMap::default(),
            start_time: DateTime::parse_from_rfc3339("3000-01-01T00:00:00Z").unwrap(), // larger than any valid start time
            end_time: DateTime::parse_from_rfc3339("1000-01-01T00:00:00Z").unwrap(), // smaller than any valid start time
        }
    }

    fn insert_event<'a, T: Event<'a>>(&mut self, event: T) {
        if let Some(ts) = T::parse_time(event.raw_time()) {
            self.update_time(&ts);

            let detail_event = DetailTimeEvent::new(&event, ts);

            let action = event.action();
            let (_key, entry) = self
                .data
                .raw_entry_mut()
                .from_key(action.as_ref())
                .or_insert_with(|| (action.to_string(), Vec::new()));

            entry.push(detail_event);
        }
    }

    fn coalesce(mut self, local_map: DetailTimeMap) -> DetailTimeMap {
        for (action, local_data) in local_map.data {
            let main_entry = self.data.entry(action).or_insert_with(Vec::new);
            main_entry.extend(local_data);
        }

        self.start_time = self.start_time.min(local_map.start_time);
        self.end_time = self.end_time.max(local_map.end_time);

        self
    }
}

impl DetailTimeMap {
    pub fn filter(&mut self, term: &str) {
        self.data
            .retain(|key, _| key.to_ascii_lowercase().contains(term));
    }

    pub fn sort_by(&mut self, field: EventField) {
        self.data
            .par_iter_mut()
            .for_each(|(_action, items)| match field {
                EventField::Duration => {
                    items.sort_by(|x, y| x.dur.partial_cmp(&y.dur).expect("NaN found when sorting"))
                }
                EventField::Db => {
                    items.sort_by(|x, y| x.db.partial_cmp(&y.db).expect("NaN found when sorting"))
                }
                EventField::Redis => items.sort_by(|x, y| {
                    x.redis
                        .partial_cmp(&y.redis)
                        .expect("NaN found when sorting")
                }),
                EventField::Gitaly => items.sort_by(|x, y| {
                    x.gitaly
                        .partial_cmp(&y.gitaly)
                        .expect("NaN found when sorting")
                }),
                EventField::Rugged => items.sort_by(|x, y| {
                    x.rugged
                        .partial_cmp(&y.rugged)
                        .expect("NaN found when sorting")
                }),
                EventField::CpuS => items.sort_by(|x, y| {
                    x.cpu_s
                        .partial_cmp(&y.cpu_s)
                        .expect("NaN found when sorting")
                }),
                EventField::Queue => items.sort_by(|x, y| {
                    x.queue
                        .partial_cmp(&y.queue)
                        .expect("NaN found when sorting")
                }),
                _ => {}
            });
    }

    // Useful for testing
    #[allow(dead_code)]
    pub fn into_stats_vec(self, stats_t: StatsType) -> StatsVec {
        let dur = self.end_time - self.start_time;
        let stats: Vec<_> = self
            .data
            .into_par_iter()
            .map(|(action, events)| {
                let mut raw = RawData::new();
                raw.fails = events.iter().filter(|e| e.status.is_fail()).count();
                raw.data.extend(events.into_iter().map(|e| e.dur * 1000.0));

                Stats::from_raw_data(action, raw, dur)
            })
            .collect();

        StatsVec::new(stats_t, stats)
    }

    pub fn set_times(
        &mut self,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
    ) {
        self.start_time = start_time;
        self.end_time = end_time;
    }

    pub fn into_verbose_stats_vec(
        self,
        stats_t: StatsType,
        sort_by: StatField,
        color: bool,
    ) -> VerboseStatsVec {
        let examples = self.find_examples(color);

        let dur = self.end_time - self.start_time;

        let stats: Vec<_> = self
            .data
            .into_par_iter()
            .map(|(action, events)| {
                let mut raw = RawData::new();
                raw.fails = events.iter().filter(|e| e.status.is_fail()).count();
                raw.data.extend(events.into_iter().map(|e| e.dur * 1000.0));

                Stats::from_raw_data(action, raw, dur)
            })
            .collect();

        let mut stats_v = StatsVec::new(stats_t, stats);
        stats_v.sort_by(sort_by);

        VerboseStatsVec::new(stats_t, stats_v, examples)
    }

    fn find_examples(&self, color: bool) -> HashMap<String, [Option<String>; 4]> {
        self.data
            .par_iter()
            .map(|(action, events)| {
                let (p100, _) = util::percentile_obj(events, 100);
                let (mut p99, p99_pctl) = util::percentile_obj(events, 99);
                let (mut p95, p95_pctl) = util::percentile_obj(events, 95);
                let (mut median, median_pctl) = util::percentile_obj(events, 50);

                if median == p100 || median == p99 || median == p95 {
                    median = None;
                }

                if p95 == p99 || p95 == p100 {
                    p95 = None;
                }

                if p99 == p100 {
                    p99 = None;
                }

                let examples = [
                    p100.map(|e| e.as_string("MAX", color)),
                    p99.map(|e| e.as_string(&format!("P{}", p99_pctl), color)),
                    p95.map(|e| e.as_string(&format!("P{}", p95_pctl), color)),
                    median.map(|e| e.as_string(&format!("P{}", median_pctl), color)),
                ];

                (action.clone(), examples)
            })
            .collect()
    }

    fn update_time(&mut self, time: &DateTime<FixedOffset>) {
        if time < &self.start_time {
            self.start_time = *time;
        }

        if time > &self.end_time {
            self.end_time = *time;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::execute::Reader;
    use crate::field::StatField;
    use crate::parse::ParseArgs;
    use crate::stats::*;

    use std::fs::File;
    use std::io::BufReader;

    #[test]
    fn detail_stats_v_matches_normal() {
        let input_file = File::open("test/api.log").unwrap();
        let reader = Reader::File(BufReader::new(input_file));
        let args = ParseArgs::new(None, 1, Vec::new());
        let stats_v = LogType::ApiJsonDurS.calculate_stats(reader, args.clone(), StatField::Count);

        let detail_file = File::open("test/api.log").unwrap();
        let detail_reader = Reader::File(BufReader::new(detail_file));
        let detail_stats = LogType::ApiJsonDurS.calculate_detailed_time_data(detail_reader, args);
        let mut detail_stats_v = detail_stats.into_stats_vec(StatsType::Api);
        detail_stats_v.sort_by(StatField::default());

        assert_eq!(stats_v, detail_stats_v);
    }
}
