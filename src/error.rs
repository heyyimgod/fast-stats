use crate::data::RawOutput;
use crate::event::Event;
use crate::log_type::LogType;
use crate::stats_vec::PrintFormat;
use crate::util::*;
use crate::{HashMap, HashSet};

use rayon::prelude::*;
use serde::Serialize;
use std::io::{self, Write};

const BACKTRACE_LEN: usize = 3;
const TIME_LEN: usize = 24;
const DEFAULT_CORR_LEN: usize = 12;
const DEFAULT_ACTION_LEN: usize = 10;
const DEFAULT_USER_LEN: usize = 12;
const DEFAULT_PROJ_LEN: usize = 8;

#[derive(Serialize, Debug)]
pub struct ErrorMap {
    data: HashMap<String, ErrorSummary>,
}

impl RawOutput for ErrorMap {
    fn new(_log_t: LogType) -> ErrorMap {
        ErrorMap {
            data: HashMap::default(),
        }
    }

    fn insert_event<'a, T: Event<'a>>(&mut self, event: T) {
        if let Some(err) = event.error() {
            let (_key, entry) = self
                .data
                .raw_entry_mut()
                .from_key(err.as_ref())
                .or_insert_with(|| (err.to_string(), ErrorSummary::new()));

            entry.push(&event);
        }
    }

    fn coalesce(mut self, local_map: ErrorMap) -> ErrorMap {
        for (err, local_data) in local_map.data {
            let main_entry = self.data.entry(err).or_insert_with(ErrorSummary::new);
            main_entry.events.extend(local_data.events);
            main_entry.backtraces.extend(local_data.backtraces);
        }

        self
    }
}

impl ErrorMap {
    pub fn sort(&mut self) {
        self.data.values_mut().for_each(|e| e.sort());
    }

    pub fn print(&self, color: bool, no_border: bool, fmt: PrintFormat) -> io::Result<()> {
        let stdout = io::stdout();
        let out = stdout.lock();

        match fmt {
            PrintFormat::Json => self.print_json(out),
            _ => self.print_text(out, color, no_border),
        }
    }

    fn print_json(&self, mut out: impl Write) -> io::Result<()> {
        let serialized = serde_json::to_string(&self).expect("failed to serialize error data");
        writeln!(out, "{}", serialized)
    }

    fn print_text(&self, mut out: impl Write, color: bool, no_border: bool) -> io::Result<()> {
        let mut sorted: Vec<_> = self.data.iter().collect();
        sorted.sort_by(|x, y| y.1.events.len().cmp(&x.1.events.len()));

        let mut iter = sorted.into_iter().peekable();
        while let Some((error, summary)) = iter.next() {
            let mut lines = Vec::new();

            if color {
                lines.push(format!("{}Error{}: {}", BLUE, RESET, error));
            } else {
                lines.push(format!("Error: {}", error));
            }

            lines.extend_from_slice(&summary.text_vec(color));

            let mut joined = match no_border {
                true => crate::print::wrap::bare(&lines),
                false => crate::print::wrap::wrap_lines(&lines),
            };

            if iter.peek().is_some() {
                joined.push('\n');
            }

            writeln!(out, "{}", joined)?;
        }

        Ok(())
    }
}

#[derive(Serialize, Debug)]
pub struct ErrorEvent {
    time: String,
    action: String,
    correlation_id: Option<String>,
    project: Option<String>,
    user: Option<String>,
}

impl ErrorEvent {
    pub fn from_event<'a>(event: &impl Event<'a>) -> ErrorEvent {
        ErrorEvent {
            time: event.raw_time().to_string(),
            correlation_id: event.correlation_id().map(String::from),
            action: event.action().to_string(),
            project: event.project().map(String::from),
            user: event.user().map(String::from),
        }
    }

    pub fn text(
        &self,
        color: bool,
        corr_len: usize,
        action_len: usize,
        user_len: usize,
        proj_len: usize,
    ) -> String {
        if color {
            format!(
                "{}{:t_len$}{}  {}{:<c_len$}{}  {}{:<a_len$}{}  {}{:<u_len$}{}  {}{:<p_len$}{}",
                GREEN,
                self.time,
                RESET,
                MAGENTA,
                self.correlation_id.as_deref().unwrap_or("-"),
                RESET,
                BLUE,
                self.action,
                RESET,
                YELLOW,
                self.user.as_deref().unwrap_or("-"),
                RESET,
                CYAN,
                self.project.as_deref().unwrap_or("-"),
                RESET,
                t_len = TIME_LEN,
                c_len = corr_len,
                a_len = action_len,
                u_len = user_len,
                p_len = proj_len,
            )
        } else {
            format!(
                "{:t_len$}  {:<c_len$}  {:<a_len$}  {:<u_len$}  {:<p_len$}",
                self.time,
                self.correlation_id.as_deref().unwrap_or("-"),
                self.action,
                self.user.as_deref().unwrap_or("-"),
                self.project.as_deref().unwrap_or("-"),
                t_len = TIME_LEN,
                c_len = corr_len,
                a_len = action_len,
                u_len = user_len,
                p_len = proj_len,
            )
        }
    }
}

#[derive(Serialize, Debug)]
pub struct ErrorSummary {
    events: Vec<ErrorEvent>,
    backtraces: HashSet<String>,
}

impl ErrorSummary {
    pub fn new() -> ErrorSummary {
        ErrorSummary {
            events: Vec::new(),
            backtraces: HashSet::new(),
        }
    }

    pub fn push<'a>(&mut self, event: &impl Event<'a>) {
        self.events.push(ErrorEvent::from_event(event));

        if let Some(bt) = event.backtrace(BACKTRACE_LEN, 2) {
            self.backtraces.insert(bt);
        }
    }

    pub fn sort(&mut self) {
        self.events.par_sort_by(|x, y| x.time.cmp(&y.time));
    }

    pub fn text_vec(&self, color: bool) -> Vec<String> {
        let corr_len = self.correlation_id_len();
        let action_len = self.action_len();
        let user_len = self.user_len();
        let proj_len = self.project_len();

        let mut out = Vec::new();
        if color {
            let ct = format!("{}Count{}: {}", BLUE, RESET, self.events.len());
            out.push(ct);
        } else {
            out.push(format!("Count: {}", self.events.len()))
        };

        match self.backtraces.len() {
            0 => {}
            1 => {
                let bt = if color {
                    format!(
                        "{}Backtrace{}:\n{}",
                        BLUE,
                        RESET,
                        self.backtraces.iter().next().unwrap()
                    )
                } else {
                    format!("Backtrace:\n{}", self.backtraces.iter().next().unwrap())
                };
                out.push(bt);
            }
            _ => {
                if color {
                    out.push(format!(
                        "{}Backtrace{}:\n{}\n",
                        BLUE,
                        RESET,
                        self.backtraces.iter().next().unwrap()
                    ));
                } else {
                    out.push(format!(
                        "Backtrace:\n{}\n",
                        self.backtraces.iter().next().unwrap()
                    ));
                }

                for bt in self.backtraces.iter().skip(1) {
                    out.push(bt.clone());
                }
            }
        };

        let mut event_str = String::new();
        let event_title = match self.events.len() {
            0..=10 => {
                if color {
                    format!("{}Events{}:", BLUE, RESET)
                } else {
                    "Events:".to_string()
                }
            }
            _ => {
                if color {
                    format!("{}Last 10 Events{}:", BLUE, RESET)
                } else {
                    "Last 10 Events:".to_string()
                }
            }
        };

        event_str.push_str(&event_title);
        event_str.push_str("\n  ");
        event_str.push_str(&self.header(color, corr_len, action_len, user_len, proj_len));

        let mut iter = self.events.iter().rev().take(10).rev().peekable();
        while let Some(event) = iter.next() {
            event_str.push_str("  ");
            event_str.push_str(&event.text(color, corr_len, action_len, user_len, proj_len));
            if iter.peek().is_some() {
                event_str.push('\n');
            }
        }

        out.push(event_str);

        out
    }

    pub fn header(
        &self,
        color: bool,
        corr_len: usize,
        action_len: usize,
        user_len: usize,
        proj_len: usize,
    ) -> String {
        if color {
            format!(
                "{}{:<t_len$}{}  {}{:<c_len$}{}  {}{:<a_len$}{}  {}{:<u_len$}{}  {}{:<p_len$}{}\n",
                UNDERLINE,
                "TIME",
                RESET,
                UNDERLINE,
                "CORR_ID",
                RESET,
                UNDERLINE,
                "ACTION",
                RESET,
                UNDERLINE,
                "USER",
                RESET,
                UNDERLINE,
                "PROJECT",
                RESET,
                t_len = TIME_LEN,
                c_len = corr_len,
                a_len = action_len,
                u_len = user_len,
                p_len = proj_len
            )
        } else {
            format!(
                "{:<t_len$}  {:<c_len$}  {:<a_len$}  {:<u_len$}  {:<p_len$}\n",
                "TIME",
                "CORR_ID",
                "ACTION",
                "USER",
                "PROJECT",
                t_len = TIME_LEN,
                c_len = corr_len,
                a_len = action_len,
                u_len = user_len,
                p_len = proj_len
            )
        }
    }

    fn correlation_id_len(&self) -> usize {
        self.events
            .iter()
            .map(|e| e.correlation_id.as_deref().unwrap_or_default().len())
            .max()
            .unwrap_or(DEFAULT_CORR_LEN)
            .max(DEFAULT_CORR_LEN)
    }

    fn action_len(&self) -> usize {
        self.events
            .iter()
            .map(|e| e.action.len())
            .max()
            .unwrap_or(DEFAULT_ACTION_LEN)
            .max(DEFAULT_CORR_LEN)
    }

    fn user_len(&self) -> usize {
        self.events
            .iter()
            .map(|e| e.user.as_deref().unwrap_or_default().len())
            .max()
            .unwrap_or(DEFAULT_USER_LEN)
            .max(DEFAULT_USER_LEN)
    }

    fn project_len(&self) -> usize {
        self.events
            .iter()
            .map(|e| e.project.as_deref().unwrap_or_default().len())
            .max()
            .unwrap_or(DEFAULT_PROJ_LEN)
            .max(DEFAULT_PROJ_LEN)
    }
}
