use crate::data::detail::DetailTimeMap;
use crate::data::raw::{RawDataMap, RawTimeDataMap};
use crate::data::RawOutput;
use crate::error::ErrorMap;
use crate::event::*;
use crate::execute::Reader;
use crate::parse::{self, ParseArgs};
use crate::stats::StatsType;
use crate::stats_vec::StatsVec;
use crate::time_chunks::{Interval, TimeChunks, TimeStatsMap};
use crate::top::ProdData;
use crate::StatField;

use chrono::prelude::*;
use std::io::{self, BufRead};
use std::process;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LogType {
    ApiJson,
    ApiJsonDetail,
    ApiJsonError,
    ApiJsonDurS,
    ApiJsonDurSDetail,
    ApiJsonDurSError,
    GitalyText,
    GitalyJson,
    GitalyJsonDetail,
    GitalyJsonError,
    ProdJson,
    ProdJsonDetail,
    ProdJsonError,
    ProdJsonDurS,
    ProdJsonDurSDetail,
    ProdJsonDurSError,
    ShellJson,
    SidekiqText,
    SidekiqJson,
    SidekiqJsonDetail,
    SidekiqJsonError,
    SidekiqJsonDurS,
    SidekiqJsonDurSDetail,
    SidekiqJsonDurSError,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum OutputType {
    Detail,
    Error,
}

impl LogType {
    pub fn specify(self, out_t: OutputType) -> LogType {
        use LogType::*;
        use OutputType::*;

        match self {
            ApiJson => match out_t {
                Detail => ApiJsonDetail,
                Error => ApiJsonError,
            },
            ApiJsonDurS => match out_t {
                Detail => ApiJsonDurSDetail,
                Error => ApiJsonDurSError,
            },
            ProdJson => match out_t {
                Detail => ProdJsonDetail,
                Error => ProdJsonError,
            },
            ProdJsonDurS => match out_t {
                Detail => ProdJsonDurSDetail,
                Error => ProdJsonDurSError,
            },
            GitalyText => {
                eprintln!("This option is not supported for non-JSON logs");
                std::process::exit(0);
            }
            GitalyJson => match out_t {
                Detail => GitalyJsonDetail,
                Error => GitalyJsonError,
            },
            SidekiqText => {
                eprintln!("This option is not supported for non-JSON logs");
                std::process::exit(0);
            }
            SidekiqJson => match out_t {
                Detail => SidekiqJsonDetail,
                Error => SidekiqJsonError,
            },
            SidekiqJsonDurS => match out_t {
                Detail => SidekiqJsonDurSDetail,
                Error => SidekiqJsonDurSError,
            },
            ShellJson => ShellJson,
            _ => unreachable!(),
        }
    }

    pub fn calculate_stats(&self, reader: Reader, args: ParseArgs, sort_by: StatField) -> StatsVec {
        let mut data_map: RawDataMap = self.parse_raw_data(reader, args.clone());

        if let Some(term) = args.search_for {
            data_map.filter(&term);
        }

        let dur = data_map.duration(*self);

        let mut stats = StatsVec::from_raw_data(data_map, dur, self.stats_t());
        stats.sort_by(sort_by);

        stats
    }

    pub fn calculate_time_stats(
        &self,
        reader: Reader,
        intvl: Interval,
        args: ParseArgs,
        sort_by: StatField,
    ) -> TimeStatsMap {
        self.calculate_time_chunks(reader, intvl, args)
            .into_stats(sort_by)
    }

    pub fn calculate_time_chunks(
        &self,
        reader: Reader,
        intvl: Interval,
        args: ParseArgs,
    ) -> TimeChunks {
        let mut data_map: RawTimeDataMap = self.parse_raw_data(reader, args.clone());

        if let Some(term) = args.search_for {
            data_map.filter(&term);
        }

        TimeChunks::from_time_data(data_map, intvl, *self)
    }

    pub fn calculate_top_stats(&self, reader: Reader, args: ParseArgs) -> ProdData {
        use LogType::*;

        match self {
            SidekiqText | SidekiqJson => {
                eprintln!("top subcommand does not support older Sidekiq logs");
                process::exit(0);
            }
            GitalyText => {
                eprintln!("top subcommand does not support non-JSON Gitaly logs");
                process::exit(0);
            }
            _ => self.parse_raw_data(reader, args),
        }
    }

    pub fn calculate_detailed_time_data(&self, reader: Reader, args: ParseArgs) -> DetailTimeMap {
        let mut data_map: DetailTimeMap = self.parse_raw_data(reader, args.clone());

        if let Some(term) = args.search_for {
            data_map.filter(&term);
        }

        data_map
    }

    pub fn calculate_error_summary(&self, reader: Reader, args: ParseArgs) -> ErrorMap {
        self.parse_raw_data(reader, args)
    }

    pub fn parse_raw_data<O>(&self, reader: Reader, args: ParseArgs) -> O
    where
        O: RawOutput + Send + 'static,
    {
        match reader {
            Reader::File(f) => self.parse(f, args),
            Reader::Stdin => self.parse(io::stdin().lock(), args),
        }
    }

    fn parse<B, O>(&self, reader: B, args: ParseArgs) -> O
    where
        B: BufRead,
        O: RawOutput + Send + 'static,
    {
        parse::parse::<B, O>(*self, reader, args)
    }

    pub fn parse_time(&self, time: &str) -> Option<DateTime<FixedOffset>> {
        use LogType::*;

        match self {
            ApiJson => ApiJsonEvent::parse_time(time),
            ApiJsonDetail => ApiJsonEventDetail::parse_time(time),
            ApiJsonError => ApiJsonEventError::parse_time(time),
            ApiJsonDurS => ApiJsonDurSEvent::parse_time(time),
            ApiJsonDurSDetail => ApiJsonDurSEventDetail::parse_time(time),
            ApiJsonDurSError => ApiJsonDurSEventError::parse_time(time),
            GitalyText => GitalyEvent::parse_time(time),
            GitalyJson => GitalyJsonEvent::parse_time(time),
            GitalyJsonDetail => GitalyJsonEventDetail::parse_time(time),
            GitalyJsonError => GitalyJsonEventError::parse_time(time),
            ProdJson => ProdJsonEvent::parse_time(time),
            ProdJsonDetail => ProdJsonEventDetail::parse_time(time),
            ProdJsonError => ProdJsonEventError::parse_time(time),
            ProdJsonDurS => ProdJsonDurSEvent::parse_time(time),
            ProdJsonDurSDetail => ProdJsonDurSEventDetail::parse_time(time),
            ProdJsonDurSError => ProdJsonDurSEventError::parse_time(time),
            ShellJson => ShellJsonEvent::parse_time(time),
            SidekiqText => SidekiqEvent::parse_time(time),
            SidekiqJson => SidekiqJsonEvent::parse_time(time),
            SidekiqJsonDetail => SidekiqJsonEventDetail::parse_time(time),
            SidekiqJsonError => SidekiqJsonEventError::parse_time(time),
            SidekiqJsonDurS => SidekiqJsonDurSEvent::parse_time(time),
            SidekiqJsonDurSDetail => SidekiqJsonDurSEventDetail::parse_time(time),
            SidekiqJsonDurSError => SidekiqJsonDurSEventError::parse_time(time),
        }
    }

    pub const fn stats_t(&self) -> StatsType {
        use LogType::*;

        match self {
            ApiJson | ApiJsonDetail | ApiJsonError | ApiJsonDurS | ApiJsonDurSDetail
            | ApiJsonDurSError => StatsType::Api,
            GitalyText | GitalyJson | GitalyJsonDetail | GitalyJsonError => StatsType::Gitaly,
            ProdJson | ProdJsonDetail | ProdJsonError | ProdJsonDurS | ProdJsonDurSDetail
            | ProdJsonDurSError => StatsType::Prod,
            ShellJson => StatsType::Shell,
            SidekiqText
            | SidekiqJson
            | SidekiqJsonDetail
            | SidekiqJsonError
            | SidekiqJsonDurS
            | SidekiqJsonDurSDetail
            | SidekiqJsonDurSError => StatsType::Sidekiq,
        }
    }
}
