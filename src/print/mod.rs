use crate::field::StatField;
use crate::stats::StatsType;

use chrono::Duration;

pub mod wrap;

pub fn csv_header(stats_t: StatsType, fields: &[StatField], prefix: Option<&str>) -> String {
    let mut output = match prefix {
        Some(pfx) => format!("{},{}", pfx, stats_t.header_title()),
        None => stats_t.header_title().to_string(),
    };

    for field in fields {
        output += &format!(",{}", field);
    }

    output
}

pub fn md_header(stats_t: StatsType, fields: &[StatField], prefix: Option<&str>) -> String {
    let mut output = match prefix {
        Some(pfx) => format!("|{}|{}", pfx, stats_t.header_title()),
        None => format!("|{}", stats_t.header_title()),
    };

    for field in fields {
        output += &format!("|{}", field);
    }

    let mut spacer_len = fields.len() + 1; // +1 for header title
    if prefix.is_some() {
        spacer_len += 1;
    }

    let spacers = "--|".repeat(spacer_len);
    format!("{}|\n|{}", output, spacers)
}

pub fn text_header(
    stats_t: StatsType,
    fields: &[StatField],
    lens: &[usize],
    prefix: Option<(&str, usize)>,
) -> String {
    let mut output = match prefix {
        Some((pfx, p_len)) => format!(
            "{:pfx_len$}  {:<len$}",
            pfx,
            stats_t.header_title(),
            pfx_len = p_len,
            len = lens[0]
        ),
        None => format!("{:<len$}", stats_t.header_title(), len = lens[0]),
    };

    for field in fields {
        let len = field.len(lens);
        output += &format!("  {:>width$}", field, width = len);
    }

    output
}

pub fn fmt_dur(dur_s: f64) -> String {
    let mut dur = Duration::milliseconds((dur_s * 1000.0) as i64);
    let mut out = String::new();

    if dur.num_hours() > 0 {
        out += &format!("{}h", dur.num_hours());
    }
    dur = dur - Duration::hours(dur.num_hours());

    if !out.is_empty() {
        out += &format!("{:02}m", dur.num_minutes());
    } else if dur.num_minutes() > 0 {
        out += &format!("{}m", dur.num_minutes());
    }
    dur = dur - Duration::minutes(dur.num_minutes());

    let secs = dur.num_milliseconds() as f64 / 1000.0;
    if out.is_empty() {
        out += &format!("{:03.1}s", secs);
    } else {
        out += &format!("{:04.1}s", secs);
    }

    out
}
