use unicode_segmentation::UnicodeSegmentation;

pub fn bare(lines: &[String]) -> String {
    let mut out = lines.join("\n");
    out.push('\n');

    out
}

pub fn wrap_lines(lines: &[String]) -> String {
    let mut out = String::new();

    // Convert tabs to spaces and unescape newlines for easier formatting
    let detabbed: Vec<_> = lines
        .iter()
        .cloned()
        .map(|l| l.replace('\t', " "))
        .map(|l| l.replace("\\n", "\n"))
        .collect();
    let mut iter = detabbed.iter().filter(|l| !is_empty_line(l)).peekable();

    let first_line = iter.peek();
    if first_line.is_none() {
        return String::new();
    }

    let header_len = first_line.map(|l| multiline_len(l)).unwrap_or_default();
    let header = line_header(header_len, &[]);

    out.push_str(&header);

    let mut prev_len = visible_len(&header);

    while let Some(line) = iter.next() {
        if is_empty_line(line) {
            continue;
        }

        let (l, len) = wrap_multiline(line);
        let curr_len = len.min(prev_len);

        let separator = match iter.peek() {
            Some(nl) => {
                let nl_len = multiline_len(nl);
                line_horizontal(curr_len, nl_len, &[curr_len - 1], &[nl_len - 1])
            }
            None => line_footer(len, &[curr_len]),
        };

        prev_len = separator.len();

        out.push_str(&l);
        out.push_str(&separator);
    }

    out
}

// Wrap multi-line strings and return length of longest line
fn wrap_multiline(chunk: &str) -> (String, usize) {
    let mut out = String::new();
    let max_len = chunk.lines().map(visible_len).max().unwrap_or_default();

    for line in chunk.lines().map(|line| line.trim_end()) {
        let mut newline = String::from("│");
        newline.push_str(line);

        let mut len = visible_len(line);
        if len < max_len {
            while len < max_len {
                newline.push(' ');
                len += 1;
            }
        }
        newline.push_str("│\n");

        out.push_str(&newline);
    }

    (out, max_len + 2)
}

fn line_horizontal(
    len: usize,
    next_len: usize,
    high_intersections: &[usize],
    low_intersections: &[usize],
) -> String {
    let mut out = String::from("├");

    let write_len = len.max(next_len);

    for i in 1..(write_len - 1) {
        match (
            high_intersections.contains(&i),
            low_intersections.contains(&i),
        ) {
            (true, true) => out.push('┼'),
            (true, false) => out.push('┴'),
            (false, true) => out.push('┬'),
            (false, false) => out.push('─'),
        }
    }

    match (len, next_len) {
        (l, n) if l > n => out.push_str("┘\n"),
        (l, n) if l < n => out.push_str("┐\n"),
        (_, _) => out.push_str("┤\n"),
    }

    out
}

fn line_header(len: usize, intersections: &[usize]) -> String {
    let mut out = String::from("┌");

    for i in 1..(len - 1) {
        match intersections.contains(&i) {
            true => out.push('┬'),
            false => out.push('─'),
        }
    }
    out.push_str("┐\n");

    out
}

fn line_footer(len: usize, intersections: &[usize]) -> String {
    let mut out = String::from("└");

    for i in 1..(len - 1) {
        match intersections.contains(&i) {
            true => out.push('┴'),
            false => out.push('─'),
        }
    }
    out.push_str("┘\n");

    out
}

fn visible_len(s: &str) -> usize {
    let mut ct = 0;
    let mut in_color = false;

    for item in s.trim_end().graphemes(true) {
        if item == "\x1b" {
            in_color = true;
        }

        if !in_color {
            ct += 1;
        }

        if in_color && item == "m" {
            in_color = false;
        }
    }

    ct
}

fn multiline_len(s: &str) -> usize {
    s.lines().map(visible_len).max().unwrap_or_default() + 2
}

fn is_empty_line(s: &str) -> bool {
    for line in s.lines() {
        if !line.trim().is_empty() {
            return false;
        }
    }

    true
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::util::*;

    #[test]
    fn wrap_one_line() {
        let lines = vec![String::from("12345")];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             └─────┘\n"
        );
    }

    #[test]
    fn wrap_two_lines_eq_len() {
        let lines = vec![String::from("12345"), String::from("12345")];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             ├─────┤\n\
             │12345│\n\
             └─────┘\n"
        );
    }

    #[test]
    fn wrap_three_lines_eq_len() {
        let lines = vec![
            String::from("12345"),
            String::from("12345"),
            String::from("12345"),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             ├─────┤\n\
             │12345│\n\
             ├─────┤\n\
             │12345│\n\
             └─────┘\n"
        );
    }

    #[test]
    fn wrap_two_lines_long_top() {
        let lines = vec![String::from("1234567890"), String::from("12345")];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌──────────┐\n\
             │1234567890│\n\
             ├─────┬────┘\n\
             │12345│\n\
             └─────┘\n"
        );
    }

    #[test]
    fn wrap_two_lines_long_bottom() {
        let lines = vec![String::from("12345"), String::from("1234567890")];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             ├─────┴────┐\n\
             │1234567890│\n\
             └──────────┘\n"
        );
    }

    #[test]
    fn wrap_three_lines_long_middle() {
        let lines = vec![
            String::from("12345"),
            String::from("1234567890"),
            String::from("12345"),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             ├─────┴────┐\n\
             │1234567890│\n\
             ├─────┬────┘\n\
             │12345│\n\
             └─────┘\n"
        );
    }

    #[test]
    fn wrap_three_lines_short_middle() {
        let lines = vec![
            String::from("1234567890"),
            String::from("12345"),
            String::from("1234567890"),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌──────────┐\n\
             │1234567890│\n\
             ├─────┬────┘\n\
             │12345│\n\
             ├─────┴────┐\n\
             │1234567890│\n\
             └──────────┘\n"
        );
    }

    #[test]
    fn wrap_four_lines_alternating() {
        let lines = vec![
            String::from("12345"),
            String::from("1234567890"),
            String::from("12345"),
            String::from("1234567890"),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             ├─────┴────┐\n\
             │1234567890│\n\
             ├─────┬────┘\n\
             │12345│\n\
             ├─────┴────┐\n\
             │1234567890│\n\
             └──────────┘\n"
        );
    }

    #[test]
    fn wrap_four_lines_middle() {
        let lines = vec![
            String::from("12345"),
            String::from("1234567890"),
            String::from("1234567890"),
            String::from("12345"),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             ├─────┴────┐\n\
             │1234567890│\n\
             ├──────────┤\n\
             │1234567890│\n\
             ├─────┬────┘\n\
             │12345│\n\
             └─────┘\n"
        );
    }

    #[test]
    fn wrap_single_multiline() {
        let lines = vec![String::from("12345\n12345")];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             │12345│\n\
             └─────┘\n"
        );
    }

    #[test]
    fn wrap_single_multiline_top_long() {
        let lines = vec![String::from("1234567890\n12345")];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌──────────┐\n\
             │1234567890│\n\
             │12345     │\n\
             └──────────┘\n"
        );
    }

    #[test]
    fn wrap_single_multiline_bottom_long() {
        let lines = vec![String::from("12345\n1234567890")];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌──────────┐\n\
             │12345     │\n\
             │1234567890│\n\
             └──────────┘\n"
        );
    }

    #[test]
    fn wrap_two_multiline() {
        let lines = vec![
            String::from("12345\n1234567890"),
            String::from("1234567890\n12345"),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌──────────┐\n\
             │12345     │\n\
             │1234567890│\n\
             ├──────────┤\n\
             │1234567890│\n\
             │12345     │\n\
             └──────────┘\n"
        );
    }

    #[test]
    fn wrap_two_multiline_long_top() {
        let lines = vec![
            String::from("12345\n1234567890"),
            String::from("12345\n12345"),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌──────────┐\n\
             │12345     │\n\
             │1234567890│\n\
             ├─────┬────┘\n\
             │12345│\n\
             │12345│\n\
             └─────┘\n"
        );
    }

    #[test]
    fn wrap_two_multiline_long_bottom() {
        let lines = vec![
            String::from("12345\n12345"),
            String::from("1234567890\n12345"),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             │12345│\n\
             ├─────┴────┐\n\
             │1234567890│\n\
             │12345     │\n\
             └──────────┘\n"
        );
    }

    #[test]
    fn wrap_multiline_string_with_tab() {
        let lines = vec![
            String::from("rpc error: code = Internal desc = close stream to gitaly-ruby: rpc error: code = Unknown desc = Gitlab::Git::CommandError: To https://git-codecommit.us-west-2.amazonaws.com/v1/repos/my-repo\n\
            \trefs/heads/master:refs/heads/master\t6a11803..ce3c606\n\
            Done\n\
            error: unable to delete 'mybranch': remote ref does not exist\n\
            error: failed to push some refs to 'https://[FILTERED]@git-codecommit.us-west-2.amazonaws.com/v1/repos/my-repo'\n")
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐\n\
             │rpc error: code = Internal desc = close stream to gitaly-ruby: rpc error: code = Unknown desc = Gitlab::Git::CommandError: To https://git-codecommit.us-west-2.amazonaws.com/v1/repos/my-repo│\n\
             │ refs/heads/master:refs/heads/master 6a11803..ce3c606                                                                                                                                        │\n\
             │Done                                                                                                                                                                                         │\n\
             │error: unable to delete 'mybranch': remote ref does not exist                                                                                                                                │\n\
             │error: failed to push some refs to 'https://[FILTERED]@git-codecommit.us-west-2.amazonaws.com/v1/repos/my-repo'                                                                              │\n\
             └─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘\n"
        );
    }

    #[test]
    fn wrap_escaped_newlines() {
        let lines = vec![
            String::from("From /git_data/repositories/+gitaly/tmp/repo234212586/repo.bundle\\n\
            * [new ref]         refs/merge-requests/4/merge -> refs/merge-requests/4/merge\\n\
            * [new ref]         refs/merge-requests/4/head  -> refs/merge-requests/4/head\\n\
            * [new ref]         refs/merge-requests/3/merge -> refs/merge-requests/3/merge\\n\
            * [new ref]         refs/merge-requests/3/head  -> refs/merge-requests/3/head\\n\
            * [new ref]         refs/merge-requests/2/head  -> refs/merge-requests/2/head\\n\
            * [new ref]         refs/merge-requests/1/head  -> refs/merge-requests/1/head\\n\
            * [new ref]         refs/keep-around/e3e2f106a754864cd0dd3b515b789f3b34c4947f -> refs/keep-around/e3e2f106a754864cd0dd3b515b789f3b34c4947f\\n\
            * [new ref]         refs/keep-around/bebddcea1f95d3943b7dd618c0a2224c8a07602b -> refs/keep-around/bebddcea1f95d3943b7dd618c0a2224c8a07602b\\n\
            * [new ref]         refs/keep-around/b6d8bd98de9422bd5c74937f375b602a55a77dc7 -> refs/keep-around/b6d8bd98de9422bd5c74937f375b602a55a77dc7\\n\
            * [new ref]         refs/keep-around/a759f92959a570bde94ca46e79d5ec29ce497170 -> refs/keep-around/a759f92959a570bde94ca46e79d5ec29ce497170\\n\
            * [new ref]         refs/keep-around/a42ac43f066c5edb844c0426870e0b1174ff46e6 -> refs/keep-around/a42ac43f066c5edb844c0426870e0b1174ff46e6\\n\
            * [new ref]         refs/keep-around/803ea620672e1e602e1ea0e08b28aa037d6b4746 -> refs/keep-around/803ea620672e1e602e1ea0e08b28aa037d6b4746\\n\
            * [new ref]         refs/keep-around/6ffd770e815ca3f627754c8f459bf26564d2e2cd -> refs/keep-around/6ffd770e815ca3f627754c8f459bf26564d2e2cd\\n\
            * [new ref]         refs/keep-around/61076c01c6b7d4cb335173939fa0d856cf1eea14 -> refs/keep-around/61076c01c6b7d4cb335173939fa0d856cf1eea14\\n\
            * [new ref]         refs/keep-around/5d2bb14ad533fe38ff98f2f21101db8c77299198 -> refs/keep-around/5d2bb14ad533fe38ff98f2f21101db8c77299198\\n\
            * [new ref]         refs/keep-around/55b06fa0bdb0f0fdb2ebf7ced9c4698bdcac9aaf -> refs/keep-around/55b06fa0bdb0f0fdb2ebf7ced9c4698bdcac9aaf\\n\
            * [new ref]         refs/keep-around/41728e4df3b86d9d64029ab6df3e251dd2daa191 -> refs/keep-around/41728e4df3b86d9d64029ab6df3e251dd2daa191\\n\
            * [new ref]         refs/keep-around/3b3f7257becbeef25816d31063dffba7e959b6d8 -> refs/keep-around/3b3f7257becbeef25816d31063dffba7e959b6d8\\n\
            * [new ref]         refs/keep-around/37b4e1dafa9fa3cd66f4b0792b1409db7ee8881b -> refs/keep-around/37b4e1dafa9fa3cd66f4b0792b1409db7ee8881b\\n\
            * [new ref]         refs/keep-around/3082784e8d7b9bbfc8ee1931455bbabd84cc9f19 -> refs/keep-around/3082784e8d7b9bbfc8ee1931455bbabd84cc9f19\\n\
            * [new ref]         refs/keep-around/0bad98b2bcb68f5cb105f0386cd2ad191cfb61b8 -> refs/keep-around/0bad98b2bcb68f5cb105f0386cd2ad191cfb61b8\\n\
            * [new ref]         refs/keep-around/0572001bf26194550e9a4fd81712582fb359a57b -> refs/keep-around/0572001bf26194550e9a4fd81712582fb359a57b\\n")
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐\n\
             │From /git_data/repositories/+gitaly/tmp/repo234212586/repo.bundle                                                                         │\n\
             │* [new ref]         refs/merge-requests/4/merge -> refs/merge-requests/4/merge                                                            │\n\
             │* [new ref]         refs/merge-requests/4/head  -> refs/merge-requests/4/head                                                             │\n\
             │* [new ref]         refs/merge-requests/3/merge -> refs/merge-requests/3/merge                                                            │\n\
             │* [new ref]         refs/merge-requests/3/head  -> refs/merge-requests/3/head                                                             │\n\
             │* [new ref]         refs/merge-requests/2/head  -> refs/merge-requests/2/head                                                             │\n\
             │* [new ref]         refs/merge-requests/1/head  -> refs/merge-requests/1/head                                                             │\n\
             │* [new ref]         refs/keep-around/e3e2f106a754864cd0dd3b515b789f3b34c4947f -> refs/keep-around/e3e2f106a754864cd0dd3b515b789f3b34c4947f│\n\
             │* [new ref]         refs/keep-around/bebddcea1f95d3943b7dd618c0a2224c8a07602b -> refs/keep-around/bebddcea1f95d3943b7dd618c0a2224c8a07602b│\n\
             │* [new ref]         refs/keep-around/b6d8bd98de9422bd5c74937f375b602a55a77dc7 -> refs/keep-around/b6d8bd98de9422bd5c74937f375b602a55a77dc7│\n\
             │* [new ref]         refs/keep-around/a759f92959a570bde94ca46e79d5ec29ce497170 -> refs/keep-around/a759f92959a570bde94ca46e79d5ec29ce497170│\n\
             │* [new ref]         refs/keep-around/a42ac43f066c5edb844c0426870e0b1174ff46e6 -> refs/keep-around/a42ac43f066c5edb844c0426870e0b1174ff46e6│\n\
             │* [new ref]         refs/keep-around/803ea620672e1e602e1ea0e08b28aa037d6b4746 -> refs/keep-around/803ea620672e1e602e1ea0e08b28aa037d6b4746│\n\
             │* [new ref]         refs/keep-around/6ffd770e815ca3f627754c8f459bf26564d2e2cd -> refs/keep-around/6ffd770e815ca3f627754c8f459bf26564d2e2cd│\n\
             │* [new ref]         refs/keep-around/61076c01c6b7d4cb335173939fa0d856cf1eea14 -> refs/keep-around/61076c01c6b7d4cb335173939fa0d856cf1eea14│\n\
             │* [new ref]         refs/keep-around/5d2bb14ad533fe38ff98f2f21101db8c77299198 -> refs/keep-around/5d2bb14ad533fe38ff98f2f21101db8c77299198│\n\
             │* [new ref]         refs/keep-around/55b06fa0bdb0f0fdb2ebf7ced9c4698bdcac9aaf -> refs/keep-around/55b06fa0bdb0f0fdb2ebf7ced9c4698bdcac9aaf│\n\
             │* [new ref]         refs/keep-around/41728e4df3b86d9d64029ab6df3e251dd2daa191 -> refs/keep-around/41728e4df3b86d9d64029ab6df3e251dd2daa191│\n\
             │* [new ref]         refs/keep-around/3b3f7257becbeef25816d31063dffba7e959b6d8 -> refs/keep-around/3b3f7257becbeef25816d31063dffba7e959b6d8│\n\
             │* [new ref]         refs/keep-around/37b4e1dafa9fa3cd66f4b0792b1409db7ee8881b -> refs/keep-around/37b4e1dafa9fa3cd66f4b0792b1409db7ee8881b│\n\
             │* [new ref]         refs/keep-around/3082784e8d7b9bbfc8ee1931455bbabd84cc9f19 -> refs/keep-around/3082784e8d7b9bbfc8ee1931455bbabd84cc9f19│\n\
             │* [new ref]         refs/keep-around/0bad98b2bcb68f5cb105f0386cd2ad191cfb61b8 -> refs/keep-around/0bad98b2bcb68f5cb105f0386cd2ad191cfb61b8│\n\
             │* [new ref]         refs/keep-around/0572001bf26194550e9a4fd81712582fb359a57b -> refs/keep-around/0572001bf26194550e9a4fd81712582fb359a57b│\n\
             └──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘\n"
        );
    }

    #[test]
    fn wrap_skip_empty_string() {
        let lines = vec![
            String::from("12345"),
            String::new(),
            String::from("1234567890"),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             ├─────┴────┐\n\
             │1234567890│\n\
             └──────────┘\n"
        );
    }

    #[test]
    fn wrap_skip_leading_empty_string() {
        let lines = vec![
            String::new(),
            String::from("12345"),
            String::from("1234567890"),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             ├─────┴────┐\n\
             │1234567890│\n\
             └──────────┘\n"
        );
    }

    #[test]
    fn wrap_skip_trailing_empty_string() {
        let lines = vec![
            String::from("12345"),
            String::from("1234567890"),
            String::new(),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             ├─────┴────┐\n\
             │1234567890│\n\
             └──────────┘\n"
        );
    }

    #[test]
    fn wrap_skip_many_empty_strings() {
        let lines = vec![
            String::new(),
            String::new(),
            String::from("12345"),
            String::new(),
            String::new(),
            String::new(),
            String::from("1234567890"),
            String::new(),
            String::new(),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────┐\n\
             │12345│\n\
             ├─────┴────┐\n\
             │1234567890│\n\
             └──────────┘\n"
        );
    }

    #[test]
    fn wrap_color() {
        let lines = vec![
            format!(
                "{}Error{}: GRPC::DeadlineExceeded 4:Deadline Exceeded",
                BLUE, RESET
            ),
            format!("{}Count{}: 15", BLUE, RESET),
        ];

        #[rustfmt::skip]
        assert_eq!(
            wrap_lines(&lines),
            "┌─────────────────────────────────────────────────┐\n\
             │\x1b[34mError\x1b[0m: GRPC::DeadlineExceeded 4:Deadline Exceeded│\n\
             ├─────────┬───────────────────────────────────────┘\n\
             │\x1b[34mCount\x1b[0m: 15│\n\
             └─────────┘\n"
        );
    }
}
