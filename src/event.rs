use crate::log_type::LogType;
use crate::stats::StatsType;

use chrono::prelude::*;
use serde::Deserialize;
use std::borrow::Cow;

pub trait Event<'a>: Send + Sized {
    fn action(&self) -> Cow<'a, str>;
    fn dur_s(&self) -> f64;
    fn dur_ms(&self) -> f64;
    fn status(&self) -> Status;
    fn parse(s: &'a str) -> Option<Self>;
    fn parse_time(time: &str) -> Option<DateTime<FixedOffset>>;
    fn raw_time(&self) -> &str;
    fn log_t() -> LogType;
    fn stats_t() -> StatsType;
    fn path(&self) -> Option<&str>;
    fn project(&self) -> Option<&str>;
    fn correlation_id(&self) -> Option<&str>;
    fn user(&self) -> Option<&str>;
    fn client_type(&self) -> Option<&str>;
    fn db(&self) -> Option<f64>;
    fn gitaly(&self) -> Option<f64>;
    fn rugged(&self) -> Option<f64>;
    fn redis(&self) -> Option<f64>;
    fn queue(&self) -> Option<f64>;
    fn cpu_s(&self) -> Option<f64>;
    fn error(&self) -> Option<Cow<'a, str>>;
    fn backtrace(&self, len: usize, indent: usize) -> Option<String>;
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ApiJsonEvent<'a> {
    route: &'a str,
    status: u16,
    duration: f64,
    time: &'a str,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ApiJsonEventDetail<'a> {
    route: &'a str,
    status: u16,
    duration: f64,
    time: &'a str,
    path: Option<&'a str>,
    username: Option<&'a str>,
    db: Option<f64>,
    queue_duration: Option<f64>,
    cpu_s: Option<f64>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ApiJsonEventError<'a> {
    route: &'a str,
    status: u16,
    duration: f64,
    time: &'a str,
    username: Option<&'a str>,
    error: Option<&'a str>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ApiJsonDurSEvent<'a> {
    route: &'a str,
    status: u16,
    duration_s: f64,
    time: &'a str,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ApiJsonDurSEventDetail<'a> {
    route: &'a str,
    status: u16,
    duration_s: f64,
    time: &'a str,
    path: &'a str,
    username: Option<&'a str>,
    #[serde(rename = "meta.project")]
    meta_project: Option<&'a str>,
    #[serde(rename = "meta.user")]
    meta_user: Option<&'a str>,
    db_duration_s: Option<f64>,
    gitaly_duration_s: Option<f64>,
    rugged_duration_s: Option<f64>,
    redis_duration_s: Option<f64>,
    cpu_s: Option<f64>,
    queue_duration_s: Option<f64>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ApiJsonDurSEventError<'a> {
    route: &'a str,
    status: u16,
    duration_s: f64,
    time: &'a str,
    username: Option<&'a str>,
    #[serde(rename = "meta.project")]
    meta_project: Option<&'a str>,
    #[serde(rename = "meta.user")]
    meta_user: Option<&'a str>,
    #[serde(rename = "exception.class")]
    exception_class: Option<&'a str>,
    #[serde(rename = "exception.message")]
    exception_message: Option<Cow<'a, str>>,
    #[serde(rename = "exception.backtrace")]
    exception_backtrace: Option<Vec<&'a str>>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ProdJsonEvent<'a> {
    controller: &'a str,
    action: &'a str,
    status: u16,
    duration: f64,
    time: &'a str,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ProdJsonEventDetail<'a> {
    controller: &'a str,
    action: &'a str,
    status: u16,
    duration: f64,
    time: &'a str,
    path: &'a str,
    username: Option<&'a str>,
    db: Option<f64>,
    queue_duration: Option<f64>,
    cpu_s: Option<f64>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ProdJsonEventError<'a> {
    controller: &'a str,
    action: &'a str,
    status: u16,
    duration: f64,
    time: &'a str,
    username: Option<&'a str>,
    error: Option<&'a str>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ProdJsonDurSEvent<'a> {
    controller: &'a str,
    action: &'a str,
    status: u16,
    duration_s: f64,
    time: &'a str,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ProdJsonDurSEventDetail<'a> {
    controller: &'a str,
    action: &'a str,
    status: u16,
    duration_s: f64,
    time: &'a str,
    path: &'a str,
    username: Option<&'a str>,
    #[serde(rename = "meta.project")]
    meta_project: Option<&'a str>,
    #[serde(rename = "meta.user")]
    meta_user: Option<&'a str>,
    db_duration_s: Option<f64>,
    gitaly_duration_s: Option<f64>,
    rugged_duration_s: Option<f64>,
    redis_duration_s: Option<f64>,
    cpu_s: Option<f64>,
    queue_duration_s: Option<f64>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ProdJsonDurSEventError<'a> {
    controller: &'a str,
    action: &'a str,
    status: u16,
    duration_s: f64,
    time: &'a str,
    path: &'a str,
    username: Option<&'a str>,
    #[serde(rename = "meta.project")]
    meta_project: Option<&'a str>,
    #[serde(rename = "meta.user")]
    meta_user: Option<&'a str>,
    #[serde(rename = "exception.class")]
    exception_class: Option<&'a str>,
    #[serde(rename = "exception.message")]
    exception_message: Option<Cow<'a, str>>,
    #[serde(rename = "exception.backtrace")]
    exception_backtrace: Option<Vec<&'a str>>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct ShellJsonEvent<'a> {
    url: Option<&'a str>,
    status: Option<u16>,
    duration_ms: Option<f64>,
    #[serde(rename = "gl_key_id")]
    key: Option<u32>,
    #[serde(rename = "gl_project_path")]
    project: Option<&'a str>,
    username: Option<&'a str>,
    msg: &'a str,
    time: &'a str,
    correlation_id: &'a str,
}

#[derive(PartialEq, Debug)]
pub struct GitalyEvent<'a> {
    method: &'a str,
    time_ms: f64,
    status: Status,
    time: &'a str,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct GitalyJsonEvent<'a> {
    #[serde(rename = "grpc.method")]
    method: &'a str,
    #[serde(rename = "grpc.time_ms")]
    time_ms: f64,
    #[serde(rename = "grpc.code")]
    code: &'a str,
    time: &'a str,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct GitalyJsonEventDetail<'a> {
    #[serde(rename = "grpc.method")]
    method: &'a str,
    #[serde(rename = "grpc.time_ms")]
    time_ms: f64,
    #[serde(rename = "grpc.code")]
    code: &'a str,
    time: &'a str,
    #[serde(rename = "grpc.request.glProjectPath")]
    project: Option<&'a str>,
    #[serde(rename = "grpc.meta.client_name")]
    client_type: Option<&'a str>,
    username: Option<&'a str>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct GitalyJsonEventError<'a> {
    #[serde(rename = "grpc.method")]
    method: &'a str,
    #[serde(rename = "grpc.time_ms")]
    time_ms: Option<f64>,
    #[serde(rename = "grpc.code")]
    code: Option<&'a str>,
    time: &'a str,
    #[serde(rename = "grpc.request.glProjectPath")]
    project: Option<&'a str>,
    error: Option<Cow<'a, str>>,
    msg: Cow<'a, str>, // Serde may need to escape new lines here, so this may be an owned string sometimes
    level: &'a str,
    #[serde(rename = "grpc.meta.client_name")]
    client_type: Option<&'a str>,
    username: Option<&'a str>,
    correlation_id: Option<&'a str>,
}

#[derive(PartialEq, Debug)]
pub struct SidekiqEvent<'a> {
    class: &'a str,
    duration: f64,
    status: Status,
    time: &'a str,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct SidekiqJsonEvent<'a> {
    class: &'a str,
    duration: f64,
    #[serde(rename = "job_status")]
    status: &'a str,
    time: &'a str,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct SidekiqJsonEventDetail<'a> {
    class: &'a str,
    duration: f64,
    #[serde(rename = "job_status")]
    status: &'a str,
    time: &'a str,
    #[serde(rename = "meta.project")]
    meta_project: Option<&'a str>,
    #[serde(rename = "meta.user")]
    meta_user: Option<&'a str>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct SidekiqJsonEventError<'a> {
    class: Option<&'a str>,
    #[serde(rename = "job_status")]
    status: Option<&'a str>,
    time: &'a str,
    #[serde(rename = "meta.project")]
    meta_project: Option<&'a str>,
    #[serde(rename = "meta.user")]
    meta_user: Option<&'a str>,
    message: Option<&'a str>,
    error_class: Option<&'a str>,
    error_message: Option<&'a str>,
    error_backtrace: Option<Vec<&'a str>>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct SidekiqJsonDurSEvent<'a> {
    class: &'a str,
    duration_s: f64,
    #[serde(rename = "job_status")]
    status: &'a str,
    time: &'a str,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct SidekiqJsonDurSEventDetail<'a> {
    class: &'a str,
    duration_s: f64,
    #[serde(rename = "job_status")]
    status: &'a str,
    time: &'a str,
    #[serde(rename = "meta.project")]
    meta_project: Option<&'a str>,
    #[serde(rename = "meta.user")]
    meta_user: Option<&'a str>,
    db_duration_s: Option<f64>,
    gitaly_duration_s: Option<f64>,
    rugged_duration_s: Option<f64>,
    redis_duration_s: Option<f64>,
    scheduling_latency_s: Option<f64>,
    cpu_s: Option<f64>,
    correlation_id: Option<&'a str>,
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct SidekiqJsonDurSEventError<'a> {
    class: Option<&'a str>,
    time: &'a str,
    #[serde(rename = "job_status")]
    status: Option<&'a str>,
    #[serde(rename = "meta.project")]
    meta_project: Option<&'a str>,
    #[serde(rename = "meta.user")]
    meta_user: Option<&'a str>,
    message: Option<&'a str>,
    error_class: Option<&'a str>,
    error_message: Option<Cow<'a, str>>, // Serde may need to escape new lines here, so this may be an owned string sometimes
    error_backtrace: Option<Vec<&'a str>>,
    correlation_id: Option<&'a str>,
    severity: &'a str,
}

macro_rules! base_impl {
    ($type:ty, $stats_t:tt, $log_t:tt) => {
        #[inline]
        fn parse(s: &str) -> Option<$type> {
            serde_json::from_str::<$type>(&s).ok()
        }

        fn parse_time(time: &str) -> Option<DateTime<FixedOffset>> {
            DateTime::parse_from_rfc3339(time).ok()
        }

        fn stats_t() -> StatsType {
            StatsType::$stats_t
        }

        fn log_t() -> LogType {
            LogType::$log_t
        }
    };
}

macro_rules! none_impl {
    ($func:ident, $ret:ty) => {
        #[inline]
        fn $func(&self) -> Option<$ret> {
            None
        }
    };
}

macro_rules! api_impl {
    ($type:ty, $stats_t:tt, $log_t:tt) => {
        #[inline]
        fn action(&self) -> Cow<'a, str> {
            Cow::from(self.route)
        }

        #[inline]
        fn status(&self) -> Status {
            if self.status >= 500 {
                Status::Fail
            } else {
                Status::Success
            }
        }

        fn raw_time(&self) -> &str {
            &self.time
        }

        base_impl!($type, $stats_t, $log_t);
    };
}

macro_rules! prod_impl {
    ($type:ty, $stats_t:tt, $log_t:tt) => {
        #[inline]
        fn action(&self) -> Cow<'a, str> {
            Cow::from(format!("{}#{}", self.controller, self.action))
        }

        #[inline]
        fn status(&self) -> Status {
            if self.status >= 500 {
                Status::Fail
            } else {
                Status::Success
            }
        }

        fn raw_time(&self) -> &str {
            &self.time
        }

        base_impl!($type, $stats_t, $log_t);
    };
}

macro_rules! gitaly_impl {
    ($type:ty, $stats_t:tt, $log_t:tt) => {
        #[inline]
        fn action(&self) -> Cow<'a, str> {
            Cow::from(self.method)
        }

        fn raw_time(&self) -> &str {
            &self.time
        }

        base_impl!($type, $stats_t, $log_t);
    };
}

macro_rules! sidekiq_impl {
    ($type:ty, $stats_t:tt, $log_t:tt) => {
        #[inline]
        fn action(&self) -> Cow<'a, str> {
            Cow::from(self.class)
        }

        #[inline]
        fn status(&self) -> Status {
            match self.status {
                "done" => Status::Success,
                _ => Status::Fail,
            }
        }

        fn raw_time(&self) -> &str {
            &self.time
        }

        base_impl!($type, $stats_t, $log_t);
    };
}

impl<'a> Event<'a> for ApiJsonEvent<'a> {
    api_impl!(ApiJsonEvent, Api, ApiJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration / 1000.0
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(user, &str);
    none_impl!(path, &str);
    none_impl!(correlation_id, &str);
    none_impl!(db, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(project, &str);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for ApiJsonEventDetail<'a> {
    api_impl!(ApiJsonEventDetail, Api, ApiJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration / 1000.0
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        self.username
    }

    #[inline]
    fn path(&self) -> Option<&str> {
        self.path
    }

    #[inline]
    fn db(&self) -> Option<f64> {
        self.db.map(|d| d / 1000.0)
    }

    #[inline]
    fn queue(&self) -> Option<f64> {
        self.queue_duration.map(|d| d / 1000.0)
    }

    #[inline]
    fn cpu_s(&self) -> Option<f64> {
        self.cpu_s
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(project, &str);
    none_impl!(error, Cow<'a, str>);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(client_type, &str);
}

impl<'a> Event<'a> for ApiJsonEventError<'a> {
    api_impl!(ApiJsonEventError, Api, ApiJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration / 1000.0
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration
    }

    #[inline]
    fn error(&self) -> Option<Cow<'a, str>> {
        self.error.map(Cow::from)
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        self.username
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    none_impl!(project, &str);
    none_impl!(path, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
}

impl<'a> Event<'a> for ApiJsonDurSEvent<'a> {
    api_impl!(ApiJsonDurSEvent, Api, ApiJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration_s
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration_s * 1000.0
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(path, &str);
    none_impl!(project, &str);
    none_impl!(correlation_id, &str);
    none_impl!(user, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for ApiJsonDurSEventDetail<'a> {
    api_impl!(ApiJsonDurSEventDetail, Api, ApiJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration_s
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration_s * 1000.0
    }

    #[inline]
    fn path(&self) -> Option<&str> {
        Some(self.path)
    }

    #[inline]
    fn project(&self) -> Option<&str> {
        self.meta_project
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        match (&self.username, &self.meta_user) {
            (Some(u), _) => Some(u),
            (None, Some(u)) => Some(u),
            _ => None,
        }
    }

    #[inline]
    fn db(&self) -> Option<f64> {
        self.db_duration_s
    }

    #[inline]
    fn gitaly(&self) -> Option<f64> {
        self.gitaly_duration_s
    }

    #[inline]
    fn rugged(&self) -> Option<f64> {
        self.rugged_duration_s
    }

    #[inline]
    fn redis(&self) -> Option<f64> {
        self.redis_duration_s
    }

    #[inline]
    fn queue(&self) -> Option<f64> {
        self.queue_duration_s
    }

    #[inline]
    fn cpu_s(&self) -> Option<f64> {
        self.cpu_s
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for ApiJsonDurSEventError<'a> {
    api_impl!(ApiJsonDurSEventError, Api, ApiJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration_s
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration_s * 1000.0
    }

    #[inline]
    fn project(&self) -> Option<&str> {
        self.meta_project
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        match (&self.username, &self.meta_user) {
            (Some(u), _) => Some(u),
            (None, Some(u)) => Some(u),
            _ => None,
        }
    }

    #[inline]
    fn error(&self) -> Option<Cow<'a, str>> {
        if let Some(err) = self.exception_class {
            return Some(Cow::from(format!(
                "{} {}",
                err,
                self.exception_message
                    .as_deref()
                    .unwrap_or_default()
                    .trim_end()
            )));
        }

        None
    }

    #[inline]
    fn backtrace(&self, len: usize, indent: usize) -> Option<String> {
        if let Some(bt) = self.exception_backtrace.as_ref() {
            if bt.is_empty() {
                return None;
            }

            let indent_str = " ".repeat(indent);
            let trace = bt.iter().take(len).fold(indent_str.clone(), |trace, line| {
                trace + line + "\n" + &indent_str
            });
            return Some(trace.trim_end().to_string());
        }

        None
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    none_impl!(path, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
}

impl<'a> Event<'a> for ProdJsonEvent<'a> {
    prod_impl!(ProdJsonEvent, Prod, ProdJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration / 1000.0
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(path, &str);
    none_impl!(project, &str);
    none_impl!(correlation_id, &str);
    none_impl!(user, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for ProdJsonEventDetail<'a> {
    prod_impl!(ProdJsonEventDetail, Prod, ProdJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration / 1000.0
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        self.username
    }

    #[inline]
    fn path(&self) -> Option<&str> {
        Some(self.path)
    }

    #[inline]
    fn db(&self) -> Option<f64> {
        self.db.map(|d| d / 1000.0)
    }

    #[inline]
    fn queue(&self) -> Option<f64> {
        self.queue_duration.map(|d| d / 1000.0)
    }

    #[inline]
    fn cpu_s(&self) -> Option<f64> {
        self.cpu_s
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(project, &str);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for ProdJsonEventError<'a> {
    prod_impl!(ProdJsonEventError, Prod, ProdJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration / 1000.0
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration
    }

    #[inline]
    fn error(&self) -> Option<Cow<'a, str>> {
        self.error.map(Cow::from)
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        self.username
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    none_impl!(project, &str);
    none_impl!(path, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
}

impl<'a> Event<'a> for ProdJsonDurSEvent<'a> {
    prod_impl!(ProdJsonDurSEvent, Prod, ProdJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration_s
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration_s * 1000.0
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(project, &str);
    none_impl!(user, &str);
    none_impl!(path, &str);
    none_impl!(correlation_id, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for ProdJsonDurSEventDetail<'a> {
    prod_impl!(ProdJsonDurSEventDetail, Prod, ProdJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration_s
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration_s * 1000.0
    }

    #[inline]
    fn path(&self) -> Option<&str> {
        Some(self.path)
    }

    #[inline]
    fn project(&self) -> Option<&str> {
        if self.meta_project.is_some() {
            return self.meta_project;
        }

        if self.path.contains("/-/") {
            self.path.split("/-/").next().filter(|p| !p.is_empty())
        } else {
            None
        }
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        match (&self.username, &self.meta_user) {
            (Some(u), _) => Some(u),
            (None, Some(u)) => Some(u),
            _ => None,
        }
    }

    #[inline]
    fn cpu_s(&self) -> Option<f64> {
        self.cpu_s
    }

    #[inline]
    fn db(&self) -> Option<f64> {
        self.db_duration_s
    }

    #[inline]
    fn gitaly(&self) -> Option<f64> {
        self.gitaly_duration_s
    }

    #[inline]
    fn rugged(&self) -> Option<f64> {
        self.rugged_duration_s
    }

    #[inline]
    fn redis(&self) -> Option<f64> {
        self.redis_duration_s
    }

    #[inline]
    fn queue(&self) -> Option<f64> {
        self.queue_duration_s
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for ProdJsonDurSEventError<'a> {
    prod_impl!(ProdJsonDurSEventError, Prod, ProdJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration_s
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration_s * 1000.0
    }

    #[inline]
    fn path(&self) -> Option<&str> {
        Some(self.path)
    }

    #[inline]
    fn project(&self) -> Option<&str> {
        if self.meta_project.is_some() {
            return self.meta_project;
        }

        if self.path.contains("/-/") {
            self.path.split("/-/").next().filter(|p| !p.is_empty())
        } else {
            None
        }
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        match (&self.username, &self.meta_user) {
            (Some(u), _) => Some(u),
            (None, Some(u)) => Some(u),
            _ => None,
        }
    }

    #[inline]
    fn error(&self) -> Option<Cow<'a, str>> {
        if let Some(err) = self.exception_class {
            return Some(Cow::from(format!(
                "{} {}",
                err,
                self.exception_message
                    .as_deref()
                    .unwrap_or_default()
                    .trim_end()
            )));
        }

        None
    }

    #[inline]
    fn backtrace(&self, len: usize, indent: usize) -> Option<String> {
        if let Some(bt) = self.exception_backtrace.as_ref() {
            if bt.is_empty() {
                return None;
            }

            let indent_str = " ".repeat(indent);
            let trace = bt.iter().take(len).fold(indent_str.clone(), |trace, line| {
                trace + line + "\n" + &indent_str
            });
            return Some(trace.trim_end().to_string());
        }

        None
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
}

impl<'a> Event<'a> for GitalyEvent<'a> {
    #[inline]
    fn action(&self) -> Cow<'a, str> {
        Cow::from(self.method)
    }

    #[inline]
    fn parse(s: &str) -> Option<GitalyEvent> {
        let time_str_end = s.find(' ')?;
        let time = s.get(..time_str_end)?;

        let code_start = s.find("grpc.code=")? + 10;
        let sub_str = s.get(code_start..)?;
        let code_end = sub_str.find(' ')?;
        let code = sub_str.get(..code_end)?;

        let meth_start = sub_str.find("grpc.method=")? + 12;
        let sub_str = sub_str.get(meth_start..)?;
        let meth_end = sub_str.find(' ')?;
        let method = sub_str.get(..meth_end)?;

        let dur_start_time = sub_str.find("grpc.time_ms=")? + 13;
        let sub_str = sub_str.get(dur_start_time..)?;
        let dur_end = sub_str.find(' ')?;
        let dur = sub_str.get(..dur_end)?;

        let status = if code == "OK" {
            Status::Success
        } else {
            Status::Fail
        };

        Some(GitalyEvent {
            method,
            time_ms: dur.parse::<f64>().unwrap_or_default(),
            status,
            time,
        })
    }

    fn parse_time(time: &str) -> Option<DateTime<FixedOffset>> {
        let naive_time = NaiveDateTime::parse_from_str(time, "%Y-%m-%d_%H:%M:%S%.f").ok()?;
        Some(
            FixedOffset::east(0)
                .from_local_datetime(&naive_time)
                .unwrap(),
        )
    }

    fn raw_time(&self) -> &str {
        self.time
    }

    fn stats_t() -> StatsType {
        StatsType::Gitaly
    }

    fn log_t() -> LogType {
        LogType::GitalyText
    }

    #[inline]
    fn status(&self) -> Status {
        self.status
    }

    #[inline]
    fn dur_s(&self) -> f64 {
        self.time_ms / 1000.0
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.time_ms
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(path, &str);
    none_impl!(project, &str);
    none_impl!(correlation_id, &str);
    none_impl!(user, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for GitalyJsonEvent<'a> {
    gitaly_impl!(GitalyJsonEvent, Gitaly, GitalyJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.time_ms / 1000.0
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.time_ms
    }

    #[inline]
    fn status(&self) -> Status {
        match self.code {
            "OK" => Status::Success,
            _ => Status::Fail,
        }
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(path, &str);
    none_impl!(project, &str);
    none_impl!(correlation_id, &str);
    none_impl!(user, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for GitalyJsonEventDetail<'a> {
    gitaly_impl!(GitalyJsonEventDetail, Gitaly, GitalyJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.time_ms / 1000.0
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.time_ms
    }

    #[inline]
    fn status(&self) -> Status {
        match self.code {
            "OK" => Status::Success,
            _ => Status::Fail,
        }
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    #[inline]
    fn project(&self) -> Option<&str> {
        self.project
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        self.username
    }

    #[inline]
    fn client_type(&self) -> Option<&str> {
        self.client_type
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    none_impl!(path, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(cpu_s, f64);
    none_impl!(queue, f64);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for GitalyJsonEventError<'a> {
    gitaly_impl!(GitalyJsonEventError, Gitaly, GitalyJson);

    #[inline]
    fn project(&self) -> Option<&str> {
        self.project
    }

    #[inline]
    fn status(&self) -> Status {
        match self.code {
            Some("OK") => Status::Success,
            _ => Status::Fail,
        }
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        self.username
    }

    #[inline]
    fn client_type(&self) -> Option<&str> {
        self.client_type
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    #[inline]
    fn dur_s(&self) -> f64 {
        let time = self.time_ms.unwrap_or_default();
        match time > 0.0 {
            true => time / 1000.0,
            false => 0.0,
        }
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.time_ms.unwrap_or_default()
    }

    #[inline]
    fn error(&self) -> Option<Cow<'a, str>> {
        match (&self.error, self.level == "error") {
            (Some(err), _) => Some(Cow::from(format!("{}: {}", err, self.msg.trim_end()))),
            (None, true) => Some(Cow::from(self.msg.trim_end().to_string())),
            (None, false) => None,
        }
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(path, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
}

impl<'a> Event<'a> for ShellJsonEvent<'a> {
    #[inline]
    fn parse(s: &str) -> Option<ShellJsonEvent> {
        serde_json::from_str::<ShellJsonEvent>(s).ok()
    }

    fn parse_time(time: &str) -> Option<DateTime<FixedOffset>> {
        DateTime::parse_from_rfc3339(time).ok()
    }

    fn stats_t() -> StatsType {
        StatsType::Shell
    }

    fn log_t() -> LogType {
        LogType::ShellJson
    }

    #[inline]
    fn action(&self) -> Cow<'a, str> {
        if let Some(url) = self.url {
            let path: String = url.split("api/v4").skip(1).collect();
            Cow::from(path.split("?key=").next().unwrap_or_default().to_owned())
        } else {
            Cow::from("n/a")
        }
    }

    fn raw_time(&self) -> &str {
        self.time
    }

    #[inline]
    fn project(&self) -> Option<&str> {
        self.project
    }

    #[inline]
    fn status(&self) -> Status {
        if let Some(status) = self.status {
            if status >= 500 {
                Status::Fail
            } else {
                Status::Success
            }
        } else {
            Status::Success
        }
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        self.username
    }

    #[inline]
    fn client_type(&self) -> Option<&str> {
        None
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        Some(self.correlation_id)
    }

    #[inline]
    fn dur_s(&self) -> f64 {
        let time = self.duration_ms.unwrap_or_default();
        match time > 0.0 {
            true => time / 1000.0,
            false => 0.0,
        }
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration_ms.unwrap_or_default()
    }

    #[inline]
    fn error(&self) -> Option<Cow<'a, str>> {
        None
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(path, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
}

impl<'a> Event<'a> for SidekiqEvent<'a> {
    #[inline]
    fn action(&self) -> Cow<'a, str> {
        Cow::from(self.class)
    }

    #[inline]
    fn parse(s: &str) -> Option<SidekiqEvent> {
        let time_str_end = s.find(' ')?;
        let time = s.get(..time_str_end)?;

        let sub_str_start = s.find("TID")?;
        let sub_str = s.get(sub_str_start..)?;
        let class_start = sub_str.find(' ')? + 1;
        let sub_str = sub_str.get(class_start..)?;
        let class_end = sub_str.find(' ')?;

        let class = sub_str.get(..class_end)?;

        if let Some(done_start) = sub_str.find("done:") {
            let sub_str = sub_str.get(done_start..)?;
            let dur_start = sub_str.find(' ')? + 1;
            let sub_str = sub_str.get(dur_start..)?;
            let dur_end = sub_str.find(' ')?;
            let dur = sub_str.get(..dur_end)?;

            Some(SidekiqEvent {
                class,
                duration: dur.parse::<f64>().unwrap_or_default(),
                status: Status::Success,
                time,
            })
        } else if let Some(fail_start) = sub_str.find("fail:") {
            let sub_str = sub_str.get(fail_start..)?;
            let dur_start = sub_str.find(' ')? + 1;
            let sub_str = sub_str.get(dur_start..)?;
            let dur_end = sub_str.find(' ')?;
            let dur = sub_str.get(..dur_end)?;

            Some(SidekiqEvent {
                class,
                duration: dur.parse::<f64>().unwrap_or_default(),
                status: Status::Fail,
                time,
            })
        } else {
            None
        }
    }

    fn parse_time(time: &str) -> Option<DateTime<FixedOffset>> {
        let naive_time = NaiveDateTime::parse_from_str(time, "%Y-%m-%d_%H:%M:%S%.f").ok()?;
        Some(
            FixedOffset::east(0)
                .from_local_datetime(&naive_time)
                .unwrap(),
        )
    }

    fn raw_time(&self) -> &str {
        self.time
    }

    fn stats_t() -> StatsType {
        StatsType::Sidekiq
    }

    fn log_t() -> LogType {
        LogType::SidekiqText
    }

    #[inline]
    fn status(&self) -> Status {
        self.status
    }
    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration * 1000.0
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(path, &str);
    none_impl!(project, &str);
    none_impl!(correlation_id, &str);
    none_impl!(user, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for SidekiqJsonEvent<'a> {
    sidekiq_impl!(SidekiqJsonEvent, Sidekiq, SidekiqJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration * 1000.0
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(path, &str);
    none_impl!(project, &str);
    none_impl!(correlation_id, &str);
    none_impl!(user, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for SidekiqJsonEventDetail<'a> {
    sidekiq_impl!(SidekiqJsonEventDetail, Sidekiq, SidekiqJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration * 1000.0
    }

    #[inline]
    fn project(&self) -> Option<&str> {
        self.meta_project
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        self.meta_user
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(path, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for SidekiqJsonEventError<'a> {
    base_impl!(SidekiqJsonEventError, Sidekiq, SidekiqJson);
    #[inline]
    fn dur_s(&self) -> f64 {
        0.0
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        0.0
    }

    #[inline]
    fn action(&self) -> Cow<'a, str> {
        Cow::from(self.class.unwrap_or("-"))
    }

    #[inline]
    fn status(&self) -> Status {
        match self.status {
            Some("done") => Status::Success,
            _ => Status::Fail,
        }
    }

    fn raw_time(&self) -> &str {
        self.time
    }

    #[inline]
    fn project(&self) -> Option<&str> {
        self.meta_project
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        self.meta_user
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    #[inline]
    fn error(&self) -> Option<Cow<'a, str>> {
        if let Some(err) = self.error_class {
            return Some(Cow::from(format!(
                "{} {}",
                err,
                self.error_message.unwrap_or_default()
            )));
        }

        if let Some(msg) = self.message {
            return Some(Cow::from(msg));
        }

        None
    }

    #[inline]
    fn backtrace(&self, len: usize, indent: usize) -> Option<String> {
        let indent_str = " ".repeat(indent);
        if let Some(bt) = self.error_backtrace.as_ref() {
            let trace = bt.iter().take(len).fold(indent_str.clone(), |trace, line| {
                trace + line + "\n" + &indent_str
            });
            return Some(trace.trim_end().to_string());
        }

        None
    }

    none_impl!(path, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
}

impl<'a> Event<'a> for SidekiqJsonDurSEvent<'a> {
    sidekiq_impl!(SidekiqJsonDurSEvent, Sidekiq, SidekiqJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration_s
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration_s * 1000.0
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(path, &str);
    none_impl!(project, &str);
    none_impl!(correlation_id, &str);
    none_impl!(user, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for SidekiqJsonDurSEventDetail<'a> {
    sidekiq_impl!(SidekiqJsonDurSEventDetail, Sidekiq, SidekiqJson);

    #[inline]
    fn dur_s(&self) -> f64 {
        self.duration_s
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        self.duration_s * 1000.0
    }

    #[inline]
    fn project(&self) -> Option<&str> {
        self.meta_project
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        self.meta_user
    }

    #[inline]
    fn db(&self) -> Option<f64> {
        self.db_duration_s
    }

    #[inline]
    fn gitaly(&self) -> Option<f64> {
        self.gitaly_duration_s
    }

    #[inline]
    fn rugged(&self) -> Option<f64> {
        self.rugged_duration_s
    }

    #[inline]
    fn redis(&self) -> Option<f64> {
        self.redis_duration_s
    }

    #[inline]
    fn queue(&self) -> Option<f64> {
        self.scheduling_latency_s
    }

    #[inline]
    fn cpu_s(&self) -> Option<f64> {
        self.cpu_s
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    #[inline]
    fn backtrace(&self, _len: usize, _indent: usize) -> Option<String> {
        None
    }

    none_impl!(path, &str);
    none_impl!(client_type, &str);
    none_impl!(error, Cow<'a, str>);
}

impl<'a> Event<'a> for SidekiqJsonDurSEventError<'a> {
    #[inline]
    fn dur_s(&self) -> f64 {
        0.0
    }

    #[inline]
    fn dur_ms(&self) -> f64 {
        0.0
    }

    #[inline]
    fn action(&self) -> Cow<'a, str> {
        Cow::from(self.class.unwrap_or("-"))
    }

    #[inline]
    fn status(&self) -> Status {
        match self.status {
            Some("done") => Status::Success,
            _ => Status::Fail,
        }
    }

    fn raw_time(&self) -> &str {
        self.time
    }

    #[inline]
    fn project(&self) -> Option<&str> {
        self.meta_project
    }

    #[inline]
    fn user(&self) -> Option<&str> {
        self.meta_user
    }

    #[inline]
    fn correlation_id(&self) -> Option<&str> {
        self.correlation_id
    }

    #[inline]
    fn error(&self) -> Option<Cow<'a, str>> {
        if let Some(err) = self.error_class {
            return Some(Cow::from(format!(
                "{} {}",
                err,
                self.error_message.as_deref().unwrap_or_default().trim_end()
            )));
        }

        if self.severity == "ERROR" {
            if let Some(msg) = self.message {
                return Some(Cow::from(msg));
            }
        }

        None
    }

    #[inline]
    fn backtrace(&self, len: usize, indent: usize) -> Option<String> {
        if let Some(bt) = self.error_backtrace.as_ref() {
            if bt.is_empty() {
                return None;
            }

            let indent_str = " ".repeat(indent);
            let trace = bt.iter().take(len).fold(indent_str.clone(), |trace, line| {
                trace + line + "\n" + &indent_str
            });
            return Some(trace.trim_end().to_string());
        }

        None
    }

    base_impl!(SidekiqJsonDurSEventError, Sidekiq, SidekiqJson);
    none_impl!(path, &str);
    none_impl!(db, f64);
    none_impl!(gitaly, f64);
    none_impl!(rugged, f64);
    none_impl!(redis, f64);
    none_impl!(queue, f64);
    none_impl!(cpu_s, f64);
    none_impl!(client_type, &str);
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Status {
    Success,
    Fail,
}

impl Status {
    pub fn is_fail(&self) -> bool {
        match self {
            Status::Fail => true,
            Status::Success => false,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn event_api_basic_parse() {
        let input = r##"{"time":"2019-04-10T04:47:01.360Z","severity":"INFO","duration":3.82,"db":1.22,"view":2.5999999999999996,"status":200,"method":"GET","path":"/api/v4/internal/authorized_keys","params":[{"key":"key","value":"[FILTERED]"},{"key":"secret_token","value":"[FILTERED]"}],"host":"127.0.0.1","ip":"127.0.0.1","ua":"Ruby","route":"/api/:version/internal/authorized_keys","gitaly_calls":0,"correlation_id":"8d5c2a7d-66f1-4e0d-917b-ae2a65954bf9"}"##;
        assert_eq!(
            Some(ApiJsonEvent {
                route: "/api/:version/internal/authorized_keys",
                status: 200,
                duration: 3.82,
                time: "2019-04-10T04:47:01.360Z",
            }),
            ApiJsonEvent::parse(input)
        );
    }

    #[test]
    fn event_api_detail_parse() {
        let input = r##"{"time":"2019-04-10T04:47:01.360Z","severity":"INFO","duration":3.82,"db":1.22,"view":2.5999999999999996,"status":200,"method":"GET","path":"/api/v4/internal/authorized_keys","params":[{"key":"key","value":"[FILTERED]"},{"key":"secret_token","value":"[FILTERED]"}],"host":"127.0.0.1","ip":"127.0.0.1","ua":"Ruby","route":"/api/:version/internal/authorized_keys","gitaly_calls":0,"correlation_id":"8d5c2a7d-66f1-4e0d-917b-ae2a65954bf9"}"##;
        assert_eq!(
            Some(ApiJsonEventDetail {
                route: "/api/:version/internal/authorized_keys",
                path: Some("/api/v4/internal/authorized_keys"),
                status: 200,
                duration: 3.82,
                time: "2019-04-10T04:47:01.360Z",
                username: None,
                db: Some(1.22),
                queue_duration: None,
                cpu_s: None,
                correlation_id: Some("8d5c2a7d-66f1-4e0d-917b-ae2a65954bf9"),
            }),
            ApiJsonEventDetail::parse(input)
        );
    }

    #[test]
    fn event_api_error_parse() {
        let input = r##"{"time":"2019-04-10T04:47:01.360Z","severity":"INFO","duration":3.82,"db":1.22,"view":2.5999999999999996,"status":500,"error":"GRPC::DeadlineExceeded","method":"GET","path":"/api/v4/internal/authorized_keys","params":[{"key":"key","value":"[FILTERED]"},{"key":"secret_token","value":"[FILTERED]"}],"host":"127.0.0.1","ip":"127.0.0.1","ua":"Ruby","route":"/api/:version/internal/authorized_keys","gitaly_calls":0,"correlation_id":"8d5c2a7d-66f1-4e0d-917b-ae2a65954bf9"}"##;
        assert_eq!(
            Some(ApiJsonEventError {
                route: "/api/:version/internal/authorized_keys",
                status: 500,
                duration: 3.82,
                time: "2019-04-10T04:47:01.360Z",
                username: None,
                error: Some("GRPC::DeadlineExceeded"),
                correlation_id: Some("8d5c2a7d-66f1-4e0d-917b-ae2a65954bf9"),
            }),
            ApiJsonEventError::parse(input)
        );
    }

    #[test]
    fn event_api_parse_time() {
        let input = r##"{"time":"2019-04-10T04:47:01.360Z","severity":"INFO","duration":3.82,"db":1.22,"view":2.5999999999999996,"status":200,"method":"GET","path":"/api/v4/internal/authorized_keys","params":[{"key":"key","value":"[FILTERED]"},{"key":"secret_token","value":"[FILTERED]"}],"host":"127.0.0.1","ip":"127.0.0.1","ua":"Ruby","route":"/api/:version/internal/authorized_keys","gitaly_calls":0,"correlation_id":"8d5c2a7d-66f1-4e0d-917b-ae2a65954bf9"}"##;
        assert_eq!(
            DateTime::parse_from_rfc3339("2019-04-10T04:47:01.360Z").ok(),
            ApiJsonEvent::parse(input).and_then(|e| ApiJsonEvent::parse_time(e.raw_time()))
        );
    }

    #[test]
    fn event_api_dur_s_parse() {
        let input = r##"{"time":"2020-04-23T22:14:20.290Z","severity":"INFO","duration_s":1.46,"db_duration_s":0.09,"view_duration_s":1.37,"status":200,"method":"GET","path":"/api/v4/projects","params":[{"key":"page","value":"2"},{"key":"per_page","value":"100"},{"key":"statistics","value":"true"}],"host":"gitlab.example.com","remote_ip":"10.10.10.10, 10.1.1.1","ua":"go-gitlab","route":"/api/:version/projects","user_id":2,"username":"myuser","queue_duration_s":0.01,"gitaly_calls":11,"gitaly_duration_s":0.13,"redis_calls":701,"redis_duration_s":0.06,"correlation_id":"u4G996jjXJ1"}"##;
        assert_eq!(
            Some(ApiJsonDurSEvent {
                route: "/api/:version/projects",
                status: 200,
                duration_s: 1.46,
                time: "2020-04-23T22:14:20.290Z",
            }),
            ApiJsonDurSEvent::parse(input)
        );
    }

    #[test]
    fn event_api_dur_s_detail_parse() {
        let input = r##"{"time":"2020-04-23T22:14:20.290Z","severity":"INFO","duration_s":1.46,"db_duration_s":0.09,"view_duration_s":1.37,"status":200,"method":"GET","path":"/api/v4/projects","params":[{"key":"page","value":"2"},{"key":"per_page","value":"100"},{"key":"statistics","value":"true"}],"host":"gitlab.example.com","remote_ip":"10.10.10.10, 10.1.1.1","ua":"go-gitlab","route":"/api/:version/projects","user_id":2,"username":"myuser","queue_duration_s":0.01,"gitaly_calls":11,"gitaly_duration_s":0.13,"redis_calls":701,"redis_duration_s":0.06,"correlation_id":"u4G996jjXJ1"}"##;
        assert_eq!(
            Some(ApiJsonDurSEventDetail {
                route: "/api/:version/projects",
                path: "/api/v4/projects",
                status: 200,
                duration_s: 1.46,
                time: "2020-04-23T22:14:20.290Z",
                username: Some("myuser"),
                meta_project: None,
                meta_user: None,
                db_duration_s: Some(0.09),
                gitaly_duration_s: Some(0.13),
                rugged_duration_s: None,
                redis_duration_s: Some(0.06),
                queue_duration_s: Some(0.01),
                cpu_s: None,
                correlation_id: Some("u4G996jjXJ1"),
            }),
            ApiJsonDurSEventDetail::parse(input)
        );
    }

    #[test]
    fn event_api_dur_s_error_parse() {
        let input = r##"{"time":"2020-04-24T06:48:21.016Z","severity":"INFO","duration_s":10.0,"db_duration_s":0.04,"view_duration_s":9.96,"status":500,"method":"GET","path":"/api/v4/projects","params":[{"key":"page","value":"1"},{"key":"per_page","value":"100"},{"key":"statistics","value":"true"}],"host":"muyhost","remote_ip":"1.1.1.1","ua":"go-gitlab","route":"/api/:version/projects","user_id":2,"username":"myuser","exception.class":"GRPC::DeadlineExceeded","exception.message":"4:Deadline Exceeded","exception.backtrace":["lib/gitlab/gitaly_client.rb:192:in `execute'","lib/gitlab/gitaly_client.rb 170:in `block in call'","lib/gitlab/gitaly_client.rb:198:in `measure_timings'","lib/gitlab/gitaly_client.rb:169:in `call'","lib/gitlab/gitaly_client/ref_service.rb:42:in `default_branch_name'","lib/gitlab/git/repository.rb:90:in `root_ref'","app/models/repository.rb:509:in `root_ref'","lib/gitlab/repository_cache_adapter.rb:84:in `block (2 levels) in cache_method_asymmetrically'","lib/gitlab/repository_cache.rb:44:in `fetch_without_caching_false'","lib/gitlab/repository_cache_adapter.rb:179:in `block (2 levels) in cache_method_output_asymmetrically'","lib/gitlab/safe_request_store.rb:12:in `fetch'","lib/gitlab/repository_cache.rb:25:in `fetch'","lib/gitlab/repository_cache_adapter.rb:178:in `block in cache_method_output_asymmetrically'","lib/gitlab/utils/strong_memoize.rb:30:in `strong_memoize'","lib/gitlab/repository_cache_adapter.rb:192:in `block in memoize_method_output'","lib/gitlab/repository_cache_adapter.rb:201:in `no_repository_fallback'","lib/gitlab/repository_cache_adapter.rb:191:in `memoize_method_output'","lib/gitlab/repository_cache_adapter.rb:177:in `cache_method_output_asymmetrically'","lib/gitlab/repository_cache_adapter.rb:83:in `block in cache_method_asymmetrically'","app/models/concerns/has_repository.rb:80:in `default_branch'","ee/lib/gitlab/ip_address_state.rb:10:in `with'","ee/lib/gitlab/jira/middleware.rb:19:in `call'"],"queue_duration_s":33.99,"gitaly_calls":1,"gitaly_duration_s":9.68,"redis_calls":216,"redis_duration_s":0.01,"correlation_id":"2bgfd0QY4s8"}"##;
        assert_eq!(
            Some(ApiJsonDurSEventError {
                route: "/api/:version/projects",
                status: 500,
                duration_s: 10.0,
                time: "2020-04-24T06:48:21.016Z",
                username: Some("myuser"),
                meta_project: None,
                meta_user: None,
                exception_class: Some("GRPC::DeadlineExceeded"),
                exception_message: Some(Cow::from("4:Deadline Exceeded")),
                exception_backtrace: Some(vec![
                                          "lib/gitlab/gitaly_client.rb:192:in `execute'",
                                          "lib/gitlab/gitaly_client.rb 170:in `block in call'",
                                          "lib/gitlab/gitaly_client.rb:198:in `measure_timings'",
                                          "lib/gitlab/gitaly_client.rb:169:in `call'",
                                          "lib/gitlab/gitaly_client/ref_service.rb:42:in `default_branch_name'",
                                          "lib/gitlab/git/repository.rb:90:in `root_ref'",
                                          "app/models/repository.rb:509:in `root_ref'",
                                          "lib/gitlab/repository_cache_adapter.rb:84:in `block (2 levels) in cache_method_asymmetrically'",
                                          "lib/gitlab/repository_cache.rb:44:in `fetch_without_caching_false'",
                                          "lib/gitlab/repository_cache_adapter.rb:179:in `block (2 levels) in cache_method_output_asymmetrically'",
                                          "lib/gitlab/safe_request_store.rb:12:in `fetch'",
                                          "lib/gitlab/repository_cache.rb:25:in `fetch'",
                                          "lib/gitlab/repository_cache_adapter.rb:178:in `block in cache_method_output_asymmetrically'",
                                          "lib/gitlab/utils/strong_memoize.rb:30:in `strong_memoize'",
                                          "lib/gitlab/repository_cache_adapter.rb:192:in `block in memoize_method_output'",
                                          "lib/gitlab/repository_cache_adapter.rb:201:in `no_repository_fallback'",
                                          "lib/gitlab/repository_cache_adapter.rb:191:in `memoize_method_output'",
                                          "lib/gitlab/repository_cache_adapter.rb:177:in `cache_method_output_asymmetrically'",
                                          "lib/gitlab/repository_cache_adapter.rb:83:in `block in cache_method_asymmetrically'",
                                          "app/models/concerns/has_repository.rb:80:in `default_branch'",
                                          "ee/lib/gitlab/ip_address_state.rb:10:in `with'",
                                          "ee/lib/gitlab/jira/middleware.rb:19:in `call'"
                ]),
                correlation_id: Some("2bgfd0QY4s8"),
            }),
            ApiJsonDurSEventError::parse(input)
        );
    }

    #[test]
    fn event_api_dur_s_parse_time() {
        let input = r##"{"time":"2020-04-23T22:14:21.769Z","severity":"INFO","duration_s":1.46,"db_duration_s":0.05,"view_duration_s":1.41,"status":200,"method":"GET","path":"/api/v4/projects","params":[{"key":"page","value":"3"},{"key":"per_page","value":"100"},{"key":"statistics","value":"true"}],"host":"gitlab.example.com","remote_ip":"10.10.10.10, 10.1.1.1","ua":"go-gitlab","route":"/api/:version/projects","user_id":2,"username":"myuser","queue_duration_s":0.01,"gitaly_calls":12,"gitaly_duration_s":0.15,"redis_calls":677,"redis_duration_s":0.06,"correlation_id":"SXIj2D1n5A"}"##;
        assert_eq!(
            DateTime::parse_from_rfc3339("2020-04-23T22:14:21.769Z").ok(),
            ApiJsonDurSEvent::parse(input).and_then(|e| ApiJsonDurSEvent::parse_time(e.raw_time()))
        );
    }

    #[test]
    fn event_prod_parse() {
        let input = r##"{"method":"GET","path":"/ns/my_proj.git/info/refs","format":"html","controller":"Projects::GitHttpController","action":"info_refs","status":200,"duration":147.69,"view":0.34,"db":14.5,"time":"2019-04-19T08:10:14.269Z","params":[{"key":"service","value":"git-upload-pack"},{"key":"namespace_id","value":"myns"},{"key":"project_id","value":"my_proj.git"}],"remote_ip":"10.10.10.10","user_id":226,"username":"myuser","ua":"JGit/3.5.3.201412180710-r","queue_duration":null,"correlation_id":"063a9dd3-c4ee-4dbc-a912-948f9b6da880"}"##;
        assert_eq!(
            Some(ProdJsonEvent {
                controller: "Projects::GitHttpController",
                action: "info_refs",
                status: 200,
                duration: 147.69,
                time: "2019-04-19T08:10:14.269Z",
            }),
            ProdJsonEvent::parse(input)
        );
    }

    #[test]
    fn event_prod_detail_parse() {
        let input = r##"{"method":"GET","path":"/ns/my_proj.git/info/refs","format":"html","controller":"Projects::GitHttpController","action":"info_refs","status":200,"duration":147.69,"view":0.34,"db":14.5,"time":"2019-04-19T08:10:14.269Z","params":[{"key":"service","value":"git-upload-pack"},{"key":"namespace_id","value":"myns"},{"key":"project_id","value":"my_proj.git"}],"remote_ip":"10.10.10.10","user_id":226,"username":"myuser","ua":"JGit/3.5.3.201412180710-r","queue_duration":null,"correlation_id":"063a9dd3-c4ee-4dbc-a912-948f9b6da880"}"##;
        assert_eq!(
            Some(ProdJsonEventDetail {
                controller: "Projects::GitHttpController",
                action: "info_refs",
                path: "/ns/my_proj.git/info/refs",
                status: 200,
                duration: 147.69,
                cpu_s: None,
                db: Some(14.5),
                queue_duration: None,
                username: Some("myuser"),
                time: "2019-04-19T08:10:14.269Z",
                correlation_id: Some("063a9dd3-c4ee-4dbc-a912-948f9b6da880"),
            }),
            ProdJsonEventDetail::parse(input)
        );
    }

    #[test]
    fn event_prod_error_parse() {
        let input = r##"{"method":"PATCH","path":"/api/v4/jobs/7601546/trace","format":"html","controller":"Gitlab::RequestForgeryProtection::Controller","action":"index","status":422,"error":"ActionController::InvalidAuthenticityToken: ActionController::InvalidAuthenticityToken","duration":3.08,"view":0.0,"db":0.0,"time":"2019-04-19T08:13:15.082Z","params":[],"remote_ip":null,"user_id":null,"username":null,"ua":null,"queue_duration":null,"correlation_id":null}"##;
        assert_eq!(
            Some(ProdJsonEventError {
                controller: "Gitlab::RequestForgeryProtection::Controller",
                action: "index",
                status: 422,
                duration: 3.08,
                username: None,
                time: "2019-04-19T08:13:15.082Z",
                error: Some("ActionController::InvalidAuthenticityToken: ActionController::InvalidAuthenticityToken"),
                correlation_id: None,
            }),
            ProdJsonEventError::parse(input)
        );
    }

    #[test]
    fn event_prod_parse_time() {
        let input = r##"{"method":"GET","path":"/ns/my_proj.git/info/refs","format":"html","controller":"Projects::GitHttpController","action":"info_refs","status":200,"duration":147.69,"view":0.34,"db":14.5,"time":"2019-04-19T08:10:14.269Z","params":[{"key":"service","value":"git-upload-pack"},{"key":"namespace_id","value":"myns"},{"key":"project_id","value":"my_proj.git"}],"remote_ip":"10.10.10.10","user_id":226,"username":"myuser","ua":"JGit/3.5.3.201412180710-r","queue_duration":null,"correlation_id":"063a9dd3-c4ee-4dbc-a912-948f9b6da880"}"##;
        assert_eq!(
            DateTime::parse_from_rfc3339("2019-04-19T08:10:14.269Z").ok(),
            ProdJsonEvent::parse(input).and_then(|e| ProdJsonEvent::parse_time(e.raw_time()))
        );
    }

    #[test]
    fn event_prod_dur_s_parse() {
        let input = r##"{"method":"GET","path":"/group/project/noteable/issue/501/notes","format":"json","controller":"Projects::NotesController","action":"index","status":200,"time":"2020-04-24T04:54:43.214Z","params":[{"key":"namespace_id","value":"group"},{"key":"project_id","value":"project"},{"key":"target_type","value":"issue"},{"key":"target_id","value":"501"}],"remote_ip":"10.10.10.10","user_id":null,"username":null,"ua":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36","queue_duration_s":0.01,"redis_calls":1,"redis_duration_s":0.0,"correlation_id":"Mm76Q3UsS31","cpu_s":0.04,"db_duration_s":0.0,"view_duration_s":0.0,"duration_s":0.02}"##;
        assert_eq!(
            Some(ProdJsonDurSEvent {
                controller: "Projects::NotesController",
                action: "index",
                status: 200,
                duration_s: 0.02,
                time: "2020-04-24T04:54:43.214Z",
            }),
            ProdJsonDurSEvent::parse(input)
        );
    }

    #[test]
    fn event_prod_dur_s_detail_parse() {
        let input = r##"{"method":"GET","path":"/group/project/noteable/issue/501/notes","format":"json","controller":"Projects::NotesController","action":"index","status":200,"time":"2020-04-24T04:54:43.214Z","params":[{"key":"namespace_id","value":"group"},{"key":"project_id","value":"project"},{"key":"target_type","value":"issue"},{"key":"target_id","value":"501"}],"remote_ip":"10.10.10.10","user_id":null,"username":null,"ua":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36","queue_duration_s":0.01,"redis_calls":1,"redis_duration_s":0.0,"correlation_id":"Mm76Q3UsS31","cpu_s":0.04,"db_duration_s":0.0,"view_duration_s":0.0,"duration_s":0.02}"##;
        assert_eq!(
            Some(ProdJsonDurSEventDetail {
                controller: "Projects::NotesController",
                action: "index",
                path: "/group/project/noteable/issue/501/notes",
                status: 200,
                duration_s: 0.02,
                username: None,
                meta_user: None,
                meta_project: None,
                cpu_s: Some(0.04),
                db_duration_s: Some(0.0),
                gitaly_duration_s: None,
                rugged_duration_s: None,
                redis_duration_s: Some(0.0),
                queue_duration_s: Some(0.01),
                time: "2020-04-24T04:54:43.214Z",
                correlation_id: Some("Mm76Q3UsS31"),
            }),
            ProdJsonDurSEventDetail::parse(input)
        );
    }

    #[test]
    fn event_prod_dur_s_error_parse() {
        let input = r##"{"method":"GET","path":"/group/project/-/merge_requests/481/ci_environments_status","format":"json","controller":"Projects::MergeRequestsController","action":"ci_environments_status","status":500,"time":"2020-12-11T01:35:29.744Z","params":[{"key":"namespace_id","value":"group"},{"key":"project_id","value":"project"},{"key":"id","value":"481"}],"remote_ip":"1.1.1.1","user_id":997,"username":"myuser","ua":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36","correlation_id":"bZe7787hsU7","meta.user":"myuser","meta.project":"group/project","meta.root_namespace":"group","meta.caller_id":"Projects::MergeRequestsController#ci_environments_status","meta.feature_category":"code_review","redis_calls":2,"redis_duration_s":0.000423,"redis_read_bytes":486,"redis_write_bytes":937,"redis_cache_calls":1,"redis_cache_duration_s":0.000205,"redis_cache_read_bytes":202,"redis_cache_write_bytes":55,"redis_shared_state_calls":1,"redis_shared_state_duration_s":0.000218,"redis_shared_state_read_bytes":284,"redis_shared_state_write_bytes":882,"cpu_s":0.05,"exception.class":"Rack::Timeout::RequestTimeoutException","exception.message":"Request ran for longer than 60000ms","exception.backtrace":["app/models/environment_status.rb:103:in `map'","app/models/environment_status.rb:103:in `build_environments_status'","app/models/environment_status.rb:14:in `for_merge_request'","app/controllers/projects/merge_requests_controller.rb:302:in `ci_environments_status'","ee/lib/gitlab/ip_address_state.rb:10:in `with'","ee/app/controllers/ee/application_controller.rb:44:in `set_current_ip_address'","app/controllers/application_controller.rb:499:in `set_current_admin'","lib/gitlab/session.rb:11:in `with_session'","app/controllers/application_controller.rb:490:in `set_session_storage'","lib/gitlab/i18n.rb:73:in `with_locale'","lib/gitlab/i18n.rb:79:in `with_user_locale'","app/controllers/application_controller.rb:484:in `set_locale'","lib/gitlab/error_tracking.rb:52:in `with_context'","app/controllers/application_controller.rb:549:in `sentry_context'","app/controllers/application_controller.rb:477:in `block in set_current_context'","lib/gitlab/application_context.rb:54:in `block in use'","lib/gitlab/application_context.rb:54:in `use'","lib/gitlab/application_context.rb:21:in `with_context'","app/controllers/application_controller.rb:469:in `set_current_context'","lib/gitlab/request_profiler/middleware.rb:17:in `call'","lib/gitlab/jira/middleware.rb:19:in `call'","lib/gitlab/middleware/go.rb:20:in `call'","lib/gitlab/etag_caching/middleware.rb:21:in `call'","lib/gitlab/middleware/multipart.rb:234:in `call'","lib/gitlab/middleware/read_only/controller.rb:50:in `call'","lib/gitlab/middleware/read_only.rb:18:in `call'","lib/gitlab/middleware/same_site_cookies.rb:27:in `call'","lib/gitlab/middleware/handle_malformed_strings.rb:21:in `call'","lib/gitlab/middleware/basic_health_check.rb:25:in `call'","lib/gitlab/middleware/handle_ip_spoof_attack_error.rb:25:in `call'","lib/gitlab/middleware/request_context.rb:23:in `call'","config/initializers/fix_local_cache_middleware.rb:9:in `call'","lib/gitlab/metrics/requests_rack_middleware.rb:76:in `call'","lib/gitlab/middleware/release_env.rb:12:in `call'"],"db_duration_s":59.9479,"view_duration_s":0.0,"duration_s":59.97583,"db_count":0,"db_write_count":0,"db_cached_count":0}"##;
        assert_eq!(
            Some(ProdJsonDurSEventError {
                controller: "Projects::MergeRequestsController",
                action: "ci_environments_status",
                path: "/group/project/-/merge_requests/481/ci_environments_status",
                status: 500,
                duration_s: 59.97583,
                username: Some("myuser"),
                meta_user: Some("myuser"),
                meta_project: Some("group/project"),
                time: "2020-12-11T01:35:29.744Z",
                exception_class: Some("Rack::Timeout::RequestTimeoutException"),
                exception_message: Some(Cow::from("Request ran for longer than 60000ms")),
                exception_backtrace: Some(vec![
                    "app/models/environment_status.rb:103:in `map'",
                    "app/models/environment_status.rb:103:in `build_environments_status'",
                    "app/models/environment_status.rb:14:in `for_merge_request'",
                    "app/controllers/projects/merge_requests_controller.rb:302:in `ci_environments_status'",
                    "ee/lib/gitlab/ip_address_state.rb:10:in `with'",
                    "ee/app/controllers/ee/application_controller.rb:44:in `set_current_ip_address'",
                    "app/controllers/application_controller.rb:499:in `set_current_admin'",
                    "lib/gitlab/session.rb:11:in `with_session'",
                    "app/controllers/application_controller.rb:490:in `set_session_storage'",
                    "lib/gitlab/i18n.rb:73:in `with_locale'",
                    "lib/gitlab/i18n.rb:79:in `with_user_locale'",
                    "app/controllers/application_controller.rb:484:in `set_locale'",
                    "lib/gitlab/error_tracking.rb:52:in `with_context'",
                    "app/controllers/application_controller.rb:549:in `sentry_context'",
                    "app/controllers/application_controller.rb:477:in `block in set_current_context'",
                    "lib/gitlab/application_context.rb:54:in `block in use'",
                    "lib/gitlab/application_context.rb:54:in `use'",
                    "lib/gitlab/application_context.rb:21:in `with_context'",
                    "app/controllers/application_controller.rb:469:in `set_current_context'",
                    "lib/gitlab/request_profiler/middleware.rb:17:in `call'",
                    "lib/gitlab/jira/middleware.rb:19:in `call'",
                    "lib/gitlab/middleware/go.rb:20:in `call'",
                    "lib/gitlab/etag_caching/middleware.rb:21:in `call'",
                    "lib/gitlab/middleware/multipart.rb:234:in `call'",
                    "lib/gitlab/middleware/read_only/controller.rb:50:in `call'",
                    "lib/gitlab/middleware/read_only.rb:18:in `call'",
                    "lib/gitlab/middleware/same_site_cookies.rb:27:in `call'",
                    "lib/gitlab/middleware/handle_malformed_strings.rb:21:in `call'",
                    "lib/gitlab/middleware/basic_health_check.rb:25:in `call'",
                    "lib/gitlab/middleware/handle_ip_spoof_attack_error.rb:25:in `call'",
                    "lib/gitlab/middleware/request_context.rb:23:in `call'",
                    "config/initializers/fix_local_cache_middleware.rb:9:in `call'",
                    "lib/gitlab/metrics/requests_rack_middleware.rb:76:in `call'",
                    "lib/gitlab/middleware/release_env.rb:12:in `call'"
                ]),
                correlation_id: Some("bZe7787hsU7"),
            }),
            ProdJsonDurSEventError::parse(input)
        );
    }

    #[test]
    fn event_prod_dur_s_error_meta_project() {
        let input = r##"{"method":"GET","path":"/group/project/noteable/merge_request/63786/notes","format":"json","controller":"Projects::NotesController","action":"index","status":500,"time":"2021-05-17T17:52:04.222Z","params":[{"key":"namespace_id","value":"group"},{"key":"project_id","value":"project"},{"key":"target_type","value":"merge_request"},{"key":"target_id","value":"63786"}],"remote_ip":"10.10.10.10","user_id":1160,"username":"myuser","ua":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36","correlation_id":"01F5XQ7T05RMQAXZ7HCGT61R0C","meta.user":"myuser","meta.project":"group/project","meta.root_namespace":"group","meta.caller_id":"Projects::NotesController#index","meta.remote_ip":"10.10.10.10","meta.feature_category":"issue_tracking","gitaly_calls":4,"gitaly_duration_s":59.902592,"redis_calls":3,"redis_duration_s":0.002521,"redis_read_bytes":324,"redis_write_bytes":824,"redis_cache_calls":1,"redis_cache_duration_s":0.000735,"redis_cache_read_bytes":109,"redis_cache_write_bytes":51,"redis_shared_state_calls":2,"redis_shared_state_duration_s":0.001786,"redis_shared_state_read_bytes":215,"redis_shared_state_write_bytes":773,"db_count":45,"db_write_count":0,"db_cached_count":8,"cpu_s":0.194122,"queue_duration_s":0.005233,"exception.class":"Rack::Timeout::RequestTimeoutException","exception.message":"Request ran for longer than 60000ms","exception.backtrace":[],"db_duration_s":0.0341,"view_duration_s":0.0,"duration_s":59.99565}"##;
        assert!(ProdJsonDurSEventError::parse(input)
            .filter(|e| e.project().is_some())
            .is_some());
    }

    #[test]
    fn event_prod_dur_s_parse_time() {
        let input = r##"{"method":"GET","path":"/group/project/noteable/issue/501/notes","format":"json","controller":"Projects::NotesController","action":"index","status":200,"time":"2020-04-24T04:54:43.214Z","params":[{"key":"namespace_id","value":"group"},{"key":"project_id","value":"project"},{"key":"target_type","value":"issue"},{"key":"target_id","value":"501"}],"remote_ip":"10.10.10.10","user_id":null,"username":null,"ua":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36","queue_duration_s":0.01,"redis_calls":1,"redis_duration_s":0.0,"correlation_id":"Mm76Q3UsS31","cpu_s":0.04,"db_duration_s":0.0,"view_duration_s":0.0,"duration_s":0.02}"##;
        assert_eq!(
            DateTime::parse_from_rfc3339("2020-04-24T04:54:43.214Z").ok(),
            ProdJsonDurSEvent::parse(input)
                .and_then(|e| ProdJsonDurSEvent::parse_time(e.raw_time()))
        );
    }

    #[test]
    fn event_gitaly_parse() {
        let input = r##"2019-02-01_11:30:20.53810 time="2019-02-01T11:30:20Z" level=info msg="finished streaming call with code OK" grpc.code=OK grpc.meta.auth_version=v2 grpc.meta.client_name=gitlab-web grpc.method=CommitDiff grpc.request.deadline="2019-02-01T11:31:20Z" grpc.request.fullMethod=/gitaly.DiffService/CommitDiff grpc.request.glRepository=project-2070 grpc.request.repoPath=core-build/goldilocks.git grpc.request.repoStorage=default grpc.request.topLevelGroup=core-build grpc.service=gitaly.DiffService grpc.start_time="2019-02-01T11:30:20Z" grpc.time_ms=96.385 peer.address=@ span.kind=server system=grpc"##;
        assert_eq!(
            Some(GitalyEvent {
                method: "CommitDiff",
                time_ms: 96.385,
                status: Status::Success,
                time: "2019-02-01_11:30:20.53810",
            }),
            GitalyEvent::parse(input)
        );
    }

    #[test]
    fn event_gitaly_parse_time() {
        let input = r##"2019-02-01_11:30:20.53810 time="2019-02-01T11:30:20Z" level=info msg="finished streaming call with code OK" grpc.code=OK grpc.meta.auth_version=v2 grpc.meta.client_name=gitlab-web grpc.method=CommitDiff grpc.request.deadline="2019-02-01T11:31:20Z" grpc.request.fullMethod=/gitaly.DiffService/CommitDiff grpc.request.glRepository=project-2070 grpc.request.repoPath=core-build/goldilocks.git grpc.request.repoStorage=default grpc.request.topLevelGroup=core-build grpc.service=gitaly.DiffService grpc.start_time="2019-02-01T11:30:20Z" grpc.time_ms=96.385 peer.address=@ span.kind=server system=grpc"##;
        assert_eq!(
            DateTime::parse_from_rfc3339("2019-02-01T11:30:20.53810Z").ok(),
            GitalyEvent::parse(input).and_then(|e| GitalyEvent::parse_time(e.raw_time()))
        );
    }

    #[test]
    fn event_gitaly_json_parse() {
        let input = r##"{"correlation_id":"qNWQPts8Zs","grpc.code":"OK","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.meta.deadline_type":"regular","grpc.method":"GetBlobs","grpc.request.deadline":"2020-11-18T17:22:37Z","grpc.request.fullMethod":"/gitaly.BlobService/GetBlobs","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-39207","grpc.request.repoPath":"@hashed/6a/f7/6af704b5fae984705e2046c7ed9972a0c10e681ccfaf8f521a50c45c1df1ca7a.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.BlobService","grpc.start_time":"2020-11-18T17:22:27Z","grpc.time_ms":42.833,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:22:28.021Z"}"##;
        assert_eq!(
            Some(GitalyJsonEvent {
                method: "GetBlobs",
                time_ms: 42.833,
                code: "OK",
                time: "2020-11-18T17:22:28.021Z",
            }),
            GitalyJsonEvent::parse(input)
        );
    }

    #[test]
    fn event_gitaly_json_detail_parse() {
        let input = r##"{"correlation_id":"qNWQPts8Zs","grpc.code":"OK","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.meta.deadline_type":"regular","grpc.method":"GetBlobs","grpc.request.deadline":"2020-11-18T17:22:37Z","grpc.request.fullMethod":"/gitaly.BlobService/GetBlobs","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-39207","grpc.request.repoPath":"@hashed/6a/f7/6af704b5fae984705e2046c7ed9972a0c10e681ccfaf8f521a50c45c1df1ca7a.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.BlobService","grpc.start_time":"2020-11-18T17:22:27Z","grpc.time_ms":42.833,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:22:28.021Z"}"##;
        assert_eq!(
            Some(GitalyJsonEventDetail {
                method: "GetBlobs",
                project: Some("group/project"),
                client_type: Some("gitlab-web"),
                username: None,
                time_ms: 42.833,
                code: "OK",
                time: "2020-11-18T17:22:28.021Z",
                correlation_id: Some("qNWQPts8Zs"),
            }),
            GitalyJsonEventDetail::parse(input)
        );
    }

    #[test]
    fn event_gitaly_json_detail_user_parse() {
        let input = r##"{"correlation_id":"01FC8KWG6MHTJQDX1ATHMRC04P","grpc.code":"OK","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.meta.deadline_type":"regular","grpc.method":"FindCommit","grpc.request.deadline":"2021-08-04T12:58:47.048Z","grpc.request.fullMethod":"/gitaly.CommitService/FindCommit","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-78428","grpc.request.repoPath":"@hashed/d8/28/d82881233e464348e438f96a945f33cf872372525521b298ae818734fc5b0d95.git","grpc.request.repoStorage":"gitaly-3","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.CommitService","grpc.start_time":"2021-08-04T12:58:17.953Z","grpc.time_ms":10.833,"level":"info","msg":"finished unary call with code OK","peer.address":"10.0.0.1:44904","pid":1571951,"remote_ip":"10.1.0.10","span.kind":"server","system":"grpc","time":"2021-08-04T12:58:17.964Z","username":"myuser"}"##;
        assert_eq!(
            Some(GitalyJsonEventDetail {
                method: "FindCommit",
                project: Some("group/project"),
                client_type: Some("gitlab-web"),
                username: Some("myuser"),
                time_ms: 10.833,
                code: "OK",
                time: "2021-08-04T12:58:17.964Z",
                correlation_id: Some("01FC8KWG6MHTJQDX1ATHMRC04P"),
            }),
            GitalyJsonEventDetail::parse(input)
        );
    }

    #[test]
    fn event_gitaly_json_error_parse() {
        let input = r##"{"correlation_id":"a3751f09-85b6-4e30-b6be-d4dbdd12f8ef","error":"rpc error: code = Internal desc = Cleanup: cleanStaleWorktrees: exit status 128","grpc.code":"Internal","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.meta.deadline_type":"regular","grpc.method":"Cleanup","grpc.request.deadline":"2020-11-18T17:42:21Z","grpc.request.fullMethod":"/gitaly.RepositoryService/Cleanup","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-14928","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.RepositoryService","grpc.start_time":"2020-11-18T17:42:11Z","grpc.time_ms":31.132,"level":"error","msg":"finished unary call with code Internal","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:42:11.378Z"}"##;
        assert_eq!(
            Some(GitalyJsonEventError {
                method: "Cleanup",
                project: Some("group/project"),
                client_type: Some("gitlab-web"),
                username: None,
                time_ms: Some(31.132),
                code: Some("Internal"),
                time: "2020-11-18T17:42:11.378Z",
                level: "error",
                msg: Cow::from("finished unary call with code Internal"),
                error: Some(Cow::from("rpc error: code = Internal desc = Cleanup: cleanStaleWorktrees: exit status 128")),
                correlation_id: Some("a3751f09-85b6-4e30-b6be-d4dbdd12f8ef"),
            }),
            GitalyJsonEventError::parse(input)
        );
    }

    #[test]
    fn event_gitaly_json_error_no_err_field_parse() {
        // No 'error' field on this event
        let input = r##"{"correlation_id":"0c40c04f-e0ee-464b-a3f8-dd2bd2512dd3","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-sidekiq","grpc.meta.deadline_type":"unknown","grpc.method":"CommitsBetween","grpc.request.deadline":"2020-11-18T17:47:19Z","grpc.request.fullMethod":"/gitaly.CommitService/CommitsBetween","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-20504","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.CommitService","grpc.start_time":"2020-11-18T17:46:49Z","level":"error","msg":"fatal: Invalid revision range master..0000000000000000000000000000000000000000\\n","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:46:49.835Z"}"##;
        assert_eq!(
            Some(GitalyJsonEventError {
                method: "CommitsBetween",
                project: Some("group/project"),
                client_type: Some("gitlab-sidekiq"),
                username: None,
                time_ms: None,
                code: None,
                time: "2020-11-18T17:46:49.835Z",
                level: "error",
                msg: Cow::from("fatal: Invalid revision range master..0000000000000000000000000000000000000000\\n"),
                error: None,
                correlation_id: Some("0c40c04f-e0ee-464b-a3f8-dd2bd2512dd3"),
            }),
            GitalyJsonEventError::parse(input)
        );
    }

    #[test]
    fn event_gitaly_json_error_complicated_error_string_parse() {
        let input = r##"{"correlation_id":"01F3RMH5150E54B7G423GFKYQ3","error":"GitLab: Commit message does not follow the pattern '\\A(Merge branch ')?((master|release-\\d{4}\\.\\d{2}\\.\\d{2}(-db-migration)?|[a-z]{3}-[A-Z]+-\\d+-[A-Za-z0-9\\-]+)(' into '|')?){1,2}'","grpc.meta.deadline_type":"none","grpc.method":"PreReceiveHook","grpc.request.fullMethod":"/gitaly.HookService/PreReceiveHook","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-138","grpc.request.repoPath":"@hashed/d6/a4/d6a4031733610bb080d0bfa794fcc9dbdcff74834aeaab7c6b927e21e9754037.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.HookService","grpc.start_time":"2021-04-20T21:56:09.900Z","level":"warning","msg":"stopping transaction because pre-receive hook failed","peer.address":"@","pid":32728,"span.kind":"server","system":"grpc","time":"2021-04-20T21:56:11.287Z"}"##;
        assert_eq!(
            Some(GitalyJsonEventError {
                method: "PreReceiveHook",
                project: Some("group/project"),
                client_type: None,
                username: None,
                time_ms: None,
                code: None,
                time: "2021-04-20T21:56:11.287Z",
                level: "warning",
                msg: Cow::from("stopping transaction because pre-receive hook failed"),
                error: Some(Cow::from("GitLab: Commit message does not follow the pattern '\\A(Merge branch ')?((master|release-\\d{4}\\.\\d{2}\\.\\d{2}(-db-migration)?|[a-z]{3}-[A-Z]+-\\d+-[A-Za-z0-9\\-]+)(' into '|')?){1,2}'")),
                correlation_id: Some("01F3RMH5150E54B7G423GFKYQ3"),
            }),
            GitalyJsonEventError::parse(input)
        );
    }

    #[test]
    fn event_gitaly_json_parse_time() {
        let input = r##"{"correlation_id":"qNWQPts8Zs","grpc.code":"OK","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.meta.deadline_type":"regular","grpc.method":"GetBlobs","grpc.request.deadline":"2020-11-18T17:22:37Z","grpc.request.fullMethod":"/gitaly.BlobService/GetBlobs","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-39207","grpc.request.repoPath":"@hashed/6a/f7/6af704b5fae984705e2046c7ed9972a0c10e681ccfaf8f521a50c45c1df1ca7a.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.BlobService","grpc.start_time":"2020-11-18T17:22:27Z","grpc.time_ms":42.833,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:22:28.021Z"}"##;
        assert_eq!(
            DateTime::parse_from_rfc3339("2020-11-18T17:22:28.021Z").ok(),
            GitalyJsonEvent::parse(input).and_then(|e| GitalyJsonEvent::parse_time(e.raw_time()))
        );
    }

    #[test]
    fn event_sidekiq_parse() {
        let input = r##"2019-03-17_03:14:28.22715 2019-03-17T03:14:28.226Z 129639 TID-oteaiznk3 ArchiveTraceWorker JID-e429392ba3bccf70f972d8c8 INFO: done: 60.454 sec"##;
        assert_eq!(
            Some(SidekiqEvent {
                class: "ArchiveTraceWorker",
                duration: 60.454,
                status: Status::Success,
                time: "2019-03-17_03:14:28.22715",
            }),
            SidekiqEvent::parse(input)
        );
    }

    #[test]
    fn event_sidekiq_time_parse() {
        let input = r##"2019-03-17_03:14:28.22715 2019-03-17T03:14:28.226Z 129639 TID-oteaiznk3 ArchiveTraceWorker JID-e429392ba3bccf70f972d8c8 INFO: done: 60.454 sec"##;
        assert_eq!(
            DateTime::parse_from_rfc3339("2019-03-17T03:14:28.22715Z").ok(),
            SidekiqEvent::parse(input).and_then(|e| SidekiqEvent::parse_time(e.raw_time()))
        );
    }

    #[test]
    fn event_sidekiq_json_parse() {
        let input = r##"{"severity":"INFO","time":"2020-04-23T05:19:04.593Z","queue":"cronjob:pipeline_schedule","class":"PipelineScheduleWorker","retry":false,"queue_namespace":"cronjob","jid":"75666b794a52687726000513","created_at":"2020-04-23T05:19:04.544Z","correlation_id":"fa48f5803ad824eabb004aa1774537d6","enqueued_at":"2020-04-23T05:19:04.547Z","pid":29039,"message":"PipelineScheduleWorker JID-75666b794a52687726000513: done: 0.04574 sec","job_status":"done","scheduling_latency_s":0.001009,"duration":0.04574,"cpu_s":0.00551,"completed_at":"2020-04-23T05:19:04.593Z","db_duration":1.021306961774826,"db_duration_s":0.001021306961774826}"##;
        assert_eq!(
            Some(SidekiqJsonEvent {
                class: "PipelineScheduleWorker",
                duration: 0.04574,
                status: "done",
                time: "2020-04-23T05:19:04.593Z",
            }),
            SidekiqJsonEvent::parse(input)
        );
    }

    #[test]
    fn event_sidekiq_json_detail_parse() {
        let input = r##"{"severity":"INFO","time":"2020-04-23T05:19:04.593Z","queue":"cronjob:pipeline_schedule","class":"PipelineScheduleWorker","retry":false,"queue_namespace":"cronjob","jid":"75666b794a52687726000513","created_at":"2020-04-23T05:19:04.544Z","correlation_id":"fa48f5803ad824eabb004aa1774537d6","enqueued_at":"2020-04-23T05:19:04.547Z","pid":29039,"message":"PipelineScheduleWorker JID-75666b794a52687726000513: done: 0.04574 sec","job_status":"done","scheduling_latency_s":0.001009,"duration":0.04574,"cpu_s":0.00551,"completed_at":"2020-04-23T05:19:04.593Z","db_duration":1.021306961774826,"db_duration_s":0.001021306961774826}"##;
        assert_eq!(
            Some(SidekiqJsonEventDetail {
                class: "PipelineScheduleWorker",
                duration: 0.04574,
                status: "done",
                meta_project: None,
                meta_user: None,
                time: "2020-04-23T05:19:04.593Z",
                correlation_id: Some("fa48f5803ad824eabb004aa1774537d6"),
            }),
            SidekiqJsonEventDetail::parse(input)
        );
    }

    #[test]
    fn event_sidekiq_json_error_parse() {
        let input = r##"{"severity":"WARN","time":"2020-04-24T07:18:48.013Z","error_class":"RepositoryUpdateMirrorWorker::UpdateError","error_message":"4:Deadline Exceeded","context":"Job raised exception","jobstr":"{\"class\":\"RepositoryUpdateMirrorWorker\",\"args\":[690],\"retry\":false,\"queue\":\"repository_update_mirror\",\"status_expiration\":54000,\"jid\":\"15fb4c5fbfd70b5bf0cee96c\",\"created_at\":1587712718.3639069,\"meta.project\":\"group/project\",\"meta.root_namespace\":\"group\",\"meta.subscription_plan\":\"free\",\"meta.caller_id\":\"ProjectImportScheduleWorker\",\"correlation_id\":\"ed338ec9c2fccaac94278e9a2d25c79e\",\"enqueued_at\":1587712718.3666008}","class":"RepositoryUpdateMirrorWorker","args":["690"],"retry":false,"queue":"repository_update_mirror","status_expiration":54000,"jid":"15fb4c5fbfd70b5bf0cee96c","created_at":"2020-04-24T07:18:38.363Z","meta.project":"group/project","meta.root_namespace":"group","meta.subscription_plan":"free","meta.caller_id":"ProjectImportScheduleWorker","correlation_id":"ed338ec9c2fccaac94278e9a2d25c79e","enqueued_at":"2020-04-24T07:18:38.366Z","error_backtrace":["ee/app/workers/repository_update_mirror_worker.rb:25:in `perform'","lib/gitlab/with_request_store.rb:7:in `with_request_store'","lib/gitlab/sidekiq_daemon/monitor.rb:49:in `within_job'"]}"##;
        assert_eq!(
            Some(SidekiqJsonEventError {
                class: Some("RepositoryUpdateMirrorWorker"),
                time: "2020-04-24T07:18:48.013Z",
                status: None,
                meta_project: Some("group/project"),
                meta_user: None,
                message: None,
                error_class: Some("RepositoryUpdateMirrorWorker::UpdateError"),
                error_message: Some("4:Deadline Exceeded"),
                error_backtrace: Some(vec![
                    "ee/app/workers/repository_update_mirror_worker.rb:25:in `perform'",
                    "lib/gitlab/with_request_store.rb:7:in `with_request_store'",
                    "lib/gitlab/sidekiq_daemon/monitor.rb:49:in `within_job'"
                ]),
                correlation_id: Some("ed338ec9c2fccaac94278e9a2d25c79e"),
            }),
            SidekiqJsonEventError::parse(input)
        );
    }

    #[test]
    fn event_sidekiq_json_time_parse() {
        let input = r##"{"severity":"INFO","time":"2020-04-23T05:19:04.593Z","queue":"cronjob:pipeline_schedule","class":"PipelineScheduleWorker","retry":false,"queue_namespace":"cronjob","jid":"75666b794a52687726000513","created_at":"2020-04-23T05:19:04.544Z","correlation_id":"fa48f5803ad824eabb004aa1774537d6","enqueued_at":"2020-04-23T05:19:04.547Z","pid":29039,"message":"PipelineScheduleWorker JID-75666b794a52687726000513: done: 0.04574 sec","job_status":"done","scheduling_latency_s":0.001009,"duration":0.04574,"cpu_s":0.00551,"completed_at":"2020-04-23T05:19:04.593Z","db_duration":1.021306961774826,"db_duration_s":0.001021306961774826}"##;
        assert_eq!(
            DateTime::parse_from_rfc3339("2020-04-23T05:19:04.593Z").ok(),
            SidekiqJsonEvent::parse(input).and_then(|e| SidekiqJsonEvent::parse_time(e.raw_time()))
        );
    }

    #[test]
    fn event_sidekiq_json_dur_s_parse() {
        let input = r##"{"severity":"INFO","time":"2020-12-10T14:45:25.085Z","queue":"cronjob:pages_domain_verification_cron","class":"PagesDomainVerificationCronWorker","retry":0,"version":0,"queue_namespace":"cronjob","jid":"5f34f9a8e733826ecb5443be","created_at":"2020-12-10T14:45:24.039Z","meta.caller_id":"Cronjob","correlation_id":"b77947611b7b70411ff065cde0d28b94","enqueued_at":"2020-12-10T14:45:24.040Z","pid":14444,"message":"PagesDomainVerificationCronWorker JID-5f34f9a8e733826ecb5443be: done: 0.9085 sec","job_status":"done","scheduling_latency_s":0.13577,"redis_calls":2,"redis_duration_s":0.102813,"redis_read_bytes":2,"redis_write_bytes":207,"redis_queues_calls":2,"redis_queues_duration_s":0.102813,"redis_queues_read_bytes":2,"redis_queues_write_bytes":207,"db_count":1,"db_write_count":0,"db_cached_count":0,"duration_s":0.9085,"cpu_s":0.014339,"completed_at":"2020-12-10T14:45:25.085Z","db_duration_s":0.203637}"##;
        assert_eq!(
            Some(SidekiqJsonDurSEvent {
                class: "PagesDomainVerificationCronWorker",
                status: "done",
                duration_s: 0.9085,
                time: "2020-12-10T14:45:25.085Z",
            }),
            SidekiqJsonDurSEvent::parse(input)
        );
    }

    #[test]
    fn event_sidekiq_json_dur_s_detail_parse() {
        let input = r##"{"severity":"INFO","time":"2020-12-10T14:45:25.085Z","queue":"cronjob:pages_domain_verification_cron","class":"PagesDomainVerificationCronWorker","retry":0,"version":0,"queue_namespace":"cronjob","jid":"5f34f9a8e733826ecb5443be","created_at":"2020-12-10T14:45:24.039Z","meta.caller_id":"Cronjob","correlation_id":"b77947611b7b70411ff065cde0d28b94","enqueued_at":"2020-12-10T14:45:24.040Z","pid":14444,"message":"PagesDomainVerificationCronWorker JID-5f34f9a8e733826ecb5443be: done: 0.9085 sec","job_status":"done","scheduling_latency_s":0.13577,"redis_calls":2,"redis_duration_s":0.102813,"redis_read_bytes":2,"redis_write_bytes":207,"redis_queues_calls":2,"redis_queues_duration_s":0.102813,"redis_queues_read_bytes":2,"redis_queues_write_bytes":207,"db_count":1,"db_write_count":0,"db_cached_count":0,"duration_s":0.9085,"cpu_s":0.014339,"completed_at":"2020-12-10T14:45:25.085Z","db_duration_s":0.203637}"##;
        assert_eq!(
            Some(SidekiqJsonDurSEventDetail {
                class: "PagesDomainVerificationCronWorker",
                status: "done",
                duration_s: 0.9085,
                meta_project: None,
                meta_user: None,
                cpu_s: Some(0.014339),
                db_duration_s: Some(0.203637),
                gitaly_duration_s: None,
                rugged_duration_s: None,
                redis_duration_s: Some(0.102813),
                scheduling_latency_s: Some(0.13577),
                time: "2020-12-10T14:45:25.085Z",
                correlation_id: Some("b77947611b7b70411ff065cde0d28b94"),
            }),
            SidekiqJsonDurSEventDetail::parse(input)
        );
    }

    #[test]
    fn event_sidekiq_json_dur_s_error_parse() {
        let input = r##"{"severity":"WARN","time":"2020-12-15T04:46:55.763Z","error_class":"ActiveRecord::QueryCanceled","error_message":"PG::QueryCanceled: ERROR:  canceling statement due to statement timeout\n","context":"Job raised exception","class":"PipelineHooksWorker","args":["22760"],"retry":3,"queue":"pipeline_hooks:pipeline_hooks","version":0,"queue_namespace":"pipeline_hooks","jid":"c84492817f6c73034cda286c","created_at":"2020-12-15T04:25:56.656Z","meta.user":"myuser","meta.project":"group/project","meta.root_namespace":"group","meta.caller_id":"PipelineProcessWorker","meta.feature_category":"continuous_integration","correlation_id":"wNfAfavMxG5","enqueued_at":"2020-12-15T04:25:56.657Z","interrupted_count":1,"db_count":10,"db_write_count":0,"db_cached_count":0,"error_backtrace":["lib/gitlab/data_builder/pipeline.rb:16:in `map'","lib/gitlab/data_builder/pipeline.rb:16:in `build'","app/models/ci/pipeline.rb:1115:in `pipeline_data'","app/models/ci/pipeline.rb:811:in `execute_hooks'","app/workers/pipeline_hooks_worker.rb:14:in `perform'","lib/gitlab/metrics/sidekiq_middleware.rb:18:in `block in call'","lib/gitlab/metrics/transaction.rb:61:in `run'","lib/gitlab/metrics/sidekiq_middleware.rb:18:in `call'","lib/gitlab/sidekiq_middleware/duplicate_jobs/strategies/until_executing.rb:32:in `perform'","lib/gitlab/sidekiq_middleware/duplicate_jobs/duplicate_job.rb:40:in `perform'","lib/gitlab/sidekiq_middleware/duplicate_jobs/server.rb:8:in `call'","lib/gitlab/sidekiq_middleware/worker_context.rb:9:in `wrap_in_optional_context'","lib/gitlab/sidekiq_middleware/worker_context/server.rb:17:in `block in call'","lib/gitlab/application_context.rb:54:in`block in use'","lib/gitlab/application_context.rb:54:in `use'","lib/gitlab/application_context.rb:21:in `with_context'","lib/gitlab/sidekiq_middleware/worker_context/server.rb:15:in `call'","lib/gitlab/sidekiq_status/server_middleware.rb:7:in `call'","lib/gitlab/sidekiq_versioning/middleware.rb:9:in `call'","lib/gitlab/sidekiq_middleware/admin_mode/server.rb:8:in `call'","lib/gitlab/sidekiq_middleware/instrumentation_logger.rb:7:in `call'","lib/gitlab/sidekiq_middleware/batch_loader.rb:7:in `call'","lib/gitlab/sidekiq_middleware/extra_done_log_metadata.rb:7:in `call'","lib/gitlab/sidekiq_middleware/request_store_middleware.rb:10:in `block in call'","lib/gitlab/with_request_store.rb:17:in `enabling_request_store'","lib/gitlab/with_request_store.rb:10:in `with_request_store'","lib/gitlab/sidekiq_middleware/request_store_middleware.rb:9:in `call'","lib/gitlab/sidekiq_middleware/server_metrics.rb:35:in`call'","lib/gitlab/sidekiq_middleware/monitor.rb:8:in `block in call'","lib/gitlab/sidekiq_daemon/monitor.rb:49:in `within_job'","lib/gitlab/sidekiq_middleware/monitor.rb:7:in `call'","lib/gitlab/sidekiq_logging/structured_logger.rb:18:in `call'"]}"##;
        assert_eq!(
            Some(SidekiqJsonDurSEventError {
                class: Some("PipelineHooksWorker"),
                status: None,
                meta_project: Some("group/project"),
                meta_user: Some("myuser"),
                time: "2020-12-15T04:46:55.763Z",
                message: None,
                error_class: Some("ActiveRecord::QueryCanceled"),
                error_message: Some(Cow::from("PG::QueryCanceled: ERROR:  canceling statement due to statement timeout\n")),
                error_backtrace: Some(vec![
                    "lib/gitlab/data_builder/pipeline.rb:16:in `map'",
                    "lib/gitlab/data_builder/pipeline.rb:16:in `build'",
                    "app/models/ci/pipeline.rb:1115:in `pipeline_data'",
                    "app/models/ci/pipeline.rb:811:in `execute_hooks'",
                    "app/workers/pipeline_hooks_worker.rb:14:in `perform'",
                    "lib/gitlab/metrics/sidekiq_middleware.rb:18:in `block in call'",
                    "lib/gitlab/metrics/transaction.rb:61:in `run'",
                    "lib/gitlab/metrics/sidekiq_middleware.rb:18:in `call'",
                    "lib/gitlab/sidekiq_middleware/duplicate_jobs/strategies/until_executing.rb:32:in `perform'",
                    "lib/gitlab/sidekiq_middleware/duplicate_jobs/duplicate_job.rb:40:in `perform'",
                    "lib/gitlab/sidekiq_middleware/duplicate_jobs/server.rb:8:in `call'",
                    "lib/gitlab/sidekiq_middleware/worker_context.rb:9:in `wrap_in_optional_context'",
                    "lib/gitlab/sidekiq_middleware/worker_context/server.rb:17:in `block in call'",
                    "lib/gitlab/application_context.rb:54:in`block in use'",
                    "lib/gitlab/application_context.rb:54:in `use'",
                    "lib/gitlab/application_context.rb:21:in `with_context'",
                    "lib/gitlab/sidekiq_middleware/worker_context/server.rb:15:in `call'",
                    "lib/gitlab/sidekiq_status/server_middleware.rb:7:in `call'",
                    "lib/gitlab/sidekiq_versioning/middleware.rb:9:in `call'",
                    "lib/gitlab/sidekiq_middleware/admin_mode/server.rb:8:in `call'",
                    "lib/gitlab/sidekiq_middleware/instrumentation_logger.rb:7:in `call'",
                    "lib/gitlab/sidekiq_middleware/batch_loader.rb:7:in `call'",
                    "lib/gitlab/sidekiq_middleware/extra_done_log_metadata.rb:7:in `call'",
                    "lib/gitlab/sidekiq_middleware/request_store_middleware.rb:10:in `block in call'",
                    "lib/gitlab/with_request_store.rb:17:in `enabling_request_store'",
                    "lib/gitlab/with_request_store.rb:10:in `with_request_store'",
                    "lib/gitlab/sidekiq_middleware/request_store_middleware.rb:9:in `call'",
                    "lib/gitlab/sidekiq_middleware/server_metrics.rb:35:in`call'",
                    "lib/gitlab/sidekiq_middleware/monitor.rb:8:in `block in call'",
                    "lib/gitlab/sidekiq_daemon/monitor.rb:49:in `within_job'",
                    "lib/gitlab/sidekiq_middleware/monitor.rb:7:in `call'",
                    "lib/gitlab/sidekiq_logging/structured_logger.rb:18:in `call'"
            ]),
                correlation_id: Some("wNfAfavMxG5"),
                severity: "WARN",
            }),
            SidekiqJsonDurSEventError::parse(input)
        );
    }

    #[test]
    fn event_sidekiq_json_dur_s_parse_time() {
        let input = r##"{"severity":"INFO","time":"2020-12-10T14:45:25.085Z","queue":"cronjob:pages_domain_verification_cron","class":"PagesDomainVerificationCronWorker","retry":0,"version":0,"queue_namespace":"cronjob","jid":"5f34f9a8e733826ecb5443be","created_at":"2020-12-10T14:45:24.039Z","meta.caller_id":"Cronjob","correlation_id":"b77947611b7b70411ff065cde0d28b94","enqueued_at":"2020-12-10T14:45:24.040Z","pid":14444,"message":"PagesDomainVerificationCronWorker JID-5f34f9a8e733826ecb5443be: done: 0.9085 sec","job_status":"done","scheduling_latency_s":0.13577,"redis_calls":2,"redis_duration_s":0.102813,"redis_read_bytes":2,"redis_write_bytes":207,"redis_queues_calls":2,"redis_queues_duration_s":0.102813,"redis_queues_read_bytes":2,"redis_queues_write_bytes":207,"db_count":1,"db_write_count":0,"db_cached_count":0,"duration_s":0.9085,"cpu_s":0.014339,"completed_at":"2020-12-10T14:45:25.085Z","db_duration_s":0.203637}"##;
        assert_eq!(
            DateTime::parse_from_rfc3339("2020-12-10T14:45:25.085Z").ok(),
            SidekiqJsonDurSEvent::parse(input)
                .and_then(|e| SidekiqJsonDurSEvent::parse_time(e.raw_time()))
        );
    }

    #[test]
    fn event_empty_backtrace_array_is_none() {
        let input = r##"{"time":"2021-04-20T21:48:07.455Z","severity":"INFO","duration_s":60.01213,"db_duration_s":0.04218,"view_duration_s":59.96995,"status":500,"method":"POST","path":"/api/v4/internal/allowed","params":[{"key":"action","value":"git-receive-pack"},{"key":"gl_repository","value":"project-130"},{"key":"project","value":"/var/opt/gitlab/git-data/repositories/@hashed/38/d6/38d66d9692ac590000a91b03a88da1c88d51fab2b78f63171f553ecc551a0c6f.git"},{"key":"changes","value":"12345 674890 refs/heads/my-branch\n"},{"key":"protocol","value":"ssh"},{"key":"env","value":"{\"GIT_ALTERNATE_OBJECT_DIRECTORIES_RELATIVE\":[\"objects\"],\"GIT_OBJECT_DIRECTORY_RELATIVE\":\"objects/incoming-CLYjrl\"}"},{"key":"key_id","value":"982"}],"host":"unix","remote_ip":"127.0.0.1","ua":"gitaly/13.9.6","route":"/api/:version/internal/allowed","exception.class":"Rack::Timeout::RequestTimeoutException","exception.message":"Request ran for longer than 60000ms","exception.backtrace":[],"queue_duration_s":48.406452,"gitaly_calls":3,"gitaly_duration_s":59.922134,"redis_calls":4,"redis_duration_s":0.004869,"redis_read_bytes":621,"redis_write_bytes":232,"redis_cache_calls":4,"redis_cache_duration_s":0.004869,"redis_cache_read_bytes":621,"redis_cache_write_bytes":232,"db_count":11,"db_write_count":0,"db_cached_count":0,"cpu_s":0.111784,"correlation_id":"01F3RM0M9886B51ET2GYCY82RA","meta.user":"myuser","meta.project":"group/project","meta.root_namespace":"group","meta.caller_id":"/api/:version/internal/allowed","meta.remote_ip":"127.0.0.1","meta.feature_category":"source_code_management","content_length":"525"}"##;
        assert!(ApiJsonDurSEventError::parse(input)
            .and_then(|e| e.backtrace(3, 2))
            .is_none());
    }

    #[test]
    fn event_sidekiq_json_dur_s_bare_error_message() {
        let input = r##"{"severity":"ERROR","time":"2021-07-27T09:44:32.567Z","message":"Could not remove remote my_remote_mirror from project 1"}"##;
        assert_eq!(
            Some(SidekiqJsonDurSEventError {
                class: None,
                status: None,
                meta_project: None,
                meta_user: None,
                time: "2021-07-27T09:44:32.567Z",
                message: Some("Could not remove remote my_remote_mirror from project 1"),
                error_class: None,
                error_message: None,
                error_backtrace: None,
                correlation_id: None,
                severity: "ERROR",
            }),
            SidekiqJsonDurSEventError::parse(input)
        );
    }

    #[test]
    fn event_sidekiq_json_dur_s_ignore_message_not_error_severity() {
        let input =
            r##"{"severity":"INFO","time":"2021-07-27T09:44:32.567Z","message":"Hello, world!"}"##;
        assert_eq!(
            None,
            SidekiqJsonDurSEventError::parse(input).unwrap().error(),
        );
    }
}
