use crate::time_chunks::Interval;

use clap::{App, Arg, SubCommand};
use std::convert::TryFrom;

pub fn cli_args() -> App<'static, 'static> {
    let app = App::new("fast stats")
        .version(clap::crate_version!())
        .author(clap::crate_authors!())
        .about("Basic performance statistics for GitLab logs")
        .subcommand(
            SubCommand::with_name("top")
                .about("Show top path/project/user by <SORT_BY>")
                .arg(
                    Arg::with_name("file")
                        .help("File to be parsed")
                        .takes_value(true)
                        .value_name("FILE_NAME")
                        .number_of_values(1),
                )
                .arg(
                    Arg::with_name("display")
                        .help("Show percentage, values, or both (default percentage)")
                        .short("d")
                        .long("display")
                        .takes_value(true)
                        .value_name("DISPLAY_FMT")
                        .number_of_values(1)
                        .possible_values(&["value", "perc", "both"]),
                )
                .arg(
                    Arg::with_name("interval")
                        .help("Split results into INTERVAL_LEN time slices. INTERVAL_LEN must be in <LEN>[h|m|s] format")
                        .short("i")
                        .long("interval")
                        .takes_value(true)
                        .value_name("INTERVAL_LEN")
                        .validator(validate_interval),
                )
                .arg(
                    Arg::with_name("format")
                        .help("Format to print in (default text)")
                        .short("f")
                        .long("format")
                        .takes_value(true)
                        .value_name("FORMAT")
                        .possible_values(&["text", "json"]),
                )
                .arg(
                    Arg::with_name("limit")
                        .help("Number of results to display")
                        .short("l")
                        .long("limit")
                        .takes_value(true)
                        .value_name("LIMIT_CT")
                        .number_of_values(1)
                        .validator(validate_limit),
                )
                .arg(
                    Arg::with_name("sort_by")
                        .help("Field to sort by descending value (default duration)")
                        .short("s")
                        .long("sort")
                        .takes_value(true)
                        .value_name("SORT_BY")
                        .possible_values(&[
                            "count", "rps", "duration", "db", "redis", "gitaly", "rugged", "queue", "cpu", "fail",
                        ])
                    )
        )
        .subcommand(
            SubCommand::with_name("errors")
                .about("Show summary of errors captured in log")
                .arg(
                    Arg::with_name("file")
                        .help("File to be parsed")
                        .takes_value(true)
                        .value_name("FILE_NAME")
                        .number_of_values(1),
                )
                .arg(
                    Arg::with_name("force_color")
                        .help("Output colored text even if writing to a pipe or file")
                        .short("C")
                        .long("color-output"),
                )
                .arg(
                    Arg::with_name("format")
                        .help("Format to print in (default text)")
                        .short("f")
                        .long("format")
                        .takes_value(true)
                        .value_name("FORMAT")
                        .possible_values(&["text", "json"]),
                )
                .arg(
                    Arg::with_name("no_border")
                        .help("Don't print borders around results")
                        .short("b")
                        .long("no-border"),
                )
        )
        .arg(
            Arg::with_name("file")
                .help("File to be parsed")
                .takes_value(true)
                .value_name("FILE_NAME")
                .number_of_values(1),
        )
        .arg(
            Arg::with_name("compare")
                .help("Calculate performance changes between two files")
                .short("c")
                .long("compare")
                .takes_value(true)
                .value_name("COMPARE_FILE")
                .number_of_values(1),
        )
        .arg(
            Arg::with_name("benchmark")
                .help("Compare performance to GitLab.com")
                .short("b")
                .long("bench")
                .takes_value(true)
                .value_name("BENCH_VERSION")
                .number_of_values(1)
                .possible_values(&["12.0", "12.1", "12.2", "12.3", "12.4", "12.5", "12.6", "12.7", "12.8",
                                   "12.9", "12.10", "13.0", "13.1", "13.2", "13.3", "13.4", "13.5", "13.6",
                                   "13.7", "13.8", "13.9", "13.10", "13.11", "13.12", "14.0", "14.1", "14.2",
                                   "14.3", "14.4", "14.5", "14.6", "14.7", "14.8"]),
        )
        .arg(
            Arg::with_name("force_color")
                .help("Output colored text even if writing to a pipe or file")
                .short("C")
                .long("color-output"),
        )
        .arg(
            Arg::with_name("format")
                .help("Format to print in (default text)")
                .short("f")
                .long("format")
                .takes_value(true)
                .value_name("FORMAT")
                .possible_values(&["text", "md", "csv", "json"]),
        )
        .arg(
            Arg::with_name("interval")
                .help("Split results into INTERVAL_LEN time slices. INTERVAL_LEN must be in <LEN>[h|m|s] format")
                .short("i")
                .long("interval")
                .takes_value(true)
                .value_name("INTERVAL_LEN")
                .validator(validate_interval),
        )
        .arg(
            Arg::with_name("limit")
                .help("Number of results to display")
                .short("l")
                .long("limit")
                .takes_value(true)
                .value_name("LIMIT_CT")
                .number_of_values(1)
                .validator(validate_limit),
        )
        .arg(
            Arg::with_name("log_type")
                .help("Manually specify log type of <INPUT> if log type cannot be deduced automatically")
                .short("t")
                .long("type")
                .takes_value(true)
                .value_name("LOG_TYPE")
                .possible_values(&[
                    "api_json",
                    "api_json_dur_s",
                    "gitaly",
                    "gitaly_json",
                    "production_json",
                    "production_json_dur_s",
                    "shell_json",
                    "sidekiq",
                    "sidekiq_json",
                    "sidekiq_json_dur_s",
                ]),
        )
        .arg(
            Arg::with_name("print_fields")
                .help("Comma separated list of fields to be printed")
                .short("p")
                .long("print-fields")
                .takes_value(true)
                .value_name("PRINT_FIELDS")
                .use_delimiter(true)
                .possible_values(&["score", "count", "rps", "max", "median", "min", "p99", "p95", "fail"])
        )
        .arg(
            Arg::with_name("search")
                .help("Case-insensitive search of controller/method/worker field")
                .short("S")
                .long("search")
                .takes_value(true)
                .value_name("SEARCH_FOR")
                .validator(validate_search),
        )
        .arg(
            Arg::with_name("sort_by")
                .help("Field to sort by descending value (default score)")
                .short("s")
                .long("sort")
                .takes_value(true)
                .value_name("SORT_BY")
                .possible_values(&[
                    "score", "count", "rps", "max", "median", "min", "p99", "p95", "fail",
                ]),
        )
        .arg(
            Arg::with_name("verbose")
                .help("Print details of p99, p95, and median events")
                .short("v")
                .long("verbose")
        );

    #[cfg(feature = "plot")]
    let app = app.subcommand(
        SubCommand::with_name("plot")
            .about("Generate a PNG file with graphs of relevant metrics")
            .arg(
                Arg::with_name("file")
                    .help("File to be parsed")
                    .takes_value(true)
                    .value_name("FILE_NAME")
                    .number_of_values(1),
            )
            .arg(
                Arg::with_name("output_file")
                    .help("Name of file to write to")
                    .short("o")
                    .long("outfile")
                    .takes_value(true)
                    .value_name("OUTPUT_FILE")
                    .number_of_values(1),
            ),
    );

    #[cfg(feature = "compress")]
    let app = app.arg(
        Arg::with_name("compress")
            .help("Create lz4 compressed archive of bincode serialized StatsVec")
            .long("compress")
            .takes_value(true)
            .value_name("COMPRESS_OUTPUT_FILE")
            .number_of_values(1),
    );

    app
}

fn validate_limit(c: String) -> Result<(), String> {
    if c.parse::<usize>().is_ok() {
        return Ok(());
    }
    Err(String::from("COUNT must be a non-negative integer"))
}

fn validate_search(s: String) -> Result<(), String> {
    if s.is_ascii() && !s.is_empty() {
        return Ok(());
    }
    Err(String::from("SEARCH_FOR must be a non-empty ASCII string"))
}

fn validate_interval(s: String) -> Result<(), String> {
    let res = Interval::try_from(s.as_str());

    match res {
        Ok(_) => Ok(()),
        Err(e) => Err(e.to_string()),
    }
}
