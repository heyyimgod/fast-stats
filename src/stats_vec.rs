use crate::data::raw::RawDataMap;
use crate::field::StatField;
use crate::stats::{PrimField, Stats, StatsType};
use crate::util::{BRIGHT_GREEN, GREEN, RED, RESET, YELLOW};
use crate::{print, HashMap};

use chrono::Duration;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use std::io::{self, stdout, Error, Write};

const DEFAULT_PRIMARY_LEN: usize = 15;

#[derive(Clone, Copy, Debug)]
pub enum PrintFormat {
    Text,
    Md,
    Csv,
    Json,
}

impl Default for PrintFormat {
    fn default() -> Self {
        PrintFormat::Text
    }
}

impl From<&str> for PrintFormat {
    fn from(s: &str) -> Self {
        match s {
            "csv" => PrintFormat::Csv,
            "json" => PrintFormat::Json,
            "md" => PrintFormat::Md,
            "text" => PrintFormat::Text,
            _ => PrintFormat::default(),
        }
    }
}

#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct StatsVec {
    pub stats_t: StatsType,
    pub stats: Vec<Stats>,
}

impl StatsVec {
    pub fn new(stats_t: StatsType, stats: Vec<Stats>) -> StatsVec {
        StatsVec { stats_t, stats }
    }

    pub fn from_raw_data(mut event_map: RawDataMap, dur: Duration, stats_t: StatsType) -> StatsVec {
        let stats: Vec<_> = event_map
            .data
            .par_drain()
            .map(|(k, v)| Stats::from_raw_data(k, v, dur))
            .collect();

        StatsVec { stats_t, stats }
    }

    pub fn sort_by(&mut self, sort_by: StatField) {
        use StatField::*;
        match sort_by {
            Count => {
                self.stats
                    .par_sort_unstable_by(|x, y| (&y.count).cmp(&x.count));
            }
            Rps => {
                self.stats.par_sort_unstable_by(|x, y| {
                    (&y.rps)
                        .partial_cmp(&x.rps)
                        .expect("Invalid comparison when sorting")
                });
            }
            Fail => {
                self.stats.par_sort_unstable_by(|x, y| {
                    (&y.perc_failed)
                        .partial_cmp(&x.perc_failed)
                        .expect("Invalid comparison when sorting")
                });
            }
            Max => {
                self.stats.par_sort_unstable_by(|x, y| {
                    (&y.max)
                        .partial_cmp(&x.max)
                        .expect("Invalid comparison when sorting")
                });
            }
            Median => {
                self.stats.par_sort_unstable_by(|x, y| {
                    (&y.median)
                        .partial_cmp(&x.median)
                        .expect("Invalid comparison when sorting")
                });
            }
            Min => {
                self.stats.par_sort_unstable_by(|x, y| {
                    (&y.min)
                        .partial_cmp(&x.min)
                        .expect("Invalid comparison when sorting")
                });
            }
            P99 => {
                self.stats.par_sort_unstable_by(|x, y| {
                    (&y.p99)
                        .partial_cmp(&x.p99)
                        .expect("Invalid comparison when sorting")
                });
            }
            P95 => {
                self.stats.par_sort_unstable_by(|x, y| {
                    (&y.p95)
                        .partial_cmp(&x.p95)
                        .expect("Invalid comparison when sorting")
                });
            }
            Score => {
                self.stats.par_sort_unstable_by(|x, y| {
                    (&y.score)
                        .partial_cmp(&x.score)
                        .expect("Invalid comparison when sorting")
                });
            }
        }
    }

    pub fn print(
        &self,
        format: PrintFormat,
        fields: &[StatField],
        limit: Option<usize>,
        primary_len: Option<usize>,
        events: Option<&HashMap<String, [Option<String>; 4]>>,
    ) -> io::Result<()> {
        use PrintFormat::*;

        let stdout = stdout();
        let out = stdout.lock();

        match format {
            Csv => self.print_csv(out, fields, limit),
            Json => self.print_json(out, fields, limit),
            Md => self.print_md(out, fields, limit),
            Text => self.print_text(out, fields, limit, primary_len, events),
        }
    }

    fn print_csv(
        &self,
        mut out: impl Write,
        fields: &[StatField],
        limit: Option<usize>,
    ) -> Result<(), Error> {
        writeln!(out, "{}", print::csv_header(self.stats_t, fields, None))?;

        let limit = self.limit(limit);
        for stat in self.stats.iter().take(limit) {
            writeln!(out, "{}", stat.csv(fields))?;
        }

        Ok(())
    }

    fn print_json(
        &self,
        mut out: impl Write,
        _fields: &[StatField],
        _limit: Option<usize>,
    ) -> Result<(), Error> {
        let serialized = serde_json::to_string(&self).expect("failed to serialize diffs");
        writeln!(out, "{}", serialized)
    }

    fn print_md(
        &self,
        mut out: impl Write,
        fields: &[StatField],
        limit: Option<usize>,
    ) -> Result<(), Error> {
        writeln!(out, "{}", print::md_header(self.stats_t, fields, None))?;

        let limit = self.limit(limit);
        for stat in self.stats.iter().take(limit) {
            writeln!(out, "{}", stat.md(fields))?;
        }

        Ok(())
    }

    fn print_text(
        &self,
        mut out: impl Write,
        fields: &[StatField],
        limit: Option<usize>,
        primary_len: Option<usize>,
        events: Option<&HashMap<String, [Option<String>; 4]>>,
    ) -> Result<(), Error> {
        let mut lens = self.col_lens();
        match primary_len {
            Some(p_len) => {
                lens[0] = p_len;
            }
            None => {
                lens[0] = self.primary_col_len(limit);
            }
        }

        if let Some(p_len) = primary_len {
            lens[0] = p_len;
        }

        writeln!(
            out,
            "{}",
            print::text_header(self.stats_t, fields, &lens, None)
        )?;

        let limit = self.limit(limit);
        for stat in self.stats.iter().take(limit) {
            writeln!(out, "{}", stat.text(PrimField::Print, fields, &lens))?;

            if let Some(evt_map) = &events {
                if let Some(items) = evt_map.get(&stat.prim_field) {
                    for item in items.iter().filter(|i| i.is_some()) {
                        writeln!(out, "    {}", item.as_deref().unwrap_or_default())?;
                    }
                    writeln!(out)?;
                }
            }
        }

        Ok(())
    }

    pub fn col_lens(&self) -> [usize; 10] {
        [
            self.primary_col_len(None),
            self.find_col_len(|s| s.count as usize, 8),
            self.find_col_len(|s| s.rps as usize, 3) + 3, // +3 for two-digit precision floating point
            self.find_col_len(|s| s.p99 as usize, 6) + 2, // +2 for one-digit precision floating point
            self.find_col_len(|s| s.p95 as usize, 6) + 2,
            self.find_col_len(|s| s.median as usize, 8) + 2,
            self.find_col_len(|s| s.max as usize, 6) + 2,
            self.find_col_len(|s| s.min as usize, 6) + 2,
            self.find_col_len(|s| s.score as usize, 6) + 2,
            self.find_col_len(|s| s.perc_failed as usize, 5) + 3,
        ]
    }

    pub fn primary_col_len(&self, limit: Option<usize>) -> usize {
        let lim = match limit {
            Some(l) => l,
            _ => self.stats.len(),
        };

        self.stats
            .iter()
            .take(lim)
            .map(|s| s.prim_field.len())
            .max()
            .unwrap_or(DEFAULT_PRIMARY_LEN)
    }

    fn find_col_len<F>(&self, field_fn: F, min_len: usize) -> usize
    where
        F: FnMut(&Stats) -> usize,
    {
        let max = self.stats.iter().map(field_fn).max().unwrap_or_default();
        (format!("{}", max).len()).max(min_len)
    }

    fn limit(&self, limit: Option<usize>) -> usize {
        limit.unwrap_or(self.stats.len())
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct StatsDiff<'a> {
    pub stats_a: &'a Stats,
    pub stats_b: &'a Stats,
    file_name_a: &'a str,
    file_name_b: &'a str,
    ct_diff: f64,
    rps_diff: f64,
    p99_diff: f64,
    p95_diff: f64,
    med_diff: f64,
    max_diff: f64,
    min_diff: f64,
    scr_diff: f64,
    fail_diff: f64,
}

impl<'a> StatsDiff<'a> {
    fn new(
        stats_a: &'a Stats,
        stats_b: &'a Stats,
        file_name_a: &'a str,
        file_name_b: &'a str,
    ) -> StatsDiff<'a> {
        let ct_diff = StatsDiff::calc_ratio(stats_a.count as f64, stats_b.count as f64);
        let rps_diff = StatsDiff::calc_ratio(stats_a.rps, stats_b.rps);
        let p99_diff = StatsDiff::calc_ratio(stats_a.p99, stats_b.p99);
        let p95_diff = StatsDiff::calc_ratio(stats_a.p95, stats_b.p95);
        let med_diff = StatsDiff::calc_ratio(stats_a.median, stats_b.median);
        let max_diff = StatsDiff::calc_ratio(stats_a.max, stats_b.max);
        let min_diff = StatsDiff::calc_ratio(stats_a.min, stats_b.min);
        let scr_diff = StatsDiff::calc_ratio(stats_a.score, stats_b.score);
        let fail_diff = StatsDiff::calc_ratio(stats_a.perc_failed, stats_b.perc_failed);

        StatsDiff {
            stats_a,
            stats_b,
            file_name_a,
            file_name_b,
            ct_diff,
            rps_diff,
            p99_diff,
            p95_diff,
            med_diff,
            max_diff,
            min_diff,
            scr_diff,
            fail_diff,
        }
    }

    fn calc_ratio(stat: f64, other: f64) -> f64 {
        let ratio = other / stat;

        if ratio.is_nan() {
            0.0
        } else {
            ratio
        }
    }

    pub fn csv_diffs(&self, fields: &[StatField]) -> String {
        let mut output = String::from("RATIO,");

        for field in fields {
            output += &format!(
                ",{:.precision$}",
                self.val(field),
                precision = field.precision()
            );
        }

        output
    }

    pub fn md_diffs(&self, fields: &[StatField]) -> String {
        let mut output = String::from("|RATIO|");

        for field in fields {
            output += &format!(
                "|{:.precision$}",
                self.val(field),
                precision = field.precision()
            );
        }

        format!("{}|", output)
    }

    fn text_diffs(&self, fields: &[StatField], lens: &[usize], color: bool) -> String {
        use StatField::*;

        let mut output = format!(" {:<len$}", "", len = lens[0]);
        if color {
            for field in fields {
                let len = field.len(lens);
                output += &match field {
                    Count | Rps => format!("{:>width$.2}x ", self.val(field), width = len),
                    Fail | P99 | P95 | Median | Max | Min | Score => colorize(self.val(field), len),
                };
            }
        } else {
            for field in fields {
                let len = field.len(lens);
                output += &format!("{:>width$.2}x ", self.val(field), width = len)
            }
        }

        output
    }

    #[inline]
    fn val(&self, field: &StatField) -> f64 {
        match field {
            StatField::Count => self.ct_diff,
            StatField::Rps => self.rps_diff,
            StatField::P99 => self.p99_diff,
            StatField::P95 => self.p95_diff,
            StatField::Median => self.med_diff,
            StatField::Max => self.max_diff,
            StatField::Min => self.min_diff,
            StatField::Score => self.scr_diff,
            StatField::Fail => self.fail_diff,
        }
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct StatsDiffVec<'a> {
    pub stats_t: StatsType,
    diffs: Vec<StatsDiff<'a>>,
    max_lens: Vec<usize>,
}

impl<'a> StatsDiffVec<'a> {
    pub fn new(
        stats_t: StatsType,
        stats_vec_a: &'a StatsVec,
        stats_vec_b: &'a StatsVec,
        file_name_a: &'a str,
        file_name_b: &'a str,
        limit: Option<usize>,
    ) -> StatsDiffVec<'a> {
        let diffs: Vec<_> = stats_vec_b
            .stats
            .par_iter()
            .filter_map(|stat_b| {
                stats_vec_a
                    .stats
                    .iter()
                    .find(|stat| stat.prim_field == stat_b.prim_field)
                    .map(|stat_a| StatsDiff::new(stat_a, stat_b, file_name_a, file_name_b))
            })
            .collect();

        let lens_a = stats_vec_a.col_lens();
        let lens_b = stats_vec_b.col_lens();

        let mut max_lens: Vec<_> = lens_a
            .iter()
            .zip(&lens_b)
            .map(|(&x, &y)| if x > y { x } else { y })
            .collect();

        let prim_lim = limit.unwrap_or(diffs.len());
        max_lens[0] = diffs
            .iter()
            .take(prim_lim)
            .map(|d| d.stats_a.prim_field.len())
            .max()
            .unwrap_or(DEFAULT_PRIMARY_LEN);

        StatsDiffVec {
            stats_t,
            diffs,
            max_lens,
        }
    }

    pub fn print(
        &self,
        format: PrintFormat,
        fields: &[StatField],
        limit: Option<usize>,
        force_color: bool,
    ) -> io::Result<()> {
        use PrintFormat::*;

        match format {
            Csv => self.print_csv(fields, limit),
            Json => self.print_json(fields, limit),
            Md => self.print_md(fields, limit),
            Text => self.print_text(fields, limit, force_color),
        }
    }

    fn print_csv(&self, fields: &[StatField], limit: Option<usize>) -> Result<(), Error> {
        let stdout = stdout();
        let mut out = stdout.lock();

        writeln!(
            out,
            "{}",
            print::csv_header(self.stats_t, fields, Some("FILE"))
        )?;

        let limit = self.limit(limit);
        for diff in self.diffs.iter().take(limit) {
            write!(out, "{},", diff.file_name_a)?;
            writeln!(out, "{}", diff.stats_a.csv(fields))?;
        }

        for diff in self.diffs.iter().take(limit) {
            write!(out, "{},", diff.file_name_b)?;
            writeln!(out, "{}", diff.stats_b.csv(fields))?;
        }

        for diff in self.diffs.iter().take(limit) {
            writeln!(out, "{}", diff.csv_diffs(fields))?;
        }

        Ok(())
    }

    fn print_json(&self, _fields: &[StatField], _limit: Option<usize>) -> Result<(), Error> {
        let stdout = stdout();
        let mut out = stdout.lock();

        let serialized = serde_json::to_string(&self).expect("failed to serialize diffs");
        writeln!(out, "{}", serialized)
    }

    fn print_md(&self, fields: &[StatField], limit: Option<usize>) -> Result<(), Error> {
        let stdout = stdout();
        let mut out = stdout.lock();

        writeln!(
            out,
            "{}",
            print::md_header(self.stats_t, fields, Some("FILE"))
        )?;

        let limit = self.limit(limit);
        for diff in self.diffs.iter().take(limit) {
            write!(out, "|{}", diff.file_name_a)?;
            writeln!(out, "{}", diff.stats_a.md(fields))?;

            write!(out, "|{}", diff.file_name_b)?;
            writeln!(out, "{}", diff.stats_b.md(fields))?;

            writeln!(out, "{}", diff.md_diffs(fields))?;
        }

        Ok(())
    }

    fn print_text(
        &self,
        fields: &[StatField],
        limit: Option<usize>,
        force_color: bool,
    ) -> Result<(), Error> {
        use PrimField::*;

        let stdout = stdout();
        let mut out = stdout.lock();

        if let Some(first_diff) = &self.diffs.first() {
            let file_len = first_diff
                .file_name_a
                .len()
                .max(first_diff.file_name_b.len());

            let header = print::text_header(
                self.stats_t,
                fields,
                &self.max_lens,
                Some(("FILE", file_len)),
            );
            writeln!(out, "{}", header)?;

            let limit = self.limit(limit);
            for diff in self.diffs.iter().take(limit) {
                write!(
                    out,
                    "{:<f_len$}  ",
                    first_diff.file_name_a,
                    f_len = file_len
                )?;
                writeln!(out, "{}", diff.stats_a.text(Print, fields, &self.max_lens))?;

                write!(out, "{:<len$}  ", first_diff.file_name_b, len = file_len)?;
                writeln!(out, "{}", diff.stats_b.text(Blank, fields, &self.max_lens))?;

                write!(out, "{:<f_len$}  ", "ratio", f_len = file_len)?;
                writeln!(
                    out,
                    "{}",
                    diff.text_diffs(fields, &self.max_lens, force_color)
                )?;

                writeln!(out)?;
            }
        }

        Ok(())
    }

    fn len(&self) -> usize {
        self.diffs.len()
    }

    fn limit(&self, limit: Option<usize>) -> usize {
        limit.unwrap_or_else(|| self.len())
    }
}

fn colorize(num: f64, len: usize) -> String {
    if num > 5.0 {
        format!("{}{: >l$.2}x{} ", RED, num, RESET, l = len)
    } else if num >= 2.0 {
        format!("{}{: >l$.2}x{} ", YELLOW, num, RESET, l = len)
    } else if num <= 0.66 && num > 0.0 {
        format!("{}{: >l$.2}x{} ", BRIGHT_GREEN, num, RESET, l = len)
    } else if num < 0.25 && num > 0.0 {
        format!("{}{: >l$.2}x{} ", GREEN, num, RESET, l = len)
    } else {
        format!("{: >l$.2}x ", num, l = len)
    }
}

#[derive(Clone, Debug)]
pub struct VerboseStatsVec {
    pub stats_t: StatsType,
    stats: StatsVec,
    examples: HashMap<String, [Option<String>; 4]>,
}

impl VerboseStatsVec {
    pub fn new(
        stats_t: StatsType,
        stats: StatsVec,
        examples: HashMap<String, [Option<String>; 4]>,
    ) -> Self {
        VerboseStatsVec {
            stats_t,
            stats,
            examples,
        }
    }

    pub fn print(
        &self,
        fmt: PrintFormat,
        fields: &[StatField],
        limit: Option<usize>,
    ) -> io::Result<()> {
        self.stats
            .print(fmt, fields, limit, None, Some(&self.examples))
    }
}
