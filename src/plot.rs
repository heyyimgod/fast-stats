use crate::data::detail::DetailData;
use crate::field::EventField;
use crate::time_chunks::{DetailTimeChunks, Interval};
use crate::util;

use chrono::{prelude::*, Duration};
use plotters::coord::Shift;
use plotters::prelude::*;
use plotters::style::RGBAColor;
use rayon::prelude::*;
use std::collections::BTreeMap;
use std::ops::Range;
use std::path::Path;

const PNG_WIDTH: u32 = 2560;
const PNG_HEIGHT: u32 = 1440;

const P99_LABEL: &str = "P99 DUR";
const P90_LABEL: &str = "P90 DUR";
const MEDIAN_LABEL: &str = "MEDIAN DUR";

pub fn plot(
    chunks: DetailTimeChunks,
    interval: Interval,
    output_path: &Path,
) -> Result<(), Box<dyn std::error::Error>> {
    let colors = [
        Palette99::pick(0).mix(0.5),
        Palette99::pick(8).mix(0.7),
        BLUE.mix(0.7),
    ];

    let start = chunks.start_time;
    let end = chunks.end_time;

    let time_range = start..end;

    let combined_data = coalesce_data(&chunks);

    let root = BitMapBackend::new(&output_path, (PNG_WIDTH, PNG_HEIGHT)).into_drawing_area();
    root.fill(&WHITE)?;

    let main_split = root.split_evenly((1, 2));
    let right_area = &main_split[1];

    let (main_area, bottom_left_area) =
        main_split[0].split_vertically(PNG_HEIGHT - (PNG_HEIGHT / 3));

    draw_main_chart(
        &main_area,
        "Request Duration",
        EventField::Duration,
        &combined_data,
        time_range.clone(),
        &colors,
    )?;

    let bottom_left_split = bottom_left_area.split_evenly((1, 2));
    let rate_area = &bottom_left_split[0];
    let queue_area = &bottom_left_split[1];

    let other_areas = right_area.split_evenly((3, 2));

    let db_area = &other_areas[0];
    let redis_area = &other_areas[1];
    let gitaly_area = &other_areas[2];
    let rugged_area = &other_areas[3];
    let cpu_area = &other_areas[4];
    let fail_area = &other_areas[5];

    draw_rps_chart(
        rate_area,
        "Request Rate",
        &combined_data,
        time_range.clone(),
        interval,
    )?;

    draw_pctl_chart(
        db_area,
        "DB Duration",
        EventField::Db,
        &combined_data,
        time_range.clone(),
        &colors,
    )?;

    draw_pctl_chart(
        redis_area,
        "Redis Duration",
        EventField::Redis,
        &combined_data,
        time_range.clone(),
        &colors,
    )?;

    draw_pctl_chart(
        gitaly_area,
        "Gitaly Duration",
        EventField::Gitaly,
        &combined_data,
        time_range.clone(),
        &colors,
    )?;

    draw_pctl_chart(
        rugged_area,
        "Rugged Duration",
        EventField::Rugged,
        &combined_data,
        time_range.clone(),
        &colors,
    )?;

    draw_pctl_chart(
        queue_area,
        "Queue Duration",
        EventField::Queue,
        &combined_data,
        time_range.clone(),
        &colors,
    )?;

    draw_cpu_chart(cpu_area, "CPU Time", &combined_data, time_range.clone())?;

    draw_fail_chart(fail_area, "Failure Rate", &combined_data, time_range)?;

    Ok(())
}

fn draw_main_chart<DB: DrawingBackend>(
    area: &DrawingArea<DB, Shift>,
    title: &str,
    field: EventField,
    data: &BTreeMap<DateTime<FixedOffset>, DetailData>,
    x_range: Range<DateTime<FixedOffset>>,
    colors: &[RGBAColor],
) -> Result<(), DrawingAreaErrorKind<DB::ErrorType>> {
    let median = percentiles(50, field, data);
    let p90 = percentiles(90, field, data);
    let p99 = percentiles(99, field, data);

    let y_range = range(p99.values());

    let mut chart = ChartBuilder::on(area)
        .caption(title, ("sans-serif", 30).into_font())
        .margin(20)
        .set_label_area_size(LabelAreaPosition::Left, 70)
        .set_label_area_size(LabelAreaPosition::Bottom, 40)
        .build_cartesian_2d(x_range, y_range)?;

    chart
        .configure_mesh()
        .disable_mesh()
        .x_desc("Time")
        .y_desc("Duration (sec)")
        .axis_desc_style(("sans-serif", 12).into_font())
        .x_labels(8)
        .x_label_formatter(&|t| t.to_rfc3339_opts(SecondsFormat::Secs, true))
        .y_labels(10)
        .draw()?;

    chart
        .draw_series(AreaSeries::new(p99.into_iter(), 0.0, &colors[0]))?
        .label(P99_LABEL)
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &colors[0]));

    chart
        .draw_series(AreaSeries::new(p90.into_iter(), 0.0, &colors[1]))?
        .label(P90_LABEL)
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &colors[1]));

    chart
        .draw_series(AreaSeries::new(median.into_iter(), 0.0, &colors[2]))?
        .label(MEDIAN_LABEL)
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &colors[2]));

    chart
        .configure_series_labels()
        .position(SeriesLabelPosition::UpperRight)
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    Ok(())
}

fn draw_pctl_chart<DB: DrawingBackend>(
    area: &DrawingArea<DB, Shift>,
    title: &str,
    field: EventField,
    data: &BTreeMap<DateTime<FixedOffset>, DetailData>,
    x_range: Range<DateTime<FixedOffset>>,
    colors: &[RGBAColor],
) -> Result<(), DrawingAreaErrorKind<DB::ErrorType>> {
    let median = percentiles(50, field, data);
    let p90 = percentiles(90, field, data);
    let p99 = percentiles(99, field, data);

    let y_range = range(p99.values());

    let mut chart = ChartBuilder::on(area)
        .x_label_area_size(30)
        .margin_left(20)
        .margin_right(20)
        .set_label_area_size(LabelAreaPosition::Left, 70)
        .caption(title, ("sans-serif", 30))
        .build_cartesian_2d(x_range, y_range)?;

    chart
        .configure_mesh()
        .disable_mesh()
        .x_labels(5)
        .x_label_formatter(&|t| t.time().to_string())
        .y_labels(10)
        .y_desc("Duration (sec)")
        .draw()?;

    chart
        .draw_series(AreaSeries::new(p99.into_iter(), 0.0, &colors[0]))?
        .label(P99_LABEL)
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &colors[0]));

    chart
        .draw_series(AreaSeries::new(p90.into_iter(), 0.0, &colors[1]))?
        .label(P90_LABEL)
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &colors[1]));

    chart
        .draw_series(AreaSeries::new(median.into_iter(), 0.0, &colors[2]))?
        .label(MEDIAN_LABEL)
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &colors[2]));

    chart
        .configure_series_labels()
        .position(SeriesLabelPosition::UpperRight)
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    Ok(())
}

fn draw_rps_chart<DB: DrawingBackend>(
    area: &DrawingArea<DB, Shift>,
    title: &str,
    data: &BTreeMap<DateTime<FixedOffset>, DetailData>,
    x_range: Range<DateTime<FixedOffset>>,
    interval: Interval,
) -> Result<(), DrawingAreaErrorKind<DB::ErrorType>> {
    let cts: BTreeMap<DateTime<FixedOffset>, f64> = data
        .par_iter()
        .map(|(idx, vals)| (*idx, vals.len() as f64 / interval.seconds() as f64))
        .collect();
    let y_range = range(cts.values());

    let mut chart = ChartBuilder::on(area)
        .x_label_area_size(30)
        .margin_left(20)
        .margin_right(20)
        .set_label_area_size(LabelAreaPosition::Left, 70)
        .caption(title, ("sans-serif", 30))
        .build_cartesian_2d(x_range, y_range)?;

    chart
        .configure_mesh()
        .disable_mesh()
        .x_labels(5)
        .x_label_formatter(&|t| t.time().to_string())
        .y_labels(10)
        .y_desc("Requests per Second")
        .draw()?;

    chart.draw_series(LineSeries::new(cts.into_iter(), &BLACK))?;

    Ok(())
}

fn draw_cpu_chart<DB: DrawingBackend>(
    area: &DrawingArea<DB, Shift>,
    title: &str,
    data: &BTreeMap<DateTime<FixedOffset>, DetailData>,
    x_range: Range<DateTime<FixedOffset>>,
) -> Result<(), DrawingAreaErrorKind<DB::ErrorType>> {
    let cpu_s: BTreeMap<DateTime<FixedOffset>, f64> = data
        .par_iter()
        .map(|(idx, vals)| (*idx, vals.cpu_s.iter().sum()))
        .collect();
    let y_range = range(cpu_s.values());

    let mut chart = ChartBuilder::on(area)
        .x_label_area_size(30)
        .margin_left(20)
        .margin_right(20)
        .set_label_area_size(LabelAreaPosition::Left, 70)
        .caption(title, ("sans-serif", 30))
        .build_cartesian_2d(x_range, y_range)?;

    chart
        .configure_mesh()
        .disable_mesh()
        .x_labels(5)
        .x_label_formatter(&|t| t.time().to_string())
        .y_labels(10)
        .y_desc("CPU Seconds")
        .draw()?;

    chart.draw_series(LineSeries::new(cpu_s.into_iter(), &BLACK))?;

    Ok(())
}

fn draw_fail_chart<DB: DrawingBackend>(
    area: &DrawingArea<DB, Shift>,
    title: &str,
    data: &BTreeMap<DateTime<FixedOffset>, DetailData>,
    x_range: Range<DateTime<FixedOffset>>,
) -> Result<(), DrawingAreaErrorKind<DB::ErrorType>> {
    let fails: BTreeMap<DateTime<FixedOffset>, f64> = data
        .par_iter()
        .map(|(idx, vals)| (*idx, (vals.fails as f64 / vals.len() as f64) * 100.0))
        .collect();
    let y_range = 0.0..100.0000001; // Allow up to 100% failure

    let mut chart = ChartBuilder::on(area)
        .x_label_area_size(30)
        .margin_left(20)
        .margin_right(20)
        .set_label_area_size(LabelAreaPosition::Left, 70)
        .caption(title, ("sans-serif", 30))
        .build_cartesian_2d(x_range, y_range)?;

    chart
        .configure_mesh()
        .disable_mesh()
        .x_labels(5)
        .x_label_formatter(&|t| t.time().to_string())
        .y_labels(10)
        .y_desc("% Failure")
        .draw()?;

    chart.draw_series(LineSeries::new(fails.into_iter(), &RED))?;

    Ok(())
}

// Combine data from each controller/route/method into a single entry
fn coalesce_data(chunks: &DetailTimeChunks) -> BTreeMap<DateTime<FixedOffset>, DetailData> {
    let seconds = chunks.seconds;
    let start_time = chunks.start_time;

    let mut sorted_times: Vec<_> = chunks.data.iter().collect();
    sorted_times.par_sort_by(|x, y| x.0.cmp(y.0));
    sorted_times
        .into_par_iter()
        .map(|(idx, data_map)| {
            let offset = seconds * *idx as i64;
            let time = start_time + Duration::seconds(offset);

            let mut data = data_map
                .data
                .values()
                .fold(DetailData::new(), |mut acc, d| {
                    acc.extend(d);
                    acc
                });

            data.sort();
            (time, data)
        })
        .collect()
}

fn percentiles(
    perc: u32,
    field: EventField,
    chunks: &BTreeMap<DateTime<FixedOffset>, DetailData>,
) -> BTreeMap<DateTime<FixedOffset>, f64> {
    chunks
        .par_iter()
        .map(|(idx, vals)| {
            let pctile = match field {
                EventField::Duration => util::percentile(&vals.dur, perc),
                EventField::Db => util::percentile(&vals.db, perc),
                EventField::Gitaly => util::percentile(&vals.gitaly, perc),
                EventField::Rugged => util::percentile(&vals.rugged, perc),
                EventField::Redis => util::percentile(&vals.redis, perc),
                EventField::Queue => util::percentile(&vals.queue, perc),
                EventField::CpuS => util::percentile(&vals.cpu_s, perc),
                _ => unreachable!(),
            };
            (*idx, pctile)
        })
        .collect()
}

fn range<'a>(values: impl IntoIterator<Item = &'a f64>) -> Range<f64> {
    let max = *values
        .into_iter()
        .max_by(|x, y| x.partial_cmp(y).expect("Unexpected NaN duration"))
        .unwrap_or(&0.0);

    // Handle ranges composed entirely of zeros
    if max == 0.0 {
        return 0.0..1.0;
    }

    0.0..max
}
