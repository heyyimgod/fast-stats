use crate::field::StatField;
use crate::util;
use crate::RawData;

use chrono::Duration;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Clone, Copy, Eq, PartialEq, Serialize, Deserialize, Debug)]
pub enum StatsType {
    Api,
    Gitaly,
    Prod,
    Shell,
    Sidekiq,
}

#[derive(Clone, Copy, Debug)]
pub enum PrimField {
    Blank,
    Print,
}

const BLANK_PRIM: &str = "";

impl StatsType {
    pub fn header_title(self) -> &'static str {
        use StatsType::*;
        match &self {
            Api => "ROUTE",
            Gitaly => "METHOD",
            Prod => "CONTROLLER",
            Shell => "ENDPOINT",
            Sidekiq => "WORKER",
        }
    }
}

impl fmt::Display for StatsType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use StatsType::*;
        match self {
            Api => "api_json".fmt(f),
            Gitaly => "gitaly".fmt(f),
            Prod => "production_json".fmt(f),
            Shell => "gitlab_shell".fmt(f),
            Sidekiq => "sidekiq".fmt(f),
        }
    }
}

#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct Stats {
    pub prim_field: String,
    pub count: usize,
    pub rps: f64,
    pub p99: f64,
    pub p95: f64,
    pub median: f64,
    pub max: f64,
    pub min: f64,
    pub score: f64,
    pub perc_failed: f64,
}

impl Stats {
    pub fn from_raw_data(prim_field: String, mut raw_data: RawData, dur: Duration) -> Stats {
        raw_data.data.par_sort_unstable_by(|x, y| {
            (x).partial_cmp(y)
                .expect("Invalid comparison when sorting durations")
        });

        let count = raw_data.data.len();
        let rps = if dur.num_seconds() == 0 {
            f64::NAN
        } else {
            count as f64 / dur.num_seconds() as f64
        };
        let p99 = util::percentile(&raw_data.data, 99);
        let p95 = util::percentile(&raw_data.data, 95);
        let median = util::percentile(&raw_data.data, 50);
        let max = *raw_data.data.last().unwrap_or(&0.0);
        let min = *raw_data.data.first().unwrap_or(&0.0);
        let score = p99 * count as f64;
        let perc_failed = (raw_data.fails as f64 / count as f64) * 100.0;

        Stats {
            prim_field,
            count,
            rps,
            p99,
            p95,
            median,
            max,
            min,
            score,
            perc_failed,
        }
    }

    pub fn csv(&self, fields: &[StatField]) -> String {
        let mut output = self.prim_field.to_string();

        for field in fields {
            output += &format!(
                ",{:.precision$}",
                self.val(field),
                precision = field.precision()
            );
        }

        output
    }

    pub fn md(&self, fields: &[StatField]) -> String {
        let mut output = format!("|{}", self.prim_field);

        for field in fields {
            output += &format!(
                "|{:.precision$}",
                self.val(field),
                precision = field.precision()
            );
        }

        format!("{}|", output)
    }

    pub fn text(&self, print_prim: PrimField, fields: &[StatField], lens: &[usize]) -> String {
        let mut output = format!("{:<len$}", self.prim_str(print_prim), len = lens[0]);

        for field in fields {
            output += &format!(
                "  {:width$.precision$}",
                self.val(field),
                width = field.len(lens),
                precision = field.precision()
            );
        }

        output
    }

    #[inline]
    fn val(&self, field: &StatField) -> f64 {
        match field {
            StatField::Count => self.count as f64,
            StatField::Rps => self.rps,
            StatField::P99 => self.p99,
            StatField::P95 => self.p95,
            StatField::Median => self.median,
            StatField::Max => self.max,
            StatField::Min => self.min,
            StatField::Score => self.score,
            StatField::Fail => self.perc_failed,
        }
    }

    fn prim_str(&self, prim: PrimField) -> &str {
        match prim {
            PrimField::Blank => BLANK_PRIM,
            PrimField::Print => &self.prim_field,
        }
    }
}
