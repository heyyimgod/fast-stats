# fast-stats

__Binaries can be downloaded from the [Releases page](https://gitlab.com/gitlab-com/support/toolbox/fast-stats/-/releases)__

A tool with minimal memory use to quickly create and compare performance statistics from and between GitLab logs.
Output can be printed as text, csv, or markdown and sorted by any field. Simple performance graphs can
be generated using the `plot` subcommand (not included in pre-built Linux binaries).

This is very similar to [json_stats](https://gitlab.com/gitlab-com/support/toolbox/json_stats), but faster and with the ability to compare files.
For comparison, on a 2021 M1 Max MacBook Pro `fast-stats` processes a 1.3 GB file in 280ms vs 6+ minutes with `json_stats`.

## When to use it

`fast-stats` is intended as a quick-and-dirty way to extract performance information within the constraints faced by
GitLab Support. If you have access tools like Kibana, Splunk, or Grafana, use them! They are much more capable than
`fast-stats`, which focuses on making the most of the data customers can easily provide to Support.

## Supported Log Types and Formats
   * production_json
   * api_json
   * gitaly
      * unstructured
      * JSON
   * sidekiq
      * unstructured
      * JSON

## Subcommands:
   * [top](#top)
   * [errors](#errors)
   * [plot](#plot)

## Field Definitions

   * `COUNT` -- Number of requests
   * `RPS` -- Requests per second
   * `P99` -- 99th percentile request duration
   * `P95` -- 95th percentile request duration
   * `MEDIAN` -- Median request duration
   * `MAX` -- Longest request duration
   * `MIN` -- Shortest request duration
   * `% FAIL` -- Percentage of requests that failed.  For `api_json` and `prod_json` this is defined as a status >= 500.
   * `SCORE` -- `COUNT` * `P99`

## Examples

Stats for a single file:

```
METHOD                       COUNT     RPS    P99_ms    P95_ms   MEDIAN_ms    MAX_ms    MIN_ms       SCORE     %FAIL
FindAllTags                     31    0.02  540180.2  531772.9     12356.7  543566.1     545.9  16745586.9     29.03
ListCommitsByOid                50    0.02  153146.8      14.9         6.1  300269.9       0.0   7657340.5      6.00
GetCommitSignatures              8    0.00  301148.9  300683.2       930.3  301265.4      45.3   2409191.5     25.00
FetchSourceBranch                3    0.00  513023.1  471603.9      5637.8  523377.9    4537.9   1539069.4     33.33
FindLicense                      2    0.00  299999.6  299910.4    298906.9  300021.9  297791.9    599999.3    100.00
```

In this example, `FindAllTags` stands out as the slowest method while `FindLicense` fails most often.
Both are good candidates for further investigation.

A `--compare` between files using the `--print-fields` flag for more compact output:

```
FILE                CONTROLLER                                                   COUNT    P99_ms     %FAIL
prod_json.log       Projects::MergeRequestsController#ci_environments_status      4911     620.8      0.00
other_prod_json.log                                                                104   59983.3      3.85
ratio                                                                            0.02x    96.63x      infx

prod_json.log       Explore::ProjectsController#index                                4    1653.9      0.00
other_prod_json.log                                                                206    1339.2      0.00
ratio                                                                           51.50x     0.81x     0.00x

prod_json.log       Projects::JobsController#show                                 3564    1052.7      0.00
other_prod_json.log                                                                 47    2228.5      0.00
ratio                                                                            0.01x     2.12x     0.00x
```

## Usage

After downloading a [binary release](https://gitlab.com/gitlab-com/support/toolbox/fast-stats/-/releases)
and making it executable (`chmod +x fast-stats`), you can run it as follows
(either added to your `PATH` or prepended with `./`):

```
fast-stats [FLAGS] [OPTIONS] <FILE> [SUBCOMMAND]
```

### Args

#### \<FILE>

Specify file to process. If this is omitted or `-` is passed then stdin will be read.

```
$ fast-stats FILE

CONTROLLER                                                   COUNT     RPS    P99_ms    P95_ms   MEDIAN_ms    MAX_ms    MIN_ms      SCORE     %FAIL
Projects::MergeRequestsController#ci_environments_status       104    0.02   59983.3   44070.7     18591.1   59984.3      25.2  6238267.6      3.85
Explore::ProjectsController#index                              206    0.03    1339.2    1127.8       976.8    3911.2     761.8   275865.1      0.00
Projects::MergeRequests::ContentController#widget              221    0.04     724.8     573.0       335.4    1132.3     113.3   160187.9      0.00
```

### Flags

#### --color-output / -C

Force text output to be in color. By default `fast-stats` only outputs colored
text if stdout is a terminal.

### Options

#### --bench / -b \<BENCH_VERSION>

Compare performance against GitLab.com. Versions 12.0+ available.

```
$ fast-stats --bench 13.6 FILE

FILE                CONTROLLER                                                   COUNT     RPS    P99_ms    P95_ms   MEDIAN_ms    MAX_ms    MIN_ms      SCORE     %FAIL
13.7                Projects::MergeRequestsController#ci_environments_status      1626   33.18     258.2     153.7        44.1    1092.4      11.3   419869.8      0.00
new_dur_s_prod.log                                                                 104    0.02   59983.3   44070.7     18591.1   59984.3      25.2  6238267.6      3.85
ratio                                                                            0.06x   0.00x   232.30x   286.68x     421.47x    54.91x     2.22x     14.86x      infx

13.7                Explore::ProjectsController#index                               45    0.92    1162.9     462.5       210.2    1679.8     182.7    52330.3      0.00
new_dur_s_prod.log                                                                 206    0.03    1339.2    1127.8       976.8    3911.2     761.8   275865.1      0.00
ratio                                                                            4.58x   0.04x     1.15x     2.44x       4.65x     2.33x     4.17x      5.27x     0.00x

13.7                Projects::MergeRequests::ContentController#widget             6283  128.22     412.7     268.0       137.0    1370.8       7.8  2593217.8      0.00
new_dur_s_prod.log                                                                 221    0.04     724.8     573.0       335.4    1132.3     113.3   160187.9      0.00
ratio                                                                            0.04x   0.00x     1.76x     2.14x       2.45x     0.83x    14.60x      0.06x     0.00x
```

#### --compare / -c \<COMPARE_FILE>

Two files of the same type can be compared, with ratio of one to the other listed.

**NOTE**: The logs do NOT need to be the same format, just the same type.  For example, an unstructured Gitaly log can be compared to a JSON gitaly log.

```
$ fast-stats --compare gitaly-slow  gitaly-normal

FILE             METHOD                COUNT     RPS    P99_ms    P95_ms   MEDIAN_ms    MAX_ms    MIN_ms       SCORE     %FAIL
gitaly-normal    FindAllTags             668    0.42     151.2     102.9        21.7     240.0       9.3    100998.4      0.00
gitaly-slow                               31    0.02  540180.2  531772.9     12356.7  543566.1     545.9  16745586.9     29.03
ratio                                  0.05x   0.04x  3572.73x  5167.24x     570.41x  2264.42x    58.68x     165.80x      infx

gitaly-normal    FetchSourceBranch        10    0.01    1809.7    1157.4        88.5    1972.8      74.1     18097.4      0.00
gitaly-slow                                3    0.00  513023.1  471603.9      5637.8  523377.9    4537.9   1539069.4     33.33
ratio                                  0.30x   0.23x   283.48x   407.47x      63.72x   265.29x    61.22x      85.04x      infx

gitaly-normal    FindAllTagNames         226    0.14      25.2      16.8         9.9      36.7       8.5      5687.6      0.00
gitaly-slow                               22    0.01    2589.6      62.8        16.0    3261.2       2.2     56970.3      0.00
ratio                                  0.10x   0.08x   102.90x     3.74x       1.61x    88.95x     0.26x      10.02x     0.00x
```

#### --format / -f \<FORMAT>

Determine print format, defaults to text.

Available formats:
   * text
   * csv
   * json
   * md
   
```
$ fast-stats --format md FILE
```

|CONTROLLER|COUNT|RPS|P99_ms|P95_ms|MEDIAN_ms|MAX_ms|MIN_ms|SCORE|%FAIL|
|--|--|--|--|--|--|--|--|--|--|
|Projects::MergeRequestsController#ci_environments_status|104|0.02|59983.3|44070.7|18591.1|59984.3|25.2|6238267.6|3.85|
|Explore::ProjectsController#index|206|0.03|1339.2|1127.8|976.8|3911.2|761.8|275865.1|0.00|
|Projects::MergeRequests::ContentController#widget|221|0.04|724.8|573.0|335.4|1132.3|113.3|160187.9|0.00|

#### --interval / -i \<INTERVAL_LEN>

Split results into INTERVAL_LEN time slices. INTERVAL_LEN must be in `\<LEN>[h|m|s]` format, where `\<LEN>` is
a positive integer.

Examples of valid syntax:

- `1h`
- `10m`
- `30s`

```
$ fast-stats --interval 1h --limit 3 --print-fields count,rps,p99 FILE

** Splitting input into 1 hour increments **

Start time: 2020-12-11 00:11:34.397
End time:   2020-12-11 01:56:09.791


   >>> 2020-12-11 00:11:34.397 <<<

CONTROLLER                                                   COUNT     RPS    P99_ms
Projects::MergeRequestsController#ci_environments_status        51    0.01   34602.6
Explore::ProjectsController#index                              119    0.03    1157.7
Projects::MergeRequests::ContentController#widget              116    0.03     820.8

   >>> 2020-12-11 01:11:34.397 <<<

CONTROLLER                                                   COUNT     RPS    P99_ms
Projects::MergeRequestsController#ci_environments_status        53    0.02   59983.9
Explore::ProjectsController#index                               87    0.03    1349.2
Projects::JobsController#show                                   41    0.02    2232.9
```

#### --limit / -l \<LIMIT_CT>

The number of rows / comparisons to print. Defaults to full results.

```
$ fast-stats --limit 2 FILE

CONTROLLER                                                   COUNT     RPS    P99_ms    P95_ms   MEDIAN_ms    MAX_ms    MIN_ms      SCORE     %FAIL
Projects::MergeRequestsController#ci_environments_status       104    0.02   59983.3   44070.7     18591.1   59984.3      25.2  6238267.6      3.85
Explore::ProjectsController#index                              206    0.03    1339.2    1127.8       976.8    3911.2     761.8   275865.1      0.00
```

#### --print-fields / -p \<PRINT_FIELDS>

Print only the fields listed in `\<PRINT_FIELDS>` in the order provided.

The primary column, controller/route/method/class will always print.

Available fields to print:
   * count
   * fail
   * max
   * median
   * min
   * p95
   * p99
   * rps
   * score

```
$ fast-stats --print-fields fail,median,count FILE

CONTROLLER                                                   %FAIL   MEDIAN_ms     COUNT
Projects::MergeRequestsController#ci_environments_status      3.85     18591.1       104
Explore::ProjectsController#index                             0.00       976.8       206
Projects::MergeRequests::ContentController#widget             0.00       335.4       221
```

#### --sort / -s \<SORT_BY>

Which field to sort results by, defaults to SCORE. Sort is always descending.

Available fields to sort by:
   * count
   * fail
   * max
   * median
   * min
   * p95
   * p99
   * rps
   * score

```
$ fast-stats --sort fail FILE

METHOD                           COUNT     RPS         P99         P95      MEDIAN         MAX         MIN       SCORE      %FAIL
Cleanup                             24    0.02     67.81ms     40.36ms     25.59ms     75.87ms     23.57ms     1627.56     16.67%
ListConflictFiles                   10    0.01    179.87ms    145.98ms     35.68ms    188.34ms     12.69ms     1798.67     10.00%
FetchSourceBranch                   10    0.01   1809.74ms   1157.41ms     88.48ms   1972.82ms     74.13ms    18097.39      0.00%
```

#### --search-for / -S \<SEARCH_FOR>

Perform a case-insensitive search of controller/route/method/class for `\<SEARCH_FOR>`.

No other fields in the logs are queried, so this cannot be used to search for a specific project.


```
$ fast-stats --search-for projects FILE

CONTROLLER                                                   COUNT     RPS    P99_ms    P95_ms   MEDIAN_ms    MAX_ms    MIN_ms      SCORE     %FAIL
Projects::MergeRequestsController#ci_environments_status       104    0.02   59983.3   44070.7     18591.1   59984.3      25.2  6238267.6      3.85
Explore::ProjectsController#index                              206    0.03    1339.2    1127.8       976.8    3911.2     761.8   275865.1      0.00
Projects::MergeRequests::ContentController#widget              221    0.04     724.8     573.0       335.4    1132.3     113.3   160187.9      0.00
```

#### --type / -t \<LOG_TYPE>

`fast-stats` will attempt to automatically determine the type of log files being passed based on their first line.
However, in cases where this is not valid (line partially cut off, stack trace in Sidekiq, etc.) you can override 
the autodetection and manually specify the type of log.

This argument will be applied to both \<FILE> and \<COMPARE_FILE> when using `--compare`

Available types:
   * api_json
   * gitaly
   * gitaly_json
   * production_json
   * sidekiq
   * sidekiq_json

##### --verbose / -v

Prints the component details of the maximum, P99, P95, and median events by total duration for each entry.
This allows easy comparison of what the limiting factor is for the slowest requests.

Actions with a smaller number of events may not have an event that corresponds with the exact percentile
checked. In these cases we show the closest event rounded up and display the actual percentile of that
event. If there are not enough events in a category to find events for each target percentiles, e.g. the
99th and 95th percentile are the same item, then the duplicate items are omitted.

Not available when printing in markdown or csv format.

```
fast-stats --limit 2 --verbose FILE
ROUTE                                          COUNT     RPS    P99_ms    P95_ms   MEDIAN_ms    MAX_ms    MIN_ms     SCORE     %FAIL
/api/:version/projects                            31    0.00   15426.4   14694.4     11865.7   15591.9     617.0  478218.6      0.00
    MAX dur: 15.59s  db:  0.09s  redis:  0.15s  gitaly:  8.00s  rugged:  5.12s  cpu:  0.00s -- 2020-12-11T00:48:34.667Z  LwtM3BjVSJa
    P97 dur: 15.04s  db:  0.11s  redis:  0.18s  gitaly:  7.42s  rugged:  5.56s  cpu:  0.00s -- 2020-12-11T00:48:18.950Z  Zg48xm1lsO3
    P50 dur: 11.87s  db:  0.08s  redis:  0.50s  gitaly:  5.82s  rugged:  3.57s  cpu:  0.00s -- 2020-12-11T01:01:49.725Z  iptvGokGCm5

/api/:version/projects/:id/repository/tree      1263    0.20     365.8     268.7       155.4    1516.4      64.9  461980.4      0.00
    MAX dur:  1.52s  db:  0.01s  redis:  0.00s  gitaly:  0.00s  rugged:  1.49s  cpu:  0.00s -- 2020-12-11T01:10:18.982Z  FwmXcXy9rY9
    P99 dur:  0.37s  db:  0.01s  redis:  0.00s  gitaly:  0.17s  rugged:  0.16s  cpu:  0.00s -- 2020-12-11T00:35:04.366Z  TAMxviqeGf5
    P95 dur:  0.27s  db:  0.01s  redis:  0.00s  gitaly:  0.00s  rugged:  0.24s  cpu:  0.00s -- 2020-12-11T01:14:23.955Z  XuA4a0rgY69
    P50 dur:  0.16s  db:  0.02s  redis:  0.00s  gitaly:  0.00s  rugged:  0.11s  cpu:  0.00s -- 2020-12-11T01:32:26.977Z  2ssOas2v5t5
```

### Subcommands

#### top

Print a summary of top items in a category such as project, user, or path by total duration
consumed by requests for that item. The types of comparison, e.g. path, project, will vary
between log files, with more options being available with later versions of GitLab.

Any columns not present in the logs will be omitted from the output, so older logs will not
contain details like redis or cpu time.

Categories:

   * Path: prod_json, api_json
   * Project: proj_json (13.0+), api_json (13.5+), gitaly, sidekiq (13.5+)
   * User: prod_json, api_json (13.5+), sidekiq (13.5+)
   * Client: gitaly

Usage: fast-stats top [OPTIONS] [FILE_NAME]

##### Options

###### --display / -d \<DISPLAY_FMT>

Show percentage of totals, actual durations, or both. Defaults to both.

Available formats:
   * value
   * perc
   * both

```
$ fast-stats top --limit 2 FILE

Totals

COUNT:   1846
DUR:     39m01.1s
DB:      32m29.0s
REDIS:   7.4s
GITALY:  20.1s
RUGGED:  3m18.2s
CPU:     3m02.0s
FAIL_CT: 4


Top 2 Paths by Duration

PATH                                                                                  COUNT                 DUR                  DB           REDIS          GITALY          RUGGED              CPU      FAIL_CT
/group/project/-/merge_requests/121/ci_environments_status                      13 /  0.70%    5m36.0s / 14.35%    5m35.7s / 17.23%   0.0s /  0.17%   0.0s /  0.00%   0.0s /  0.00%    0.5s /  0.26%   1 / 25.00%
/other_group/a_project/-/merge_requests/48/ci_environments_status                9 /  0.49%    5m02.8s / 12.93%    5m02.5s / 15.52%   0.0s /  0.04%   0.0s /  0.00%   0.0s /  0.00%    0.4s /  0.23%   1 / 25.00%


Top 2 Projects by Duration

PROJECT                                                                                COUNT                 DUR                  DB           REDIS          GITALY          RUGGED              CPU      FAIL_CT
a_group/project_one                                                              52 /  2.82%    7m55.5s / 20.31%    7m51.5s / 24.19%   0.1s /  1.37%   0.0s /  0.00%   2.1s /  1.06%    2.7s /  1.48%   1 / 25.00%
b_group/project_two                                                              53 /  2.87%    6m24.5s / 16.42%    6m15.7s / 19.27%   0.1s /  1.62%   0.0s /  0.00%   3.3s /  1.66%    6.7s /  3.65%   0 /  0.00%


Top 2 Users by Duration

USER                                                                                   COUNT                 DUR                  DB           REDIS          GITALY          RUGGED              CPU      FAIL_CT
user1                                                                           164 /  8.88%   14m28.1s / 37.08%   14m09.3s / 43.57%   0.4s /  4.77%   0.1s /  0.74%   8.6s /  4.36%   12.9s /  7.08%   1 / 25.00%
user2                                                                            40 /  2.17%    7m00.7s / 17.97%    6m57.4s / 21.42%   0.1s /  0.90%   0.1s /  0.27%   1.5s /  0.77%    2.3s /  1.29%   1 / 25.00%
```

#### --format / -f \<FORMAT>

Determine print format, defaults to text.

Available formats:
   * text
   * json

#### --interval / -i \<INTERVAL_LEN>

Split results into INTERVAL_LEN time slices. INTERVAL_LEN must be in `\<LEN>[h|m|s]` format, where `\<LEN>` is
a positive integer.

Examples of valid syntax:

- `1h`
- `10m`
- `30s`

###### --limit / -l \<LIMIT_CT>

The number of rows to print. Defaults to 10.

```
$ fast-stats top --interval 5m --limit 2 FILE

** Splitting input into 5 minute increments **

Start time: 2020-12-11 00:11:34.397
End time:   2020-12-11 01:56:09.791

   >>> 2020-12-11 00:11:34.397 <<<

Totals

COUNT:   99
DUR:     1m09.4s
DB:      50.4s
REDIS:   0.3s
GITALY:  2.6s
RUGGED:  9.6s
CPU:     7.7s


Top 2 Paths by Duration

PATH                                                                                  COUNT                 DUR                  DB           REDIS          GITALY          RUGGED              CPU      FAIL_CT
/group/project/-/merge_requests/121/ci_environments_status                      13 /  0.70%    5m36.0s / 14.35%    5m35.7s / 17.23%   0.0s /  0.17%   0.0s /  0.00%   0.0s /  0.00%    0.5s /  0.26%   1 / 25.00%
/other_group/a_project/-/merge_requests/48/ci_environments_status                9 /  0.49%    5m02.8s / 12.93%    5m02.5s / 15.52%   0.0s /  0.04%   0.0s /  0.00%   0.0s /  0.00%    0.4s /  0.23%   1 / 25.00%

...

   >>> 2020-12-11 00:16:34.397 <<<

Totals

COUNT:   99
DUR:     1m09.4s
DB:      50.4s
REDIS:   0.3s
GITALY:  2.6s
RUGGED:  9.6s
CPU:     7.7s


Top 2 Paths by Duration

...
```

###### --sort / -s \<SORT_BY>

Which field to sort results by, defaults to DUR. Sort is always descending.

Available fields to sort by:
   * count
   * duration
   * db
   * redis
   * gitaly
   * rugged
   * queue
   * cpu
   * fail

```
fast-stats top --sort gitaly --display value FILE

Top 2 Paths by Gitaly Duration

PATH                                                         COUNT      DUR       DB    REDIS   GITALY   RUGGED      CPU  FAIL_CT
/group1/project3/-/refs/master/logs_tree                     0.11%    0.14%    0.00%    0.15%   14.15%    0.20%    0.11%    0.00%
/group2/project/-/merge_requests/4/widget.json               0.90%    0.21%    0.03%    0.49%    7.09%    3.11%    0.66%    0.00%


Top 2 Projects by Gitaly Duration

PROJECT                                                      COUNT      DUR       DB    REDIS   GITALY   RUGGED      CPU  FAIL_CT
/group1/project3                                             0.67%    0.26%    0.01%    0.48%   23.76%    0.77%    0.48%    0.00%
/group2/project4                                            11.65%    4.11%    1.21%   65.76%   17.15%    4.53%   45.33%    0.00%


Top 2 Users by Gitaly Duration

USER                                                         COUNT      DUR       DB    REDIS   GITALY   RUGGED      CPU  FAIL_CT
user3                                                        2.35%    0.50%    0.04%    1.21%   36.80%    3.22%    1.23%    0.00%
user4                                                       18.37%    6.29%    3.22%   67.47%   23.31%    8.09%   48.48%    0.00%
```

#### errors

Print a list of all errors, sorted by count. If one ore more backtraces are associated the error
the first three lines of each will be printed.

Any columns not present in the logs will be omitted from the output, so older logs will not
contain details like redis or cpu time.

Usage: fast-stats top [FLAGS] [FILE_NAME]

```
$ fast-stats errors FILE
┌─────────────────────────────────────────────────────┐
│Error: Gitlab::Git::CommandError 4:Deadline Exceeded.│
├─────────┬───────────────────────────────────────────┘
│Count: 10│
├─────────┴─────────────────────────────────────────────────────────────────────┐
│Backtrace:                                                                     │
│  lib/gitlab/git/wraps_gitaly_errors.rb:13:in `rescue in wrapped_gitaly_errors'│
│  lib/gitlab/git/wraps_gitaly_errors.rb:6:in `wrapped_gitaly_errors'           │
│  lib/gitlab/git/repository.rb:771:in `write_ref'                              │
├───────────────────────────────────────────────────────────────────────────────┤
│  lib/gitlab/git/wraps_gitaly_errors.rb:13:in `rescue in wrapped_gitaly_errors'│
│  lib/gitlab/git/wraps_gitaly_errors.rb:6:in `wrapped_gitaly_errors'           │
│  lib/gitlab/git/commit.rb:113:in `between'                                    │
├───────────────────────────────────────────────────────────────────────────────┴─────────────────────────────────────────────┐
│Events:                                                                                                                      │
│  TIME                      CORR_ID                           ACTION                                USER      PROJECT        │
│  2020-12-15T05:32:23.327Z  GirFqA7nE99                       Deployments::UpdateEnvironmentWorker  user1     group2/project8│
│  2020-12-15T05:32:50.741Z  QH1o6KC7jE5                       Deployments::UpdateEnvironmentWorker  myuser    group/project  │
│  2020-12-15T05:33:33.961Z  P2cTptLxsN7                       Deployments::UpdateEnvironmentWorker  user1     group2/project8│
│  2020-12-15T05:33:34.092Z  ifhQmC9RnG7                       Deployments::LinkMergeRequestWorker   user1     group2/project8│
│  2020-12-15T05:33:34.095Z  QH1o6KC7jE5                       Deployments::LinkMergeRequestWorker   myuser    group/project  │
│  2020-12-15T05:34:20.020Z  jmjTNsGMyt3                       Deployments::UpdateEnvironmentWorker  user1     group2/project8│
│  2020-12-15T05:34:39.476Z  55b69ae5396624d01d3ae3af778ae7bc  Deployments::UpdateEnvironmentWorker  user1     group2/project8│
│  2020-12-15T05:38:42.066Z  atnrfyPJU95                       Deployments::LinkMergeRequestWorker   myuser    group/project  │
│  2020-12-15T05:38:52.055Z  atnrfyPJU95                       Deployments::LinkMergeRequestWorker   myuser    group/project  │
│  2020-12-15T06:30:41.704Z  wbmGxG9Cg6                        Deployments::LinkMergeRequestWorker   user1     group2/project8│
└─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
```

##### Flags

#### --format / -f \<FORMAT>

Determine print format, defaults to text.

Available formats:
   * text
   * json

###### --no-border / -b

Don't print a border around each error entry. The border mades it easier to distinguish between
entries but may not paste well into documents. Use this flag to disable it.

```
$ fast-stats errors --no-border FILE

Error: Rack::Timeout::RequestTimeoutException Request ran for longer than 60000ms
Count: 4
Backtrace:
  app/models/environment_status.rb:103:in `map'
  app/models/environment_status.rb:103:in `build_environments_status'
  app/models/environment_status.rb:14:in `for_merge_request'
Events:
  TIME                      CORR_ID       ACTION                                                    USER          PROJECT
  2020-12-11T01:18:50.248Z  YxqOfH8vfd9   Projects::MergeRequestsController#ci_environments_status  user1         group1/project1
  2020-12-11T01:19:09.672Z  bR78i04wGI3   Projects::MergeRequestsController#ci_environments_status  user2         group2/project2
  2020-12-11T01:35:29.744Z  bZe7787hsU7   Projects::MergeRequestsController#ci_environments_status  user3         group1/project7
  2020-12-11T01:37:08.751Z  YOTxaq1hq74   Projects::MergeRequestsController#ci_environments_status  user4         group3/project5
```

###### --color-output / -C

Force text output to be in color. By default `fast-stats` only outputs colored
text if stdout is a terminal.

#### plot

Output a 2560x1440 PNG with graphs of the following values:
   * Request Duration
   * Request Rate
   * Queue Duration
   * DB Duration
   * Redis Duration
   * Gitaly Duration
   * Rugged Duration
   * CPU Time
   * Failure Rate

![plot_screenshot](/uploads/fa8fbe092d20e3f6c707410b57c382b5/fast_stats_plot.png)

If the data needed to create some of these graphs is not be present in the log then the corresponding
graph will be blank.

**Note**: This feature is not enabled in pre-built Linux binaries.

##### Options

###### --outfile / -o \<OUTPUT_FILE>

Name of file to write to.

## Building from Source

Minimum supported Rust version supported is 1.59

On Debian-based Linux systems the following packages are required to build with the default `plot`
feature enabled:

```sh
sudo apt install gcc make pkg-config libfreetype6-dev libfontconfig1-dev
```

To build without plot, run `cargo build --release --no-default-features`
