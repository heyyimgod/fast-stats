use std::env;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("cargo:rerun-if-changed=benchmark-data");

    let out_dir = env::var("OUT_DIR")?;

    let lz4_path = Path::new(&out_dir).join("lz4_bench.rs");
    let mut lz4_file = File::create(&lz4_path)?;

    let mut bench_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    bench_dir.push("benchmark-data");

    writeln!(&mut lz4_file, r##"["##,)?;

    for entry in bench_dir.read_dir()?.flatten() {
        let path = entry.path();

        if path.extension().map(|e| e == "lz4").unwrap_or_default() {
            if !entry.file_type()?.is_file() {
                continue;
            }

            let name: &Path = match path.file_stem() {
                Some(s) => s.as_ref(),
                None => continue,
            };

            writeln!(
                &mut lz4_file,
                r##"("{}", include_bytes!(r#"{}"#)),"##,
                name.display(),
                path.display()
            )?;
        }
    }

    writeln!(&mut lz4_file, r##"]"##,)?;

    Ok(())
}
