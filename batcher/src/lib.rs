pub use crate::batcher::Batcher;
pub use crate::line_batch::LineBatch;

mod batcher;
mod line_batch;

#[cfg(test)]
mod test {
    use super::*;

    const DEFAULT_BUF: usize = 4096;

    fn get_lines(batch_size: usize, input: &[u8]) -> LineBatch {
        let mut b = Batcher::<_, DEFAULT_BUF>::new(batch_size, input);
        b.iter().next().unwrap().unwrap()
    }

    #[test]
    fn empty_input() {
        let input = "";
        let mut b = Batcher::<_, DEFAULT_BUF>::new(1, input.as_bytes());
        let lines = b.iter().next().transpose().unwrap();

        assert_eq!(lines, None);
    }

    #[test]
    fn single_line() {
        let input = "Hello, World!\n";
        let lines = get_lines(1, input.as_bytes());

        assert_eq!(lines.offsets, [input.trim().len()]);
        assert_eq!(lines.iter().next(), Some(input.trim()));
    }

    #[test]
    fn two_lines() {
        let inputs = vec!["Hello, World!\n", "Goodbye, World!\n"];
        let input = inputs.join("");
        let lines = get_lines(2, input.as_bytes());

        assert_eq!(
            lines.offsets,
            [inputs[0].trim().len(), inputs[1].trim().len()]
        );

        let mut iter = lines.iter();
        assert_eq!(iter.next(), Some(inputs[0].trim()));
        assert_eq!(iter.next(), Some(inputs[1].trim()));
    }

    #[test]
    fn no_newline() {
        let input = "Hello, World!";
        let lines = get_lines(1, input.as_bytes());

        assert_eq!(lines.offsets, [input.len()]);
        assert_eq!(lines.iter().next(), Some(input.trim()));
    }

    #[test]
    fn two_lines_no_trailing_newline() {
        let inputs = vec!["Hello, World!\n", "Goodbye, World!\n"];
        let input = inputs.join("");
        let lines = get_lines(2, input.as_bytes());

        assert_eq!(
            lines.offsets,
            [inputs[0].trim().len(), inputs[1].trim().len()]
        );

        let mut iter = lines.iter();
        assert_eq!(iter.next(), Some(inputs[0].trim()));
        assert_eq!(iter.next(), Some(inputs[1].trim()));
    }

    #[test]
    fn n_greater_than_input_lines() {
        let inputs = vec!["Hello, World!\n", "Goodbye, World!\n"];
        let input = inputs.join("");
        let lines = get_lines(10, input.as_bytes());

        assert_eq!(
            lines.offsets,
            [inputs[0].trim().len(), inputs[1].trim().len()]
        );

        let mut iter = lines.iter();
        assert_eq!(iter.next(), Some(inputs[0].trim()));
        assert_eq!(iter.next(), Some(inputs[1].trim()));
    }

    #[test]
    fn n_greater_than_input_lines_no_trailing_newline() {
        let inputs = vec!["Hello, World!\n", "Goodbye, World!"];
        let input = inputs.join("");
        let lines = get_lines(10, input.as_bytes());

        assert_eq!(
            lines.offsets,
            [inputs[0].trim().len(), inputs[1].trim().len()]
        );

        let mut iter = lines.iter();
        assert_eq!(iter.next(), Some(inputs[0].trim()));
        assert_eq!(iter.next(), Some(inputs[1].trim()));
    }

    #[test]
    fn single_line_buf_smaller_than_line() {
        let mut input = "Hello, World!".repeat(10);
        input += "\n";
        let mut b = Batcher::<_, 16>::new(1, input.as_bytes());
        let lines = b.iter().next().unwrap().unwrap();

        assert_eq!(lines.offsets, [input.len() - 1]);
        assert_eq!(lines.iter().next(), Some(input.trim()));
    }

    #[test]
    fn two_lines_buf_smaller_than_line() {
        let inputs = vec![
            "Hello, World!".repeat(10),
            "\n".to_string(),
            "Goodbye, World!".repeat(10),
            "\n".to_string(),
        ];
        let input = inputs.join("");
        let mut b = Batcher::<_, 16>::new(2, input.as_bytes());
        let lines = b.iter().next().unwrap().unwrap();

        assert_eq!(lines.offsets, [inputs[0].len(), inputs[2].len()]);

        let mut iter = lines.iter();
        assert_eq!(iter.next(), Some(inputs[0].trim()));
        assert_eq!(iter.next(), Some(inputs[2].trim()));
    }

    #[test]
    fn three_lines_buf_smaller_than_line() {
        let inputs = vec![
            "Hello, World!".repeat(10),
            "\n".to_string(),
            "Goodbye, World!".repeat(10),
            "\n".to_string(),
            "foo bar baz qux".repeat(10),
            "\n".to_string(),
        ];
        let input = inputs.join("");
        let mut b = Batcher::<_, 16>::new(3, input.as_bytes());
        let lines = b.iter().next().unwrap().unwrap();

        assert_eq!(
            lines.offsets,
            [inputs[0].len(), inputs[2].len(), inputs[4].len()]
        );

        let mut iter = lines.iter();
        assert_eq!(iter.next(), Some(inputs[0].trim()));
        assert_eq!(iter.next(), Some(inputs[2].trim()));
        assert_eq!(iter.next(), Some(inputs[4].trim()));
    }

    #[test]
    fn three_lines_buf_smaller_than_line_no_trailing_newline() {
        let inputs = vec![
            "Hello, World!".repeat(10),
            "\n".to_string(),
            "Goodbye, World!".repeat(10),
            "\n".to_string(),
            "foo bar baz qux".repeat(10),
        ];
        let input = inputs.join("");
        let mut b = Batcher::<_, 16>::new(3, input.as_bytes());
        let lines = b.iter().next().unwrap().unwrap();

        assert_eq!(
            lines.offsets,
            [inputs[0].len(), inputs[2].len(), inputs[4].len()]
        );

        let mut iter = lines.iter();
        assert_eq!(iter.next(), Some(inputs[0].trim()));
        assert_eq!(iter.next(), Some(inputs[2].trim()));
        assert_eq!(iter.next(), Some(inputs[4].trim()));
    }

    #[test]
    fn just_newline() {
        let input = "\n";
        let lines = get_lines(1, input.as_bytes());

        assert_eq!(lines.offsets, [input.trim().len()]);
        assert_eq!(lines.iter().next(), None);
    }

    #[test]
    fn just_newline_batch_size_greater_than_lines() {
        let input = "\n";
        let lines = get_lines(10, input.as_bytes());

        assert_eq!(lines.offsets, [0]);
        assert_eq!(lines.iter().next(), None);
    }

    #[test]
    fn just_two_newlines() {
        let input = "\n\n";
        let lines = get_lines(2, input.as_bytes());

        assert_eq!(lines.offsets, [0, 0]);
        assert_eq!(lines.iter().next(), None);
    }

    #[test]
    fn leading_newline() {
        let input = "\nHello, World!\n";
        let lines = get_lines(2, input.as_bytes());

        assert_eq!(lines.offsets, [0, input.trim().len()]);

        let mut iter = lines.iter();
        assert_eq!(iter.next(), Some(input.trim()));
    }

    #[test]
    fn two_leading_newlines() {
        let input = "\n\nHello, World!\n";
        let lines = get_lines(3, input.as_bytes());

        assert_eq!(lines.offsets, [0, 0, input.trim().len()]);

        let mut iter = lines.iter();
        assert_eq!(iter.next(), Some(input.trim()));
    }

    #[test]
    fn log_snippet() {
        let input = r##"{"severity":"INFO","time":"2020-12-15T04:45:48.592Z","message":"Pushed job e25a2e09cf19e00496c8d896 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"e25a2e09cf19e00496c8d896","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:48.592Z","message":"Pushed job ff8aed0d310e4b93e4b00db3 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"ff8aed0d310e4b93e4b00db3","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:48.593Z","message":"Pushed job 94314a3f4acb94120bc04011 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"94314a3f4acb94120bc04011","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:48.627Z","message":"Pushed job 3c84e0e919c5e7a1ac7f4965 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"3c84e0e919c5e7a1ac7f4965","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:48.676Z","message":"Pushed job 3637ddfdd013e213d6107ce3 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"3637ddfdd013e213d6107ce3","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:48.789Z","message":"Pushed job 8592d80dcf30cafd61379802 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"8592d80dcf30cafd61379802","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:48.790Z","message":"Pushed job 826e2e7c1498c8a28634cc23 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"826e2e7c1498c8a28634cc23","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:48.867Z","message":"Pushed job 8f97e36ac6d2d5bd3db51584 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"8f97e36ac6d2d5bd3db51584","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:49.002Z","message":"Pushed job 7226b84f12a1d9e123ba568f back to queue queue:pipeline_hooks:pipeline_hooks","jid":"7226b84f12a1d9e123ba568f","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:49.069Z","message":"Pushed job 6a6f59f53cd7d8547ae37e6e back to queue queue:pipeline_hooks:pipeline_hooks","jid":"6a6f59f53cd7d8547ae37e6e","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:49.077Z","message":"Pushed job 3c84e0e919c5e7a1ac7f4965 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"3c84e0e919c5e7a1ac7f4965","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:49.077Z","message":"Pushed job 594afb9403abb5bc942dddbf back to queue queue:pipeline_hooks:pipeline_hooks","jid":"594afb9403abb5bc942dddbf","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:49.104Z","message":"Pushed job 29d4a406b6e9ac25440041fc back to queue queue:pipeline_hooks:pipeline_hooks","jid":"29d4a406b6e9ac25440041fc","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:49.185Z","message":"Pushed job e14a36f8eacf4b0eea1f3272 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"e14a36f8eacf4b0eea1f3272","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:49.222Z","message":"Pushed job 44b5e541e435545205727c6f back to queue queue:pipeline_processing:pipeline_process","jid":"44b5e541e435545205727c6f","queue":"queue:pipeline_processing:pipeline_process","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.223Z","class":"PipelineHooksWorker","jid":"d038f4ddb762689de12fa196","message":"Reliable Fetcher: adding dead PipelineHooksWorker job d038f4ddb762689de12fa196 to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.224Z","class":"PipelineHooksWorker","jid":"e6705ab2327c1021ab6422ba","message":"Reliable Fetcher: adding dead PipelineHooksWorker job e6705ab2327c1021ab6422ba to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.276Z","class":"PipelineHooksWorker","jid":"9bfe640c3ac5d2f2dddf656e","message":"Reliable Fetcher: adding dead PipelineHooksWorker job 9bfe640c3ac5d2f2dddf656e to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.277Z","class":"PipelineHooksWorker","jid":"588c9004c06f83fc0aff8886","message":"Reliable Fetcher: adding dead PipelineHooksWorker job 588c9004c06f83fc0aff8886 to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.277Z","class":"PipelineHooksWorker","jid":"cee7fe3e9d3ee69ef2875fb0","message":"Reliable Fetcher: adding dead PipelineHooksWorker job cee7fe3e9d3ee69ef2875fb0 to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.355Z","class":"PipelineHooksWorker","jid":"1b206275aeee8432721b5d41","message":"Reliable Fetcher: adding dead PipelineHooksWorker job 1b206275aeee8432721b5d41 to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.428Z","class":"PipelineHooksWorker","jid":"70a9e882f56e5e8f2d102cf1","message":"Reliable Fetcher: adding dead PipelineHooksWorker job 70a9e882f56e5e8f2d102cf1 to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.534Z","class":"PipelineHooksWorker","jid":"2d78c3f7c8820c8e1db1e939","message":"Reliable Fetcher: adding dead PipelineHooksWorker job 2d78c3f7c8820c8e1db1e939 to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.572Z","class":"PipelineHooksWorker","jid":"3487ae74ae86cc759aa25251","message":"Reliable Fetcher: adding dead PipelineHooksWorker job 3487ae74ae86cc759aa25251 to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.611Z","class":"PipelineHooksWorker","jid":"843c7d35fa1dd54fad7b5230","message":"Reliable Fetcher: adding dead PipelineHooksWorker job 843c7d35fa1dd54fad7b5230 to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.670Z","class":"PipelineHooksWorker","jid":"22023326ea069f06084cd8d8","message":"Reliable Fetcher: adding dead PipelineHooksWorker job 22023326ea069f06084cd8d8 to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.716Z","class":"PipelineHooksWorker","jid":"63a5f7c9806a17c21f246ccc","message":"Reliable Fetcher: adding dead PipelineHooksWorker job 63a5f7c9806a17c21f246ccc to interrupted queue","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:49.767Z","message":"Pushed job 58d1c361c6490fd306ab4ebd back to queue queue:pipeline_hooks:pipeline_hooks","jid":"58d1c361c6490fd306ab4ebd","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:49.812Z","message":"Pushed job eed8684b994d17c3b05fe856 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"eed8684b994d17c3b05fe856","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:49.859Z","message":"Pushed job ccb1084d7e35d1a52d419247 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"ccb1084d7e35d1a52d419247","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.912Z","class":"PipelineHooksWorker","jid":"77f47c904b881c37024028cb","message":"Reliable Fetcher: adding dead PipelineHooksWorker job 77f47c904b881c37024028cb to interrupted queue","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:49.948Z","class":"PipelineHooksWorker","jid":"9a7d896d772d563bf23a0406","message":"Reliable Fetcher: adding dead PipelineHooksWorker job 9a7d896d772d563bf23a0406 to interrupted queue","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:50.015Z","message":"Pushed job a604593cebe176a30534545f back to queue queue:pipeline_hooks:pipeline_hooks","jid":"a604593cebe176a30534545f","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:50.099Z","message":"Pushed job e417ce10ebf22191504e0fa4 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"e417ce10ebf22191504e0fa4","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:50.099Z","message":"Pushed job a2c732078b207f2d2a0d2271 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"a2c732078b207f2d2a0d2271","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:50.168Z","message":"Pushed job 99540d50b00dd4c1292fa59f back to queue queue:pipeline_hooks:pipeline_hooks","jid":"99540d50b00dd4c1292fa59f","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:50.204Z","message":"Pushed job e48ce31376d50a3400182d2b back to queue queue:pipeline_hooks:pipeline_hooks","jid":"e48ce31376d50a3400182d2b","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:50.245Z","message":"Pushed job 79a8640a05144b80cf60e25f back to queue queue:pipeline_hooks:pipeline_hooks","jid":"79a8640a05144b80cf60e25f","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:50.308Z","message":"Pushed job 04a5372f27251a0a6220dbd8 back to queue queue:pipeline_hooks:pipeline_hooks","jid":"04a5372f27251a0a6220dbd8","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"INFO","time":"2020-12-15T04:45:50.341Z","message":"Pushed job 2328f62248a1c38a093012bd back to queue queue:pipeline_hooks:pipeline_hooks","jid":"2328f62248a1c38a093012bd","queue":"queue:pipeline_hooks:pipeline_hooks","retry":0}
{"severity":"WARN","time":"2020-12-15T04:45:50.385Z","class":"PipelineHooksWorker","jid":"b4966cfd2c91b8823e35c7f3","message":"Reliable Fetcher: adding dead PipelineHooksWorker job b4966cfd2c91b8823e35c7f3 to interrupted queue","retry":0}"##;
        let mut batcher = Batcher::<_, DEFAULT_BUF>::new(25, input.as_bytes());

        let lines = batcher.iter().next().unwrap().unwrap();
        let out: Vec<_> = lines.iter().collect();

        assert_eq!(out.len(), 25);

        for (i, line) in input.lines().enumerate().take(25) {
            assert_eq!(out[i], line);
        }
    }
}
