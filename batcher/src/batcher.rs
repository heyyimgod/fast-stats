use super::LineBatch;

use std::cmp;
use std::io::{self, prelude::*, ErrorKind};

const DEFAULT_BUF_SIZE: usize = 1024 * 64;

#[derive(PartialEq, Debug)]
pub struct Batcher<R, const N: usize = DEFAULT_BUF_SIZE> {
    batch_size: usize,
    reader: R,
    buf: Box<[u8; N]>,
    len: usize,
    pos: usize,
    done: bool,
}

#[derive(PartialEq, Debug)]
pub struct Iter<'a, R, const N: usize> {
    batcher: &'a mut Batcher<R, N>,
}

#[derive(PartialEq, Debug)]
enum ReadStatus {
    Eof,
    Cont,
}

impl<R, const N: usize> Batcher<R, N>
where
    R: Read,
{
    pub fn new(batch_size: usize, reader: R) -> Self {
        Batcher {
            batch_size,
            reader,
            buf: Box::new([0; N]),
            len: 0,
            pos: 0,
            done: false,
        }
    }

    pub fn iter(&mut self) -> Iter<R, N> {
        Iter { batcher: self }
    }

    // Based on std::BufReader::read_until, but allows us to read batch_size lines into the buffer
    // as a single contiguous Vec<u8>
    fn read_lines(&mut self) -> io::Result<Option<LineBatch>> {
        if self.done {
            return Ok(None);
        }

        let mut out = LineBatch::new(self.batch_size);

        let mut lines = 0;
        let mut rem = 0; // Track length when line is split across multiple buffers
        let mut buf_offsets = Vec::with_capacity(self.batch_size); // Offsets found in iteration

        loop {
            if self.empty() {
                if let ReadStatus::Eof = self.fill_buf()? {
                    self.done = true;

                    match (rem > 0, !out.offsets.is_empty()) {
                        // Copy over remainder of buf and set final offset
                        (true, _) => {
                            out.add_offset(rem);
                            return Ok(Some(out));
                        }
                        // We found a newline in an earlier iteration, but no data to copy now
                        (false, true) => {
                            return Ok(Some(out));
                        }
                        // Immediate EOF
                        (false, false) => {
                            return Ok(None);
                        }
                    }
                }
            }

            // Starting pos may be in middle of buf on subsequent read_lines() calls.
            // We need to copy from that point, not the start of the buf
            let copy_start = self.pos;

            loop {
                match memchr::memchr(b'\n', self.slice_to_end()) {
                    Some(i) => {
                        lines += 1;

                        out.add_offset(rem + i);
                        rem = 0; // Remainder has been consumed
                        buf_offsets.push(i);
                        self.consume(i + 1); // Move position past newline

                        if lines >= self.batch_size {
                            let s = self.slice_to_pos(copy_start);
                            out.add_slice(s);

                            return Ok(Some(out));
                        }
                    }
                    None => {
                        // No further newlines, done with this buffer
                        self.consume_all();

                        let s = self.slice_to_pos(copy_start);
                        out.add_slice(s);

                        if !buf_offsets.is_empty() {
                            // Include buf_offsets.len() because offsets start one past newlines,
                            // skipping a byte
                            let offset_sum = buf_offsets.iter().sum::<usize>() + buf_offsets.len();

                            // We found a newline in this buffer, rem starts after last offset.
                            rem += self.len - offset_sum - copy_start;
                        } else {
                            // No newlines, rem is all uncopied bytes
                            rem += self.len - copy_start;
                        }

                        break;
                    }
                }
            }
            buf_offsets.clear();
        }
    }

    fn fill_buf(&mut self) -> io::Result<ReadStatus> {
        self.pos = 0;

        loop {
            match self.reader.read(&mut *self.buf) {
                Ok(n) if n > 0 => {
                    self.len = n;
                    return Ok(ReadStatus::Cont);
                }
                Ok(_) => {
                    return Ok(ReadStatus::Eof);
                }
                // Match behavior of std::io::Read::read_until
                Err(ref e) if e.kind() == ErrorKind::Interrupted => continue,
                Err(e) => return Err(e),
            }
        }
    }

    fn consume(&mut self, ct: usize) {
        self.pos = cmp::min(self.pos + ct, self.buf.len());
    }

    fn consume_all(&mut self) {
        self.pos = self.len;
    }

    fn slice_to_end(&self) -> &[u8] {
        &self.buf[self.pos..self.len]
    }

    fn slice_to_pos(&self, i: usize) -> &[u8] {
        &self.buf[i..self.pos]
    }

    fn empty(&self) -> bool {
        self.len == 0 || self.pos + 1 >= self.len
    }
}

impl<R: Read, const N: usize> Iterator for Iter<'_, R, N> {
    type Item = io::Result<LineBatch>;

    fn next(&mut self) -> Option<io::Result<LineBatch>> {
        self.batcher.read_lines().transpose()
    }
}
