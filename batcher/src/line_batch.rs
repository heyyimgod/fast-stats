const LINE_LEN: usize = 2048;

#[derive(PartialEq, Debug)]
pub struct LineBatch {
    pub buf: Vec<u8>,
    pub offsets: Vec<usize>,
}

#[derive(PartialEq, Debug)]
pub struct Iter<'a> {
    batch: &'a LineBatch,
    idx: usize,
    pos: usize,
}

impl LineBatch {
    pub fn new(batch_size: usize) -> Self {
        LineBatch {
            buf: Vec::with_capacity(LINE_LEN * batch_size),
            offsets: Vec::with_capacity(batch_size),
        }
    }

    pub fn add_offset(&mut self, idx: usize) {
        self.offsets.push(idx);
    }

    pub fn add_slice(&mut self, s: &[u8]) {
        self.buf.extend_from_slice(s);
    }

    pub fn iter(&self) -> Iter {
        Iter {
            batch: self,
            idx: 0,
            pos: 0,
        }
    }
}

// Assumes strings are split at newlines as with String.lines()
impl From<Vec<String>> for LineBatch {
    fn from(lines: Vec<String>) -> LineBatch {
        let mut out = LineBatch::new(lines.len());

        for line in lines {
            if line.is_empty() {
                continue;
            }

            out.buf.extend_from_slice(line.as_bytes());

            if let Some(b'\n') = line.as_bytes().last() {
                out.add_offset(line.len() - 1);
            } else {
                out.add_offset(line.len());
                out.buf.push(b'\n');
            }
        }

        out
    }
}

impl<'a> Iterator for Iter<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<&'a str> {
        if self.idx >= self.batch.offsets.len() {
            return None;
        }

        let mut i = self.batch.offsets[self.idx] + self.pos;

        // Skip empty lines
        while self.pos == i {
            self.idx += 1;
            self.pos += 1;

            if self.idx >= self.batch.offsets.len() {
                return None;
            }

            i = self.batch.offsets[self.idx] + self.pos;
        }

        let slice = &self.batch.buf[self.pos..i];

        self.pos = i + 1;
        self.idx += 1;

        // Return None if invalid UTF-8. This means we lose the remainder of
        // the batch after a line with malformed text
        std::str::from_utf8(slice).ok()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn from_vec_w_newlines() {
        let input = vec![
            String::from("Hello, world!\n"),
            String::from("Stuck in the middle with you\n"),
            String::from("Goodbye, World!\n"),
        ];
        let b: LineBatch = input.clone().into();
        for (i, line) in b.iter().enumerate() {
            assert_eq!(line, input[i].trim());
        }
    }

    #[test]
    fn from_vec_wo_newlines() {
        let input = vec![
            String::from("Hello, world!"),
            String::from("Stuck in the middle with you"),
            String::from("Goodbye, World!"),
        ];
        let b: LineBatch = input.clone().into();
        for (i, line) in b.iter().enumerate() {
            assert_eq!(line, input[i]);
        }
    }

    #[test]
    fn from_vec_removes_empty_lines() {
        let input = vec![
            String::from("Hello, world!\n"),
            String::from("\n"),
            String::from("\n"),
            String::from("Stuck in the middle with you\n"),
            String::from("Goodbye, World!\n"),
        ];
        let b: LineBatch = input.clone().into();

        let trimmed: Vec<_> = input.iter().filter(|s| !s.trim().is_empty()).collect();
        for (i, line) in b.iter().enumerate() {
            assert_eq!(line, trimmed[i].trim());
        }
    }
}
