#![no_main]
use libfuzzer_sys::fuzz_target;

fuzz_target!(|data: &[u8]| {
    let mut batcher = batcher::Batcher::<_, 8192>::new(50, data);

    for batch in batcher.iter() {
        for line in batch.iter() {
            let _ = line;
        }
    }
});
